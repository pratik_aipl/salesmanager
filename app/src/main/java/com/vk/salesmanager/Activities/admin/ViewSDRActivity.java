package com.vk.salesmanager.Activities.admin;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Fonts.RobotoTextView;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.Models.ClosingStock;
import com.vk.salesmanager.Models.CompanyLocation;
import com.vk.salesmanager.Models.DSRCheckOutDetails;
import com.vk.salesmanager.Models.DSRDetails;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewSDRActivity extends AppCompatActivity implements AsynchTaskListner {


    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.rcyclerView)
    RecyclerView rcyclerView;
    @BindView(R.id.tv_message)
    RobotoTextView tvMessage;
    @BindView(R.id.lin_empty)
    LinearLayout linEmpty;
    @BindView(R.id.float_add)
    FloatingActionButton floatAdd;

    ArrayList<DSRDetails> dsrDetails=new ArrayList<>();

    DSRDetailsAdapter dsrDetailsAdapter;

    String ID,DNAME;
    public JsonParserUniversal jParser;
    DSRDetails dsr ;
    DSRCheckOutDetails dsrCheckOutDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_q_t_y);
        ButterKnife.bind(this);
        jParser = new JsonParserUniversal();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ID=getIntent().getStringExtra("ID");
        DNAME=getIntent().getStringExtra("NAME");

        tvTitle.setText(DNAME);
        floatAdd.setVisibility(View.GONE);

        new CallRequest(this).GetDSRDetails(ID);

    }

    public void SetupRecylerView() {

        dsrDetailsAdapter = new DSRDetailsAdapter(dsrDetails, this);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        rcyclerView.setLayoutAnimation(controller);
        rcyclerView.scheduleLayoutAnimation();
        rcyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rcyclerView.setItemAnimator(new DefaultItemAnimator());
        rcyclerView.setAdapter(dsrDetailsAdapter);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getDSRDetails:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {
                            App.mySharedPref.clearApp();
                            Intent i = new Intent(this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else if (respCode.equalsIgnoreCase("200")) {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                JSONArray jarray = jObj.getJSONArray("DSR");
                                if (jarray.length() > 0) {
                                    linEmpty.setVisibility(View.GONE);
                                    rcyclerView.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        dsr = (DSRDetails) jParser.parseJson(jData, new DSRDetails());

                                        JSONArray jCheckOut = jData.getJSONArray("CheckOut");
                                        for (int j = 0; j < jCheckOut.length(); j++) {
                                            JSONObject jDataLocation = jCheckOut.getJSONObject(j);
                                            dsrCheckOutDetail = (DSRCheckOutDetails) jParser.parseJson(jDataLocation, new DSRCheckOutDetails());
                                            dsr.dsrCheckOutDetails.add(dsrCheckOutDetail);
                                        }
                                        dsrDetails.add(dsr);
                                    }
                                    SetupRecylerView();
                                }
                            }else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), this);
                                linEmpty.setVisibility(View.VISIBLE);
                                rcyclerView.setVisibility(View.GONE);
                            }
                        } else {
                            linEmpty.setVisibility(View.VISIBLE);
                            rcyclerView.setVisibility(View.GONE);
                            //  Utils.showToast("Server side error...", this);
                        }
                        break;
                }
            } catch (JSONException e) {
                linEmpty.setVisibility(View.VISIBLE);
                rcyclerView.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }

}