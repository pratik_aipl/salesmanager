package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;

public class DistributorOrder implements Serializable{
    public String DistributorID = "", DistributorName = "";

    public String getDistributorID() {
        return DistributorID;
    }

    public void setDistributorID(String distributorID) {
        DistributorID = distributorID;
    }

    public String getDistributorName() {
        return DistributorName;
    }

    public void setDistributorName(String distributorName) {
        DistributorName = distributorName;
    }
}
