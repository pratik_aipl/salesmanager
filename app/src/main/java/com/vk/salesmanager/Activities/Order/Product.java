package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;

public class Product implements Serializable {
    public String ItemID = "",
            ItemName = "",
            SKU = "",
            CompanyID = "",
            Weight = "",
            UnitWeight = "",
            Remark = "",
            ItemUrl = "",
            MRP = "";

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getUnitWeight() {
        return UnitWeight;
    }

    public void setUnitWeight(String unitWeight) {
        UnitWeight = unitWeight;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getItemUrl() {
        return ItemUrl;
    }

    public void setItemUrl(String itemUrl) {
        ItemUrl = itemUrl;
    }

    public String getMRP() {
        return MRP;
    }

    public void setMRP(String MRP) {
        this.MRP = MRP;
    }
}
