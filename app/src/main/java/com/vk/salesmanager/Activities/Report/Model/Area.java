package com.vk.salesmanager.Activities.Report.Model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 8/31/2018.
 */

public class Area implements Serializable {
    public String LocationID = "", LocationName = "", LocationType = "", ParentLocationID = "", Remark = "";

    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String locationID) {
        LocationID = locationID;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getLocationType() {
        return LocationType;
    }

    public void setLocationType(String locationType) {
        LocationType = locationType;
    }

    public String getParentLocationID() {
        return ParentLocationID;
    }

    public void setParentLocationID(String parentLocationID) {
        ParentLocationID = parentLocationID;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }
}
