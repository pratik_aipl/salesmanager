package com.vk.salesmanager.Activities.ASM;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.CompanyLocation;
import com.vk.salesmanager.Models.Route;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DrawRouteActivity extends AppCompatActivity implements
        OnMapReadyCallback, AdapterView.OnItemClickListener, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, AsynchTaskListner {

    DrawRouteActivity instance;
    ArrayList<LatLng> MarkerPoints;
    private GoogleMap mMap;
    AutoCompleteTextView etOrigin, etDestination;
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    //    private static final String API_KEY = "AIzaSyAI0EXE3jqr55vJpB2aWgzacDkefDwiLDw";
    private static final String API_KEY = "AIzaSyBmvyNz5KpO7Ka4F_rwOls08cfYQA0DqEM";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    GooglePlacesAutocompleteAdapter madapter;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    Handler handler;
    LocationManager locationManager;
    double latti, longti;
    public Button btn_next;
    public Spinner sp_area;
    String lattitude, longtitude, DISTANCE, DURATION, origin, destination, uid;
    private static final int REQUEST_LOCATION = 1;
    public CompanyLocation companyLocation;
    public ArrayList<String> strCompanyLocationArray = new ArrayList<>();
    public JsonParserUniversal jParser;
    public Toolbar toolbar;
    public TextView tv_title;
    public static String CompanyLocationID = "";
    public ArrayList<CompanyLocation> companyLocationArray = new ArrayList<>();
    public EditText et_to, et_from, et_routeno;
    public Route obj;
    public int SelectedID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_route);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        jParser = new JsonParserUniversal();
        instance = this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Add Route");
        et_from = findViewById(R.id.et_from);
        et_to = findViewById(R.id.et_to);
        et_routeno = findViewById(R.id.et_routeno);


        etOrigin = findViewById(R.id.mapfromtv);

        etDestination = findViewById(R.id.maptotv);
        btn_next = findViewById(R.id.btn_next);
        //TODO: SET AUTO COMPLETE TEXT VIEW WITH PLACE HOLDER
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CompanyLocationID.equals("0") || CompanyLocationID.equals("")) {
                    Utils.showToast("Please Select Area", instance);
                } else if (et_from.getText().toString().equals("")) {
                    et_from.setError("Please enter From Location");
                    et_from.setFocusable(true);
                } else if (et_to.getText().toString().equals("")) {
                    et_to.setError("Please enter To location");
                    et_to.setFocusable(true);
                } else if (et_routeno.getText().toString().equals("")) {
                    et_routeno.setError(" Please enter Route No");
                    et_routeno.setFocusable(true);
                } else {
                    Intent intent = getIntent();
                    if (intent.hasExtra("edit")) {
                        new CallRequest(instance).define_route_edit(CompanyLocationID, et_from.getText().toString().trim(),
                                et_to.getText().toString().trim(), et_routeno.getText().toString(), obj.getRouteID());

                    } else {
                        new CallRequest(instance).define_route(CompanyLocationID, et_from.getText().toString().trim(),
                                et_to.getText().toString().trim(), et_routeno.getText().toString());

                    }
                }
            }
        });
        sp_area = findViewById(R.id.sp_area);
        new CallRequest(instance).getCompanyLocation();
        sp_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        CompanyLocationID = String.valueOf(companyLocationArray.get(position).getID());

                    } else {
                        CompanyLocationID = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        MarkerPoints = new ArrayList<>();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //getLocation();


  /*      //TODO: CALL UPDATE CURRENT LOCATION API
        handler = new Handler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getAddress();
            }
        }, 1500);
*/
        madapter = new GooglePlacesAutocompleteAdapter(this, R.layout.listitem);
        etOrigin.setAdapter(madapter);
        etOrigin.setOnItemClickListener(DrawRouteActivity.this);
        etDestination.setAdapter(madapter);
        etDestination.setOnItemClickListener(this);


        //TODO: FIND DISTANCE ON DESTINATION AUTO COMPLETE TEXT VIEW

        etDestination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //TODO: HIDE KEYPAD
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

                //TODO: CALL FIND DISTANCE METHOD
                sendRequest();
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("edit")) {
            obj = (Route) getIntent().getExtras().getSerializable("obj");
            setData(obj);
        }


    } //TODO: SET CURRENT LOCATION IN EDIT TEXT

    public void setData(Route obj) {
        et_from.setText(obj.getFrom());
        et_to.setText(obj.getTo());
        et_routeno.setText(obj.getRounteNo());
    }

    /*private void getAddress() {
        //Toast.makeText(getActivity(), "method", Toast.LENGTH_SHORT).show();

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latti, longti, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                etOrigin.setText(String.valueOf(address.getAddressLine(0)));
                //Toast.makeText(getActivity(), String.valueOf(address.getAddressLine(0))+","+address.getLocality()+","+address.getAdminArea()+","+address.getCountryName()+","+address.getPostalCode(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
*/
   /* //TODO: GET CURRENT LOCATION
    private void getLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            // Geocoder geocoder = new Geocoder(context, Locale.getDefault());

            if (location != null) {
                latti = location.getLatitude();
                longti = location.getLongitude();

                // sydney = new LatLng(latti, longti);
                lattitude = String.valueOf(latti);
                longtitude = String.valueOf(longti);

                //Toast.makeText(this, "Lattitude:- "+lattitude+"\n"+"Longtitude:- "+longtitude, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Enabled to access your location", Toast.LENGTH_SHORT).show();
            }

        }
    }*/


    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

//Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

//move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

//stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //TODO: CALL PLACE HOLDER GooglePlacesAutocompleteAdapter METHOD
    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }

    }

    //TODO: CALL PLACE HOLDER autocomplete METHOD
    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {

            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:in");//country:ng|country:za
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));


            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            //Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            //Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            //Log.e(Log, "Cannot process JSON results", e);
        }

        return resultList;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

// Setting onclick event listener for the map


    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

// Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

// Show an explanation to the user asynchronously -- don't block
// this thread waiting for the user's response! After the user
// sees the explanation, try again to request the permission.

//Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
// No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
// If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

// permission was granted. Do the
// contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

// Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

// other 'case' lines to check for other permissions this app might request.
// You can add here other case statements according to your requirement.
        }
    }

    private void sendRequest() {
        origin = etOrigin.getText().toString();
        destination = etDestination.getText().toString();

        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter Pickup Location", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            Toast.makeText(this, "Please enter Drop Location", Toast.LENGTH_SHORT).show();
            return;
        }
        if (origin.equals(destination)) {
            Toast.makeText(this, "Please set Different Pickup or Drop Location", Toast.LENGTH_LONG).show();
            return;
        }


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getCompanyLocation:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            strCompanyLocationArray.clear();
                            companyLocationArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            companyLocation = new CompanyLocation();
                            companyLocation.setValue("Area");
                            companyLocation.setID("0");
                            strCompanyLocationArray.add(companyLocation.getValue());
                            companyLocationArray.add(companyLocation);
                            JSONArray jAreaArray = jData.getJSONArray("CompanyLocation");
                            if (jAreaArray != null && jAreaArray.length() > 0) {
                                for (int i = 0; i < jAreaArray.length(); i++) {
                                    companyLocation = (CompanyLocation) jParser.parseJson(jAreaArray.getJSONObject(i), new CompanyLocation());
                                    companyLocationArray.add(companyLocation);
                                    strCompanyLocationArray.add(companyLocation.getValue());
                                  /*  if (obj.getCompanyLocationID().equals(companyLocation.getID())) {
                                        SelectedID = i;
                                    }*/
                                }

                                sp_area.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCompanyLocationArray));
                                Intent intent = getIntent();
                                if (intent.hasExtra("edit")) {
                                    for (int i = 0; i < companyLocationArray.size(); i++) {
                                        if (companyLocationArray.get(i).getID().equalsIgnoreCase(obj.getCompanyLocationID())) {
                                            sp_area.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case define_route:
                    Utils.hideProgressDialog();
                    JSONObject jObj;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {

                            Utils.showToast(jObj.getString("message"), instance);
                            setResult(RESULT_OK, new Intent());
                            finish();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
}
