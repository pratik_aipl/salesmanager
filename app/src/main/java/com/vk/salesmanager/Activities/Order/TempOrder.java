package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;

public  class TempOrder implements Serializable{
    public String ItemID = "", ItemName = "";
    public int QTY = 0;

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public int getQTY() {
        return QTY;
    }

    public void setQTY(int QTY) {
        this.QTY = QTY;
    }
}
