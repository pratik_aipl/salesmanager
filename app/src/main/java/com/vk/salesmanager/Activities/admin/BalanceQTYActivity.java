package com.vk.salesmanager.Activities.admin;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vk.salesmanager.Activities.Distributor.Distributor;
import com.vk.salesmanager.Activities.Distributor.DistributorListAdapter;
import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Fonts.RobotoTextView;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ClosingStock;
import com.vk.salesmanager.Models.CompanyLocation;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BalanceQTYActivity extends AppCompatActivity implements AsynchTaskListner {


    @BindView(R.id.tv_title)
    RobotoTextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.rcyclerView)
    RecyclerView rcyclerView;
    @BindView(R.id.tv_message)
    RobotoTextView tvMessage;
    @BindView(R.id.lin_empty)
    LinearLayout linEmpty;
    @BindView(R.id.float_add)
    FloatingActionButton floatAdd;

    ArrayList<ClosingStock> closingStocksList=new ArrayList<>();

    BalanceQTYAdapter balanceQTYAdapter;

    String ID,DNAME;
    public JsonParserUniversal jParser;
    ClosingStock closingStock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_q_t_y);
        ButterKnife.bind(this);
        jParser = new JsonParserUniversal();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ID=getIntent().getStringExtra("ID");
        DNAME=getIntent().getStringExtra("DNAME");

        tvTitle.setText(DNAME);
        floatAdd.setVisibility(View.GONE);

        new CallRequest(this).GetClosingStock(ID);

    }

    public void SetupRecylerView() {

        balanceQTYAdapter = new BalanceQTYAdapter(closingStocksList, this);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        rcyclerView.setLayoutAnimation(controller);
        rcyclerView.scheduleLayoutAnimation();
        rcyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rcyclerView.setItemAnimator(new DefaultItemAnimator());
        rcyclerView.setAdapter(balanceQTYAdapter);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getDistributorStock:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {
                            App.mySharedPref.clearApp();
                            Intent i = new Intent(this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else if (respCode.equalsIgnoreCase("200")) {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                JSONArray jarray = jObj.getJSONArray("data");
                                if (jarray.length() > 0 && jarray != null) {
                                    linEmpty.setVisibility(View.GONE);
                                    rcyclerView.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        closingStock = (ClosingStock) jParser.parseJson(jData, new ClosingStock());
                                        closingStocksList.add(closingStock);
                                    }
                                    SetupRecylerView();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), this);
                                linEmpty.setVisibility(View.VISIBLE);
                                rcyclerView.setVisibility(View.GONE);
                            }
                        } else {
                            linEmpty.setVisibility(View.VISIBLE);
                            rcyclerView.setVisibility(View.GONE);
                            //  Utils.showToast("Server side error...", this);
                        }
                        break;
                }
            } catch (JSONException e) {
                linEmpty.setVisibility(View.VISIBLE);
                rcyclerView.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }

}