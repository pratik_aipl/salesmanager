package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;
import java.util.ArrayList;

public  class AddOrder implements Serializable{

    public String RetailerName = "", RetailerID = "", DistributorID = "", DistributorName = "", Date = "";

    public String getRetailerName() {
        return RetailerName;
    }

    public void setRetailerName(String retailerName) {
        RetailerName = retailerName;
    }

    public String getRetailerID() {
        return RetailerID;
    }

    public void setRetailerID(String retailerID) {
        RetailerID = retailerID;
    }

    public String getDistributorID() {
        return DistributorID;
    }

    public void setDistributorID(String distributorID) {
        DistributorID = distributorID;
    }

    public String getDistributorName() {
        return DistributorName;
    }

    public void setDistributorName(String distributorName) {
        DistributorName = distributorName;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public ArrayList<TempOrder> tempOrderArrayList = new ArrayList<>();
}
