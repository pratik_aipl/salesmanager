package com.vk.salesmanager.Activities.Profile;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.salesmanager.App;
import com.vk.salesmanager.BuildConfig;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.MainUser;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MemberProfileActivity extends AppCompatActivity implements AsynchTaskListner {
    public MemberProfileActivity instance;
    public Toolbar toolbar;
    public ImageView img_address_edit, img_id_edit, img_detail_edit;
    public TextView tv_title, tv_pincode, tv_city, tv_state, tv_area, tv_address, tv_dob,
            tv_mobile, tv_email, tv_last_name, tv_first_name, tv_unique_no, tv_image_proof;
    public MainUser mainUser;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_profile);

        instance = MemberProfileActivity.this;
        jParser = new JsonParserUniversal();
        img_profile = findViewById(R.id.img_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        img_address_edit = findViewById(R.id.img_address_edit);
        img_id_edit = findViewById(R.id.img_id_edit);
        img_detail_edit = findViewById(R.id.img_detail_edit);
        tv_pincode = findViewById(R.id.tv_pincode);
        tv_city = findViewById(R.id.tv_city);
        tv_state = findViewById(R.id.tv_state);
        tv_area = findViewById(R.id.tv_area);
        tv_address = findViewById(R.id.tv_address);
        tv_dob = findViewById(R.id.tv_dob);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_email = findViewById(R.id.tv_email);
        tv_last_name = findViewById(R.id.tv_last_name);
        tv_first_name = findViewById(R.id.tv_first_name);
        tv_unique_no = findViewById(R.id.tv_unique_no);
        tv_image_proof = findViewById(R.id.tv_image_proof);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Member Profile");


        img_address_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ProfileTwoActivity.class));

            }
        });
        img_id_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ProfileThreeActivity.class));

            }
        });
        img_detail_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ProfileOneActivity.class));

            }
        });
    }

    private void setProfileData(MainUser mainUser) {
        Picasso.with(instance)
                .load(BuildConfig.API_URL + mainUser.getPhotoURL())
                .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                .into(img_profile);

        tv_first_name.setText(mainUser.getFirstName().toString());
        tv_pincode.setText(mainUser.getZipcode().toString());
        String logoPath=mainUser.getUniqueIdentityURL().toString();
        tv_image_proof.setText(logoPath.substring(logoPath.lastIndexOf("/") + 1, logoPath.length()));
        tv_unique_no.setText(mainUser.getUniqueIdentityNO().toString());
        tv_last_name.setText(mainUser.getLastName().toString());
        tv_email.setText(mainUser.getEmail().toString());
        tv_mobile.setText(mainUser.getMobile().toString());
        tv_dob.setText(Utils.changeDateFormat(mainUser.getDOB().toString()));
        tv_address.setText(mainUser.getAddress().toString());
        tv_area.setText(mainUser.getAreaName().toString());
        tv_state.setText(mainUser.getStateName().toString());
        tv_city.setText(mainUser.getCityName().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        new CallRequest(instance).getProfile();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getProfile:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONObject jData = jObj.getJSONObject("data");
                            mainUser = (MainUser) jParser.parseJson(jData, new MainUser());
                            App.mainUser = mainUser;

                            setProfileData(mainUser);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }
}
