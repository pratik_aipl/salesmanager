package com.vk.salesmanager.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.salesmanager.Activities.ASM.AsmListActivity;
import com.vk.salesmanager.Activities.ASM.AssignRouteListActivity;
import com.vk.salesmanager.Activities.ASM.RouteListActivity;
import com.vk.salesmanager.Activities.CNF.CNFListActivity;
import com.vk.salesmanager.Activities.Distributor.DistributorListActivity;
import com.vk.salesmanager.Activities.Order.OrderListActivity;
import com.vk.salesmanager.Activities.Profile.MemberProfileActivity;
import com.vk.salesmanager.Activities.Report.ReportActivity;
import com.vk.salesmanager.Activities.Retailer.RetailorListActivity;
import com.vk.salesmanager.Activities.Sales.SalesManagerListActivity;
import com.vk.salesmanager.Activities.Sales.SalesRouteListActivity;
import com.vk.salesmanager.Activities.SuperStockist.SuperStockistListActivity;
import com.vk.salesmanager.Activities.ZSM.RsmListActivity;
import com.vk.salesmanager.Activities.admin.AdminSalesManagerListActivity;
import com.vk.salesmanager.Activities.admin.DSRSelectionActivity;
import com.vk.salesmanager.Activities.dsr.AddDailySalesReportActivity;
import com.vk.salesmanager.App;
import com.vk.salesmanager.BuildConfig;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.MainUser;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.FragmentDrawer;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

//Edited By Ayaz Baharuni (7-11-2020)

public class DashBoardActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, AsynchTaskListner {

    public DashBoardActivity instance;
    public CardView card_distributors, card_ss_distributors, card_asm_drawroute, card_cnf_ss, card_md_sales_report, card_md_ss, card_md_cnf, card_rsm_superS, card_asm, card_sm, card_retailers, card_routes, card_sales_report, card_place_order;
    public CardView card_asm_report, card_sm_route, card_asm_assign_route, card_asm_distributors, card_asm_sm, card_asm_retailers, card_asm_place_order;
    public CardView card_dis_report, card_sm_distributors, card_sm_retailers, card_sm_place_order, card_sm_sales_report;
    public CardView card_dis_retailers, card_dis_place_order, card_cnf_sales_report, card_ss_sales_report, card_zsm_sales_report;
    public CardView card_sm_dsr, card_asm_dsr, card_rsm, card_zsm_superS, card_ret_orderlist, card_ret_report;
    public CardView card_admin_daily_sel, card_admin_weekly_sel, card_admin_month_sel, card_admin_yearly_sel,card_admin_Closing_Stock,card_admin_view_DSR;
    public LinearLayout mainLay, lin_asm, lin_rsm, lin_sm, lin_dis, lin_md, lin_cnf, lin_ssm, lin_zsm, lin_retailer,lin_admin;
    public ImageView drawerButton;
    public CircleImageView img_profile, nav_img_profile;
    public RelativeLayout rel_profile;
    public JsonParserUniversal jParser;
    public MainUser mainUser;
    public TextView tv_name, tv_role_name, tv_mobile, nav_userName, nav_email, tv_message;
    public TextView tv_dly_ord, tv_dly_amut, tv_wkly_ord, tv_wkly_amut, tv_mntly_ord, tv_mntly_amut, tv_yrly_ord, tv_yrly_amut ;
    public Button btn_retry;
    public LinearLayout lin_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        instance = DashBoardActivity.this;
        jParser = new JsonParserUniversal();

        FindElements();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new CallRequest(instance).getProfile();
    }

    public void FindElements() {
        rel_profile = findViewById(R.id.rel_profile);

        lin_dis = findViewById(R.id.lin_dis);
        lin_admin = findViewById(R.id.lin_admin);
        lin_rsm = findViewById(R.id.lin_rsm);
        lin_asm = findViewById(R.id.lin_asm);
        lin_retailer = findViewById(R.id.lin_retailer);
        lin_zsm = findViewById(R.id.lin_zsm);
        lin_md = findViewById(R.id.lin_md);
        lin_cnf = findViewById(R.id.lin_cnf);
        lin_ssm = findViewById(R.id.lin_ssm);
        lin_sm = findViewById(R.id.lin_sm);
        btn_retry = findViewById(R.id.btn_retry);
        lin_empty = findViewById(R.id.lin_empty);
        tv_message = findViewById(R.id.tv_message);



        card_admin_daily_sel = findViewById(R.id.card_admin_daily_sel);
        card_admin_weekly_sel = findViewById(R.id.card_admin_weekly_sel);
        card_admin_month_sel = findViewById(R.id.card_admin_month_sel);
        card_admin_yearly_sel = findViewById(R.id.card_admin_yearly_sel);
        card_admin_Closing_Stock = findViewById(R.id.card_admin_Closing_Stock);
        card_admin_view_DSR = findViewById(R.id.card_admin_view_DSR);

        tv_dly_ord = findViewById(R.id.tv_dly_ord);
        tv_dly_amut = findViewById(R.id.tv_dly_amut);
        tv_wkly_ord = findViewById(R.id.tv_wkly_ord);
        tv_wkly_amut = findViewById(R.id.tv_wkly_amut);
        tv_mntly_ord = findViewById(R.id.tv_mntly_ord);
        tv_mntly_amut = findViewById(R.id.tv_mntly_amut);
        tv_yrly_ord = findViewById(R.id.tv_yrly_ord);
        tv_yrly_amut = findViewById(R.id.tv_yrly_amut);



        card_asm_distributors = findViewById(R.id.card_asm_distributors);
        card_asm_sm = findViewById(R.id.card_asm_sm);
        card_asm_retailers = findViewById(R.id.card_asm_retailers);
        card_asm_place_order = findViewById(R.id.card_asm_place_order);
        card_asm_drawroute = findViewById(R.id.card_asm_drawroute);
        card_dis_retailers = findViewById(R.id.card_dis_retailers);
        card_dis_place_order = findViewById(R.id.card_dis_place_order);
        card_asm_assign_route = findViewById(R.id.card_asm_assign_route);
        card_sm_dsr = findViewById(R.id.card_sm_dsr);
        card_asm_dsr = findViewById(R.id.card_asm_dsr);

        card_sm_route = findViewById(R.id.card_sm_route);
        card_sm_distributors = findViewById(R.id.card_sm_distributors);
        card_sm_retailers = findViewById(R.id.card_sm_retailers);
        card_sm_place_order = findViewById(R.id.card_sm_place_order);
        card_sm_sales_report = findViewById(R.id.card_sm_sales_report);
        card_md_sales_report = findViewById(R.id.card_md_sales_report);
        card_cnf_sales_report = findViewById(R.id.card_cnf_sales_report);
        card_ss_sales_report = findViewById(R.id.card_ss_sales_report);
        card_ret_report = findViewById(R.id.card_ret_report);
        card_zsm_sales_report = findViewById(R.id.card_zsm_sales_report);

        card_asm_report = findViewById(R.id.card_asm_report);
        card_dis_report = findViewById(R.id.card_dis_report);
        card_asm_place_order = findViewById(R.id.card_asm_place_order);
        card_distributors = findViewById(R.id.card_distributors);
        card_ss_distributors = findViewById(R.id.card_ss_distributors);
        card_asm = findViewById(R.id.card_asm);
        card_rsm_superS = findViewById(R.id.card_superS);
        card_zsm_superS = findViewById(R.id.card_zsm_superS);
        card_ret_orderlist = findViewById(R.id.card_ret_orderlist);
        card_rsm = findViewById(R.id.card_rsm);
        card_md_ss = findViewById(R.id.card_md_ss);
        card_md_cnf = findViewById(R.id.card_md_cnf);
        card_cnf_ss = findViewById(R.id.card_cnf_ss);

        card_sm = findViewById(R.id.card_sm);
        card_retailers = findViewById(R.id.card_retailers);
        card_routes = findViewById(R.id.card_routes);
        card_sales_report = findViewById(R.id.card_sales_report);
        card_place_order = findViewById(R.id.card_place_order);
        tv_name = findViewById(R.id.tv_name);
        tv_role_name = findViewById(R.id.tv_role_name);
        tv_mobile = findViewById(R.id.tv_mobile);
        nav_userName = findViewById(R.id.nav_userName);
        nav_email = findViewById(R.id.nav_email);
        img_profile = findViewById(R.id.img_profile);
        nav_img_profile = findViewById(R.id.nav_img_profile);
        mainLay = findViewById(R.id.mainLay);
        FragmentDrawer drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerButton = findViewById(R.id.drawerButton);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);
        if (Utils.isNetworkAvailable(instance)) {
            lin_empty.setVisibility(View.GONE);
            CategoryOnClick();
        } else {
            tv_message.setText("No Internet, Please try again later");
            btn_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utils.isNetworkAvailable(instance)) {
                        lin_empty.setVisibility(View.GONE);
                        CategoryOnClick();
                    }
                }
            });

        }


    }

    public void CategoryOnClick() {
        rel_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MemberProfileActivity.class));
            }
        });
        card_asm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AsmListActivity.class));
            }
        });
        card_rsm_superS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SuperStockistListActivity.class));
            }
        });
        card_zsm_superS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SuperStockistListActivity.class));
            }
        });

        card_ret_orderlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, OrderListActivity.class));
            }
        });


        card_rsm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, RsmListActivity.class));
            }
        });
        card_md_ss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SuperStockistListActivity.class));
            }
        });
        card_cnf_ss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SuperStockistListActivity.class));
            }
        });
        card_md_cnf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, CNFListActivity.class));
            }
        });
        card_sm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SalesManagerListActivity.class));
            }
        });
        card_sm_route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SalesRouteListActivity.class));
            }
        });
        card_retailers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, RetailorListActivity.class));
            }
        });
        card_distributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DistributorListActivity.class));
            }
        });
        card_ss_distributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DistributorListActivity.class));
            }
        });
        card_asm_drawroute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, RouteListActivity.class));
            }
        });
        card_asm_assign_route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AssignRouteListActivity.class));
            }
        });
        card_routes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        card_sales_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_cnf_sales_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_zsm_sales_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_ss_sales_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_ret_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_md_sales_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, OrderListActivity.class));
            }
        });

        card_asm_sm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, SalesManagerListActivity.class));
            }
        });
        card_asm_retailers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, RetailorListActivity.class));
            }
        });
        card_asm_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_asm_distributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DistributorListActivity.class));
            }
        });
        card_sm_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, OrderListActivity.class));
            }
        });
        card_asm_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, OrderListActivity.class));
            }
        });
        card_sm_retailers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, RetailorListActivity.class));
            }
        });
        card_sm_distributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DistributorListActivity.class));
            }
        });
        card_sm_sales_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });
        card_dis_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, OrderListActivity.class));
            }
        });
        card_dis_retailers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, RetailorListActivity.class));
            }
        });
        card_dis_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ReportActivity.class));
            }
        });

        card_sm_dsr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AddDailySalesReportActivity.class)
                        .putExtra("From", "SM"));
            }
        });
        card_asm_dsr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AddDailySalesReportActivity.class).putExtra("From", "ASM"));
            }
        });



        // For Admin
        card_admin_daily_sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AdminSalesManagerListActivity.class).putExtra("TYPE","D"));
            }
        });

        card_admin_weekly_sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AdminSalesManagerListActivity.class).putExtra("TYPE","W"));
            }
        });

        card_admin_month_sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AdminSalesManagerListActivity.class).putExtra("TYPE","M"));
            }
        });

        card_admin_yearly_sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AdminSalesManagerListActivity.class).putExtra("TYPE","Y"));
            }
        });

        card_admin_Closing_Stock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DistributorListActivity.class));
            }
        });

        card_admin_view_DSR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DSRSelectionActivity.class));
            }
        });
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {

        switch (position) {
            case 0:
                break;
            case 1:
                startActivity(new Intent(instance, LeaderBoardActivity.class));
                break;
            case 2:
                break;

            case 3:
                break;

            case 4:
                startActivity(new Intent(instance, HelpQueryActivity.class));
                break;

            case 5:
                break;
                case 6:
                doLogout();
                break;

            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {


        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        moveTaskToBack(true);
                    }
                }).create().show();

    }

    public void doLogout() {


        new AlertDialog.Builder(this)
                .setTitle("Logout?")
                .setMessage("Are you sure for logout?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        App.mySharedPref.clearApp();
                        Intent i = new Intent(instance, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }).create().show();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getProfile:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            mainUser = (MainUser) jParser.parseJson(jData, new MainUser());
                            App.mainUser = mainUser;

                            setUserData(mainUser);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;

                        case getSalesDetails:
                        Utils.hideProgressDialog();
                        JSONObject jObjs = new JSONObject(result);
                        if (jObjs.getBoolean("status")) {
                            JSONObject jData = jObjs.getJSONObject("data");

                            tv_dly_ord.setText("ORDERS : "+jData.getString("DailySalesTotalOrder"));
                            tv_dly_amut.setText("\u20A8 "+jData.getString("DailySales"));
                            tv_wkly_ord.setText("ORDERS : "+jData.getString("WeeklySalesTotalOrder"));
                            tv_wkly_amut.setText("\u20A8 "+jData.getString("WeeklySales"));
                            tv_mntly_ord.setText("ORDERS : "+jData.getString("MonthlySalesTotalOrder"));
                            tv_mntly_amut.setText("\u20A8 "+jData.getString("MonthlySales"));
                            tv_yrly_ord.setText("ORDERS : "+jData.getString("YearlySalesTotalOrder"));
                            tv_yrly_amut.setText("\u20A8 "+jData.getString("YearlySales"));

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObjs.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }

    private void setUserData(MainUser mainUser) {
        tv_name.setText(mainUser.getFirstName() + " " + mainUser.getLastName());
        nav_userName.setText(mainUser.getFirstName() + " " + mainUser.getLastName());
        tv_role_name.setText(mainUser.getRoleName());
        nav_email.setText(mainUser.getEmail());
        tv_mobile.setText("Reg. Mobile : " + mainUser.getMobile());
        if (TextUtils.isEmpty(mainUser.getPhotoURL()))
            Picasso.with(instance)
                    .load(BuildConfig.API_URL + mainUser.getPhotoURL())
                    .error(R.drawable.profile)
                    //this is optional the image to display while the url image is downloading
                    .into(img_profile);
        Picasso.with(instance)
                .load(BuildConfig.API_URL + mainUser.getPhotoURL())
                .error(R.drawable.profile)
                //this is optional the image to display while the url image is downloading
                .into(nav_img_profile);

        if (mainUser.getRole_id().equalsIgnoreCase("1")) {
            new CallRequest(instance).getAdminSalesData();
            //Admin
            lin_admin.setVisibility(View.VISIBLE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);

        }

        if (mainUser.getRole_id().equalsIgnoreCase("2")) {
            //Rsm
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.VISIBLE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);

        }
        if (mainUser.getRole_id().equalsIgnoreCase("3")) {
            //asm
            lin_admin.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_asm.setVisibility(View.VISIBLE);
            lin_sm.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);


        }
        if (mainUser.getRole_id().equalsIgnoreCase("4")) {
            //sm
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.VISIBLE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);


        }
        if (mainUser.getRole_id().equalsIgnoreCase("5")) {
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.VISIBLE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);

        }
        if (mainUser.getRole_id().equalsIgnoreCase("6")) {
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.VISIBLE);
        }
        if (mainUser.getRole_id().equalsIgnoreCase("7")) {

            //MD
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.VISIBLE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);


        }
        if (mainUser.getRole_id().equalsIgnoreCase("8")) {

            //CNF
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.VISIBLE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);


        }
        if (mainUser.getRole_id().equalsIgnoreCase("9")) {
            //SS
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.VISIBLE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.GONE);
            lin_retailer.setVisibility(View.GONE);


        }
        if (mainUser.getRole_id().equalsIgnoreCase("10")) {
            //ZSM
            lin_admin.setVisibility(View.GONE);
            lin_asm.setVisibility(View.GONE);
            lin_sm.setVisibility(View.GONE);
            lin_dis.setVisibility(View.GONE);
            lin_rsm.setVisibility(View.GONE);
            lin_ssm.setVisibility(View.GONE);
            lin_md.setVisibility(View.GONE);
            lin_cnf.setVisibility(View.GONE);
            lin_zsm.setVisibility(View.VISIBLE);
            lin_retailer.setVisibility(View.GONE);


        }
    }
}
