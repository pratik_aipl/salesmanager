package com.vk.salesmanager.Activities.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vk.salesmanager.Models.DSRCheckOutDetails;
import com.vk.salesmanager.Models.DSRDetails;
import com.vk.salesmanager.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DSRSubListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DSRCheckOutDetails> mArrayList;
    public Context context;

    public DSRSubListAdapter(ArrayList<DSRCheckOutDetails> moviesList, Context context) {
        mArrayList = moviesList;
         this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_checkout,tv_name,tv_isorder,tv_orderno,tv_remark;

        public MyViewHolder(View view) {
            super(view);
            tv_checkout = view.findViewById(R.id.tv_checkout);
            tv_name = view.findViewById(R.id.tv_name);
            tv_isorder = view.findViewById(R.id.tv_isorder);
            tv_orderno = view.findViewById(R.id.tv_orderno);
            tv_remark = view.findViewById(R.id.tv_remark);
          }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dsr_sub_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        DSRCheckOutDetails order = mArrayList.get(pos);
        holder.tv_checkout.setText("Check-Out : "+order.getCheckOut());
        holder.tv_name.setText("Name : "+order.getFirstName()+" "+order.getLastName());
        holder.tv_isorder.setText("Is Order : "+order.getOrderPlace());
        holder.tv_orderno.setText("Order No : "+order.getOrderNumber());
        holder.tv_remark.setText("Remarks : "+order.getRemarks());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

}
