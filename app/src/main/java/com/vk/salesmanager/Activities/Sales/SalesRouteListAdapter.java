package com.vk.salesmanager.Activities.Sales;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vk.salesmanager.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 11/3/2018.
 */

public class SalesRouteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private ArrayList<SalesRoute> mArrayList;
    public Context context;
    private ArrayList<SalesRoute> mFilteredList;

    public SalesRouteListAdapter(ArrayList<SalesRoute> moviesList, Context context) {
        mArrayList = moviesList;
        mFilteredList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_route_no, tv_route_name, tv_date, tv_from, tv_to;
        public RelativeLayout rel_date;

        public MyViewHolder(View view) {
            super(view);
            tv_route_no = view.findViewById(R.id.tv_route_no);
            tv_date = view.findViewById(R.id.tv_date);
            tv_to = view.findViewById(R.id.tv_to);
            tv_from = view.findViewById(R.id.tv_from);
            tv_route_name = view.findViewById(R.id.tv_route_name);
            rel_date = view.findViewById(R.id.rel_date);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_sm_route_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        SalesRoute order = mFilteredList.get(pos);
        holder.tv_date.setText(order.getRouteDate());
        holder.tv_route_no.setText(order.getRounteNo());
        holder.tv_route_name.setText(order.getLocationName());
        holder.tv_from.setText("From : " + order.getFrom());
        holder.tv_to.setText("To : " + order.getTo());
        holder.setIsRecyclable(false);
        if (order.getRouteDate().equals("") || order.getRouteDate() == null) {
            holder.rel_date.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);


    }


    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<SalesRoute> filteredList = new ArrayList<>();

                    for (SalesRoute androidVersion : mArrayList) {

                        if (androidVersion.getLocationName().toLowerCase().contains(charString)
                                || androidVersion.getRounteNo().toLowerCase().contains(charString)
                                || androidVersion.getRouteDate().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<SalesRoute>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
