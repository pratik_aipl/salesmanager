package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 8/27/2018.
 */

public class PlaceOrder implements Serializable {
    public String OrderDetailID="";

    public String getOrderDetailID() {
        return OrderDetailID;
    }

    public void setOrderDetailID(String orderDetailID) {
        OrderDetailID = orderDetailID;
    }

    public String getDelieveredQTY() {
        return DelieveredQTY;
    }

    public void setDelieveredQTY(String delieveredQTY) {
        DelieveredQTY = delieveredQTY;
    }

    public String DelieveredQTY="";
}
