package com.vk.salesmanager.Activities.Distributor;

import android.content.Intent;
import android.graphics.PorterDuff;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.Area;
import com.vk.salesmanager.Models.City;
import com.vk.salesmanager.Models.States;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DistributorEditTwoActivity extends AppCompatActivity implements AsynchTaskListner {
    public DistributorEditTwoActivity instance;
    public EditText et_address, et_pincode;
    public FloatingActionButton float_previous, float_next;
    public Toolbar toolbar;
    public TextView tv_title;
    public Spinner  sp_state, sp_city, sp_area;
    public JsonParserUniversal jParser;
    public States states;
    public ArrayList<States> statesArray = new ArrayList<>();
    public ArrayList<String> strStatesArray = new ArrayList<>();
    public String states_id = "";
    public City city;
    public ArrayList<City> cityArray = new ArrayList<>();
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String city_id = "";
    public Area area;
    public ArrayList<Area> areaArray = new ArrayList<>();
    public ArrayList<String> strAreaArray = new ArrayList<>();
    public String area_id = "";
    public int selcted_city = -1, selcted_state = -1, selcted_country = 0, selcted_area = -1;
    public Button btn_update;

    public Distributor distributor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member2);
        distributor = (Distributor) getIntent().getExtras().getSerializable("distributor");

        instance = DistributorEditTwoActivity.this;
        jParser = new JsonParserUniversal();

        float_previous = findViewById(R.id.float_previous);
        float_next = findViewById(R.id.float_next);
        btn_update = findViewById(R.id.btn_update);
        sp_state = findViewById(R.id.sp_state);
        sp_city = findViewById(R.id.sp_city);
        sp_area = findViewById(R.id.sp_area);
        toolbar = findViewById(R.id.toolbar);
        et_address = findViewById(R.id.et_address);
        et_pincode = findViewById(R.id.et_pincode);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Edit Distributor");

        float_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        float_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validaton();

            }
        });


        states = new States();
        city = new City();
        area = new Area();
        new CallRequest(instance).getState();

        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        states_id = String.valueOf(statesArray.get(position).getID());
                        new CallRequest(instance).getCity(states_id);
                    } else {
                        states_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        city_id = String.valueOf(cityArray.get(position).getID());
                        new CallRequest(instance).getArea(city_id);

                    } else {
                        city_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        area_id = String.valueOf(areaArray.get(position).getID());

                    } else {
                        area_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        setData(distributor);



    }

    public void Validaton() {
        App.addMemberObj.setAddress(et_address.getText().toString());
         App.addMemberObj.setState(states_id);
        App.addMemberObj.setCity(city_id);
        App.addMemberObj.setArea(area_id);
        App.addMemberObj.setZipcode(et_pincode.getText().toString());
        startActivity(new Intent(instance, DistributorEditThreeActivity.class)
                .putExtra("distributor", distributor));

    }

    private void setData(Distributor mainUser) {
        et_address.setText(mainUser.getAddress().toString());
        et_pincode.setText(mainUser.getZipcode().toString());
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case getState:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status") == true) {
                            strStatesArray.clear();
                            statesArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");

                            states.setValue("State");
                            states.setID("0");
                            strStatesArray.add(states.getValue());
                            statesArray.add(states);
                            JSONArray jStateArray = jData.getJSONArray("State");
                            if (jStateArray != null && jStateArray.length() > 0) {
                                for (int i = 0; i < jStateArray.length(); i++) {
                                    states = (States) jParser.parseJson(jStateArray.getJSONObject(i), new States());
                                    if (distributor.getState().equalsIgnoreCase(states.getID()))
                                        selcted_state = i;
                                    statesArray.add(states);
                                    strStatesArray.add(states.getValue());
                                }
                                sp_state.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strStatesArray));
                                sp_state.setSelection(selcted_state + 1);
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getCity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status") == true) {
                            strCityArray.clear();
                            cityArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");

                            city.setValue("City");
                            city.setID("0");
                            strCityArray.add(city.getValue());
                            cityArray.add(city);
                            JSONArray jCityArray = jData.getJSONArray("CIty");
                            if (jCityArray != null && jCityArray.length() > 0) {
                                for (int i = 0; i < jCityArray.length(); i++) {
                                    city = (City) jParser.parseJson(jCityArray.getJSONObject(i), new City());

                                    if (distributor.getCityName().equalsIgnoreCase(city.getValue()))
                                        selcted_city = i;
                                    cityArray.add(city);
                                    strCityArray.add(city.getValue());
                                }
                                sp_city.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));
                                sp_city.setSelection(selcted_city + 1);
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getArea:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status") == true) {
                            strAreaArray.clear();
                            areaArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");

                            area.setValue("Area");
                            area.setID("0");
                            strAreaArray.add(area.getValue());
                            areaArray.add(area);
                            JSONArray jAreaArray = jData.getJSONArray("Area");
                            if (jAreaArray != null && jAreaArray.length() > 0) {
                                for (int i = 0; i < jAreaArray.length(); i++) {
                                    area = (Area) jParser.parseJson(jAreaArray.getJSONObject(i), new Area());

                                    if (distributor.getAreaName().equalsIgnoreCase(area.getValue()))
                                        selcted_area = i;
                                    areaArray.add(area);
                                    strAreaArray.add(area.getValue());
                                }
                                sp_area.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strAreaArray));
                                sp_area.setSelection(selcted_area + 1);
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }


}
