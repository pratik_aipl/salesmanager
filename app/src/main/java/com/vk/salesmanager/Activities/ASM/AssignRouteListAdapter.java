package com.vk.salesmanager.Activities.ASM;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vk.salesmanager.Activities.Order.AssignRoute;
import com.vk.salesmanager.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 11/3/2018.
 */

class AssignRouteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private ArrayList<AssignRoute> mArrayList;
    public Context context;
    private ArrayList<AssignRoute> mFilteredList;

    public AssignRouteListAdapter(ArrayList<AssignRoute> moviesList, Context context) {
        mArrayList = moviesList;
        mFilteredList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_route_name, tv_date;
        public RelativeLayout rel_date;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_date = view.findViewById(R.id.tv_date);
            tv_route_name = view.findViewById(R.id.tv_route_name);
            rel_date = view.findViewById(R.id.rel_date);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_assign_route_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        AssignRoute order = mFilteredList.get(pos);
        holder.tv_date.setText(order.getRouteDate());
        holder.tv_name.setText(order.getFirstName() + " " + order.getLastName());
        holder.tv_route_name.setText(order.getRounteNo() + "   ( " + order.getFrom() + " - " + order.getTo()+" )");
        holder.setIsRecyclable(false);
        if (order.getRouteDate().equals("") || order.getRouteDate() == null) {
            holder.rel_date.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);


    }


    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<AssignRoute> filteredList = new ArrayList<>();

                    for (AssignRoute androidVersion : mArrayList) {

                        if (androidVersion.getLastName().toLowerCase().contains(charString)
                                || androidVersion.getFirstName().toLowerCase().contains(charString)
                                || androidVersion.getLastName().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<AssignRoute>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
