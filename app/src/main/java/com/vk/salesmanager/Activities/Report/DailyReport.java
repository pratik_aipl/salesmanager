package com.vk.salesmanager.Activities.Report;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 8/30/2018.
 */

public class DailyReport implements Serializable {
    public String Label = "", Date = "", Amount = "", Item = "";

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }
}
