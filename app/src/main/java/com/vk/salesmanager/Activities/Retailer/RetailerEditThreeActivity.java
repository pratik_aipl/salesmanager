package com.vk.salesmanager.Activities.Retailer;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RetailerEditThreeActivity extends AppCompatActivity implements AsynchTaskListner {
    public RetailerEditThreeActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_image_name;
    public RelativeLayout rel_browse;
    Uri selectedUri;
    public String logoPath = "";
    public EditText et_uniqe_number;
    public Button btn_addmember;
    public TextView tv_dob;
    final Calendar myCalendar = Calendar.getInstance();
    public Retailor retailor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member3);
        retailor = (Retailor) getIntent().getExtras().getSerializable("retailor");
        instance = RetailerEditThreeActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_image_name = findViewById(R.id.tv_image_name);
        tv_dob = findViewById(R.id.et_dob);
        rel_browse = findViewById(R.id.rel_browse);
        btn_addmember = findViewById(R.id.btn_addmember);
        et_uniqe_number = findViewById(R.id.et_uniqe_number);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Edit Reatailor");
        rel_browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        tv_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_dob.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());     datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        setData(retailor);

        btn_addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                App.addMemberObj.setUniqueIdentityNO(et_uniqe_number.getText().toString());
                App.addMemberObj.setDOB(tv_dob.getText().toString());
                App.addMemberObj.setUniqueIdentityImage(logoPath);
                App.addMemberObj.setUserId(retailor.getUserID());
                new CallRequest(instance).EditRetailer(App.addMemberObj.getUserId(),
                        App.addMemberObj.getFirstName(),
                        App.addMemberObj.getLastName(),
                        App.addMemberObj.getRetailerName(),
                        App.addMemberObj.getDOB(),
                        App.addMemberObj.getUniqueIdentityNO(),
                        App.addMemberObj.getAddress(),
                        App.addMemberObj.getArea(),
                        App.addMemberObj.getCity(),
                        App.addMemberObj.getState(),
                        App.addMemberObj.getZipcode(),
                        App.addMemberObj.getCompanyLocationID(),
                        App.addMemberObj.getEmail(),
                        App.addMemberObj.getMobile(),
                        App.addMemberObj.getUniqueIdentityImage(),
                        App.addMemberObj.getPhoto(),
                        App.addMemberObj.getSMUserID() ,
                        App.addMemberObj.getDistId());

            }
        });
    }

    private void setData(Retailor asm) {
       // et_uniqe_number.setText(asm.getUniqueIdentityNO().toString());
      //  tv_image_name.setText(asm.getUniqueIdentityURL().toString());
      //  tv_dob.setText(asm.getDOB().toString());

    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath().toString();

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    selectedUri = result.getUri();
                    logoPath = selectedUri.getPath().toString();
                    tv_image_name.setText(logoPath.substring(logoPath.lastIndexOf("/") + 1, logoPath.length()));
                    Log.i("TAG", "logoPath :-> " + logoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(2, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case EditRetailer:
                        Utils.hideProgressDialog();
                        final JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            Utils.showToast(jObj.getString("message"), instance);
                            startActivity(new Intent(instance, RetailorListActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        } else {
            Utils.showToast("Something went wrong", this);

        }
    }
}

