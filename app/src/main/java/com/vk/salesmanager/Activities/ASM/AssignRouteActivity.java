package com.vk.salesmanager.Activities.ASM;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.Models.Route;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AssignRouteActivity extends AppCompatActivity implements AsynchTaskListner {
    public Button btn_submit;
    public TextView tv_date;
    public Spinner sp_sm, sp_route;
    final Calendar myCalendar = Calendar.getInstance();

    public AssignRouteActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_image_name;

    public ArrayList<Route> routeArrayList = new ArrayList<>();
    public ArrayList<String> strRouteArrayList = new ArrayList<>();
//    public Route routeObj;

    public ArrayList<ASM> smArrayList = new ArrayList<>();
    public ArrayList<String> strSmArrayList = new ArrayList<>();

    public ASM smObj;
    public JsonParserUniversal jParser;
    public String sm_id = "", route_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_route);
        instance = AssignRouteActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_image_name = findViewById(R.id.tv_image_name);
        tv_date = findViewById(R.id.tv_date);
        sp_sm = findViewById(R.id.sp_sm);
        btn_submit = findViewById(R.id.btn_submit);
        sp_route = findViewById(R.id.sp_route);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Assign Route");


        tv_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMinDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        new CallRequest(instance).getSM();

        sp_sm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        sm_id = String.valueOf(smArrayList.get(position).getId());
                        new CallRequest(instance).getSmAssignRouteList(sm_id);
                    } else {
                        sm_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_route.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        route_id = String.valueOf(routeArrayList.get(position).getRouteID());

                    } else {
                        route_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_date.getText().toString().equals("")) {
                    tv_date.setError("Please enter date");
                    tv_date.setFocusable(true);
                } else if (sm_id.equals("0") || sm_id.equals("")) {
                    Utils.showToast("Please select sm", instance);
                } else if (route_id.equals("0") || route_id.equals("")) {
                    Utils.showToast("Please select route", instance);
                } else {
                    new CallRequest(instance).assign_route(tv_date.getText().toString(), route_id, sm_id);
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getSM:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            smObj = new ASM();
                            smObj.setFirstName(" Select SM");
                            smArrayList.add(smObj);
                            strSmArrayList.add(" Select SM");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("SM");
                            if (jarray.length() > 0 && jarray != null) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    smObj = (ASM) jParser.parseJson(jData, new ASM());
                                    smArrayList.add(smObj);
                                    strSmArrayList.add(smObj.getFirstName() + " " + smObj.getLastName());
                                }
                                sp_sm.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strSmArrayList));

                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getSmAssignRouteList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Route routeObj = new Route();
                            routeObj.setLocationName(" Select Route");
                            routeArrayList.clear();
                            strRouteArrayList.clear();
                            routeArrayList.add(routeObj);
                            strRouteArrayList.add(" Select Route");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("Route");
                            if (jarray.length() > 0 && jarray != null) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    Route route = (Route) jParser.parseJson(jData, new Route());
                                    routeArrayList.add(route);
                                    strRouteArrayList.add(route.getFrom() + " " + route.getTo());
                                }
                                sp_route.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strRouteArrayList));
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case assign_route:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {

                            Utils.showToast(jObj.getString("message"), instance);
                            setResult(RESULT_OK, new Intent());
                            finish();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
