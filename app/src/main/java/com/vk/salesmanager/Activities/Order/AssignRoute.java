package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 11/3/2018.
 */

public class AssignRoute implements Serializable {
    public String RouteSMID = "";
    public String RouteDate = "";
    public String SMID = "";
    public String RouteID = "";
    public String FirstName = "";
    public String LastName = "";
    public String From = "";
    public String To = "";

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public String getRounteNo() {
        return RounteNo;
    }

    public void setRounteNo(String rounteNo) {
        RounteNo = rounteNo;
    }

    public String RounteNo="";

    public String getRouteSMID() {
        return RouteSMID;
    }

    public void setRouteSMID(String routeSMID) {
        RouteSMID = routeSMID;
    }

    public String getRouteDate() {
        return RouteDate;
    }

    public void setRouteDate(String routeDate) {
        RouteDate = routeDate;
    }

    public String getSMID() {
        return SMID;
    }

    public void setSMID(String SMID) {
        this.SMID = SMID;
    }

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
