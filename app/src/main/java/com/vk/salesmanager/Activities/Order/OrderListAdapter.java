package com.vk.salesmanager.Activities.Order;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vk.salesmanager.Models.Order;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.Utils;

import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private ArrayList<Order> mArrayList;
    public Context context;
    private ArrayList<Order> mFilteredList;

    public OrderListAdapter(ArrayList<Order> moviesList, Context context) {
        mArrayList = moviesList;
        mFilteredList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_date, tv_order_name, tv_medical_name, tv_sm_name, tv_order_status;
        public RelativeLayout rel_date;

        public MyViewHolder(View view) {
            super(view);
            tv_date = view.findViewById(R.id.tv_date);
            tv_order_name = view.findViewById(R.id.tv_order_name);
            tv_medical_name = view.findViewById(R.id.tv_medical_name);
            tv_sm_name = view.findViewById(R.id.tv_sm_name);
            tv_order_status = view.findViewById(R.id.tv_order_status);
            rel_date = view.findViewById(R.id.rel_date);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_order_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        Order order = mFilteredList.get(pos);
        if(order.getDate().equalsIgnoreCase("Today")){
            holder.tv_date.setText(order.getDate());
        }else{
            holder.tv_date.setText(Utils.changeDateFormat(order.getDate()));
        }
        holder.tv_order_name.setText("Order # : " + order.getOrderNo());
        holder.tv_medical_name.setText(order.getRetailerName());
        holder.tv_sm_name.setText("Sales Manager : " + order.getSaleManagerFirstName() + " " + order.getSaleManagerLastName());
        holder.tv_order_status.setText(order.getOrderStatus());
        holder.setIsRecyclable(false);
        if (order.getDate().equals("") || order.getDate() == null) {
            holder.rel_date.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, OrderDetailActivity.class)
                        .putExtra("OrderHeaderID", mFilteredList.get(pos).getOrderHeaderID()));
            }
        });


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);


    }


    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<Order> filteredList = new ArrayList<>();

                    for (Order androidVersion : mArrayList) {

                        if (androidVersion.getLastName().toLowerCase().contains(charString)
                                || androidVersion.getFirstName().toLowerCase().contains(charString)
                                || androidVersion.getLastName().toLowerCase().contains(charString)
                                || androidVersion.getRetailerName().toLowerCase().contains(charString)
                                || androidVersion.getOrderStatus().toLowerCase().contains(charString)
                                || androidVersion.getOrderNo().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Order>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
