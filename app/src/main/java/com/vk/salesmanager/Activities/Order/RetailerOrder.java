package com.vk.salesmanager.Activities.Order;

import java.io.Serializable;

public class RetailerOrder implements Serializable {
    public String RetailerID = "", RetailerName = "";

    public String getRetailerID() {
        return RetailerID;
    }

    public void setRetailerID(String retailerID) {
        RetailerID = retailerID;
    }

    public String getRetailerName() {
        return RetailerName;
    }

    public void setRetailerName(String retailerName) {
        RetailerName = retailerName;
    }
}
