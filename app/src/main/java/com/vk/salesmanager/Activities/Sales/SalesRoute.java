package com.vk.salesmanager.Activities.Sales;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 11/3/2018.
 */

public class SalesRoute implements Serializable {
    public String RouteDate = "", SMID = "", RouteID = "", CompanyLocationID = "", From = "", To = "", LocationName = "", RounteNo = "";

    public String getRouteDate() {
        return RouteDate;
    }

    public void setRouteDate(String routeDate) {
        RouteDate = routeDate;
    }

    public String getSMID() {
        return SMID;
    }

    public void setSMID(String SMID) {
        this.SMID = SMID;
    }

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getCompanyLocationID() {
        return CompanyLocationID;
    }

    public void setCompanyLocationID(String companyLocationID) {
        CompanyLocationID = companyLocationID;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getRounteNo() {
        return RounteNo;
    }

    public void setRounteNo(String rounteNo) {
        RounteNo = rounteNo;
    }
}
