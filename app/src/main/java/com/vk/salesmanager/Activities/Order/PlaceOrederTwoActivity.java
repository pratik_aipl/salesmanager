package com.vk.salesmanager.Activities.Order;

import android.content.Intent;
import android.graphics.PorterDuff;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlaceOrederTwoActivity extends AppCompatActivity implements AsynchTaskListner {
    public PlaceOrederTwoActivity instance;
    public Spinner sp_product, sp_distributor;
    public Product productObj;
    public ArrayList<Product> productArrayList = new ArrayList<>();
    public ArrayList<String> strProductArray = new ArrayList<>();
    public JsonParserUniversal jParser;
    public String product_id = "", product_name = "";
    public Toolbar toolbar;
    public TempOrder tempOrderObj;
    public Button btn_add, btn_order;
    public EditText et_qty;
    public ArrayList<TempOrder> orderArrayList = new ArrayList<>();
    public boolean isNew = false;
    public RecyclerView rcyclerView;
    public AddOrderAdapter adapter;
    public LinearLayout lin_empty;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_oreder_two);
        instance = this;
        jParser = new JsonParserUniversal();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        sp_product = findViewById(R.id.sp_product);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_add = findViewById(R.id.btn_add);
        btn_order = findViewById(R.id.btn_order);
        et_qty = findViewById(R.id.et_qty);
        rcyclerView = findViewById(R.id.rcyclerView);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Place Order");
        new CallRequest(instance).getProduct();
        lin_empty = findViewById(R.id.lin_empty);
        lin_empty.setVisibility(View.GONE);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (product_id.equals("") || product_id.equals("0")) {
                    Utils.showToast("Please select Product", instance);
                } else if (et_qty.getText().toString().isEmpty()) {
                    et_qty.setError("plasse enter QTY.");
                    et_qty.setFocusable(true);
                } else {
                    Utils.hideKeyboard(instance,btn_add);
                    if (orderArrayList != null && orderArrayList.size() > 0) {

                        for (int i = 0; i < orderArrayList.size(); i++) {
                            if (orderArrayList.get(i).getItemID().equals(product_id)) {
                                isNew = false;
                                orderArrayList.get(i).setQTY(orderArrayList.get(i).getQTY() + Integer.parseInt(et_qty.getText().toString()));
                                break;
                            } else {
                                isNew = true;
                            }
                        }
                        if (isNew) {
                            tempOrderObj = new TempOrder();
                            tempOrderObj.setItemID(product_id);
                            tempOrderObj.setItemName(product_name);
                            tempOrderObj.setQTY(Integer.parseInt(et_qty.getText().toString()));
                            orderArrayList.add(tempOrderObj);
                        }
                    } else {
                        tempOrderObj = new TempOrder();
                        tempOrderObj.setItemID(product_id);
                        tempOrderObj.setItemName(product_name);
                        tempOrderObj.setQTY(Integer.parseInt(et_qty.getText().toString()));
                        orderArrayList.add(tempOrderObj);

                    }
                    SetupRecylerView();
                }
            }
        });
        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderArrayList != null && orderArrayList.size() > 0) {
                    App.addOrderObj.tempOrderArrayList = orderArrayList;
                    startActivity(new Intent(instance, PlaceOrderThreeActivity.class));

                } else {
                    Utils.showToast("Please enter order", instance);
                }
            }
        });
    }

    public void SpinerClickEvent() {
        sp_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        product_id = String.valueOf(productArrayList.get(position).getItemID());
                        product_name = productArrayList.get(position).getItemName();
                        et_qty.getText().clear();
                    } else {
                        product_id = "0";
                        product_name = "";
                    }
                } catch (Exception e) {
                    product_id = "0";
                    product_name = "";
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void SetupRecylerView() {

        adapter = new AddOrderAdapter(orderArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rcyclerView.setLayoutAnimation(controller);
        rcyclerView.scheduleLayoutAnimation();
        rcyclerView.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rcyclerView.setItemAnimator(new DefaultItemAnimator());
        rcyclerView.setAdapter(adapter);
        strProductArray.clear();
        et_qty.getText().clear();
        new CallRequest(instance).getProduct();
       // sp_product.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strProductArray));
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getProduct:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            strProductArray.clear();
                            productArrayList.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            productObj = new Product();
                            productObj.setItemName("Select Product");
                            productObj.setItemID("0");
                            strProductArray.add(productObj.getItemName());
                            productArrayList.add(productObj);
                            JSONArray jStateArray = jData.getJSONArray("Products");
                            if (jStateArray != null && jStateArray.length() > 0) {
                                for (int i = 0; i < jStateArray.length(); i++) {
                                    productObj = (Product) jParser.parseJson(jStateArray.getJSONObject(i), new Product());
                                    productArrayList.add(productObj);
                                    strProductArray.add(productObj.getItemName());
                                }
                                sp_product.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strProductArray));
                                SpinerClickEvent();
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
