package com.vk.salesmanager.Activities.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.vk.salesmanager.Models.ClosingStock;
import com.vk.salesmanager.Models.DSRCheckOutDetails;
import com.vk.salesmanager.Models.DSRDetails;
import com.vk.salesmanager.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DSRDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DSRDetails> mArrayList;
    public Context context;
    DSRSubListAdapter  dsrsublistadapter;

    public DSRDetailsAdapter(ArrayList<DSRDetails> moviesList, Context context) {
        mArrayList = moviesList;
         this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_checkin;
        RecyclerView recyclerView;

        public MyViewHolder(View view) {
            super(view);
            recyclerView = view.findViewById(R.id.dsr_rcyclerView);
            tv_checkin = view.findViewById(R.id.tv_checkin);
          }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dsr_details_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        DSRDetails order = mArrayList.get(pos);
        holder.tv_checkin.setText("Check-In :  "+order.getCheckIn());

        dsrsublistadapter = new DSRSubListAdapter(order.getDsrCheckOutDetails(), context);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        holder.recyclerView.setLayoutAnimation(controller);
        holder.recyclerView.scheduleLayoutAnimation();
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.recyclerView.setAdapter(dsrsublistadapter);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

}
