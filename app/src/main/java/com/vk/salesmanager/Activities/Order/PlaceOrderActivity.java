package com.vk.salesmanager.Activities.Order;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class PlaceOrderActivity extends AppCompatActivity implements AsynchTaskListner {
    public PlaceOrderActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_date;
    public String retailer_id = "", distributor_id = "", distributor_name = "", retailer_name = "";
    public Spinner sp_retalier, sp_distributor;
    public RetailerOrder retailerOrder;
    public ArrayList<RetailerOrder> retailerArrayList = new ArrayList<>();
    public ArrayList<String> strretailerArray = new ArrayList<>();
    public DistributorOrder distributorOrder;
    public ArrayList<DistributorOrder> distributorArrayList = new ArrayList<>();
    public ArrayList<String> strDistributorArray = new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    public JsonParserUniversal jParser;

    public Button btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        instance = PlaceOrderActivity.this;
        jParser = new JsonParserUniversal();
        App.addOrderObj = new AddOrder();
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_next = findViewById(R.id.btn_next);
        sp_retalier = findViewById(R.id.sp_retalier);
        sp_distributor = findViewById(R.id.sp_distributor);

        new CallRequest(instance).getRetailor();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Place Order");
        tv_date = findViewById(R.id.tv_date);
        tv_date.setText(new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime()));


      /*  tv_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    //String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                   // SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                                    tv_date.setText(df.format(Calendar.getInstance().getTime()));

                                }
                            };
                         *//*   DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMinDate(temp.getTimeInMillis());
                            datePickerDialog.show(); *//*
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });*/
        sp_retalier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        retailer_id = String.valueOf(retailerArrayList.get(position).getRetailerID());
                        retailer_name = String.valueOf(retailerArrayList.get(position).getRetailerName());
                        new CallRequest(instance).getDistributorOrder(retailer_id);
                    } else {
                        retailer_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (retailer_id.equals("") || retailer_id.equals("0")) {
                    Utils.showToast("Please select retailer", instance);
                } else if (distributor_id.equals("") || distributor_id.equals("0")) {
                    Utils.showToast("Please select retailer", instance);
                } else if (tv_date.getText().toString().isEmpty()) {
                    tv_date.setError("pleease select date");
                } else {
                    App.addOrderObj.setDate(tv_date.getText().toString().trim());
                    App.addOrderObj.setDistributorID(distributor_id);
                    App.addOrderObj.setRetailerID(retailer_id);
                    App.addOrderObj.setDistributorName(distributor_name);
                    App.addOrderObj.setRetailerName(retailer_name);
                    startActivity(new Intent(instance, PlaceOrederTwoActivity.class));
                }
            }
        });
        SpinerClickEvent();
    }

    public void SpinerClickEvent() {

        sp_distributor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        distributor_id = String.valueOf(distributorArrayList.get(position).getDistributorID());
                        distributor_name = String.valueOf(distributorArrayList.get(position).getDistributorName());
                    } else {
                        distributor_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getRetailor:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            strretailerArray.clear();
                            retailerArrayList.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            retailerOrder = new RetailerOrder();
                            retailerOrder.setRetailerName("Select Retailer");
                            retailerOrder.setRetailerID("0");
                            strretailerArray.add(retailerOrder.getRetailerName());
                            retailerArrayList.add(retailerOrder);
                            JSONArray jStateArray = jData.getJSONArray("Retailer");
                            if (jStateArray != null && jStateArray.length() > 0) {
                                for (int i = 0; i < jStateArray.length(); i++) {
                                    retailerOrder = (RetailerOrder) jParser.parseJson(jStateArray.getJSONObject(i), new RetailerOrder());
                                    retailerArrayList.add(retailerOrder);
                                    strretailerArray.add(retailerOrder.getRetailerName());
                                }
                                sp_retalier.setAdapter(new ArrayAdapter<>(instance, android.R.layout.simple_spinner_dropdown_item, strretailerArray));

                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

                case getDistributorOrder:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            strretailerArray.clear();
                            retailerArrayList.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            distributorOrder = new DistributorOrder();
                            distributorOrder.setDistributorName("Select Distributor");
                            distributorOrder.setDistributorID("0");
                            strDistributorArray.add(distributorOrder.getDistributorName());
                            distributorArrayList.add(distributorOrder);
                            JSONArray jStateArray = jData.getJSONArray("list");
                            if (jStateArray != null && jStateArray.length() > 0) {
                                for (int i = 0; i < jStateArray.length(); i++) {
                                    distributorOrder = (DistributorOrder) jParser.parseJson(jStateArray.getJSONObject(i), new DistributorOrder());
                                    distributorArrayList.add(distributorOrder);
                                    strDistributorArray.add(distributorOrder.getDistributorName());
                                }
                                sp_distributor.setAdapter(new ArrayAdapter<>(instance, android.R.layout.simple_spinner_dropdown_item, strDistributorArray));

                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
