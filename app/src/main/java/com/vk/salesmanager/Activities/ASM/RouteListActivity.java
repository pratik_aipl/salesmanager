package com.vk.salesmanager.Activities.ASM;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.Route;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RouteListActivity extends AppCompatActivity implements AsynchTaskListner {

    public RouteListActivity instance;
    public FloatingActionButton float_add;
    public Toolbar toolbar, toolbar_search;
    public LinearLayout lin_empty;
    public RecyclerView rv_product_list;
    public ArrayList<Route> routeArrayList = new ArrayList<>();
    public ImageView img_back, img_search;
    private SearchView mSearchView;
    public JsonParserUniversal jParser;
    public RouteListAdapter adapter;
    public TextView tv_title;
    int RESULT_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_list);
        instance = RouteListActivity.this;
        jParser = new JsonParserUniversal();
        App.indexListArea.clear();
        rv_product_list = findViewById(R.id.rcyclerView);
        img_search = findViewById(R.id.img_search);
        img_back = findViewById(R.id.img_back);
        mSearchView = findViewById(R.id.searchView1);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        float_add = findViewById(R.id.float_add);
        toolbar_search = findViewById(R.id.toolbar_search);
        lin_empty = findViewById(R.id.lin_empty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setSupportActionBar(toolbar_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_search.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbar_search.setVisibility(View.GONE);
                toolbar.setVisibility(View.VISIBLE);
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
            }
        });
        tv_title.setText("Route list");
        mSearchView.setFocusable(false);
        search(mSearchView);
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbar_search.setVisibility(View.VISIBLE);
                toolbar.setVisibility(View.GONE);
                mSearchView.setFocusable(true);
                mSearchView.onActionViewExpanded();
            }
        });
        if (App.mainUser.getRole_id().equalsIgnoreCase("4")) {
            //sm
            float_add.setVisibility(View.GONE);
        }
        if (App.mainUser.getRole_id().equalsIgnoreCase("5")) {
            //distributor
            float_add.setVisibility(View.GONE);
        }
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(instance, DrawRouteActivity.class), RESULT_CODE = 1);

            }
        });

        getroute();
    }

    private void getroute() {
        new CallRequest(instance).getRoute();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_CODE) {
            if(resultCode == Activity.RESULT_OK){
                getroute();
            }
        }
    }
    public void search(SearchView searchView) {
        searchView.setQueryHint("Search...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    public void SetupRecylerView() {

        adapter = new RouteListAdapter(routeArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_product_list.setLayoutAnimation(controller);
        rv_product_list.scheduleLayoutAnimation();
        rv_product_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_product_list.setItemAnimator(new DefaultItemAnimator());
        rv_product_list.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getRoute:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {
                            App.mySharedPref.clearApp();
                            Intent i = new Intent(instance, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else if (respCode.equalsIgnoreCase("200")) {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                JSONObject jdata = jObj.getJSONObject("data");
                                JSONArray jarray = jdata.getJSONArray("Route");
                                if (jarray.length() > 0 && jarray != null) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_product_list.setVisibility(View.VISIBLE);
                                    routeArrayList.clear();
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                       Route route = (Route) jParser.parseJson(jData, new Route());
                                        routeArrayList.add(route);
                                    }
                                    SetupRecylerView();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_product_list.setVisibility(View.GONE);
                            }
                        } else {
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_product_list.setVisibility(View.GONE);
                            Utils.showToast("Server side error...", this);
                        }
                        break;
                }
            } catch (JSONException e) {
                lin_empty.setVisibility(View.VISIBLE);
                rv_product_list.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        } else {
            lin_empty.setVisibility(View.VISIBLE);
            rv_product_list.setVisibility(View.GONE);
            Utils.hideProgressDialog();

        }
    }

}
