package com.vk.salesmanager.Activities.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vk.salesmanager.Models.ClosingStock;
import com.vk.salesmanager.Models.Help;
import com.vk.salesmanager.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BalanceQTYAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ClosingStock> mArrayList;
    public Context context;

    public BalanceQTYAdapter(ArrayList<ClosingStock> moviesList, Context context) {
        mArrayList = moviesList;
         this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_p_name, tv_po_name,tv_td_name,tv_tp_name,tv_bq_name;

        public MyViewHolder(View view) {
            super(view);
            tv_p_name = view.findViewById(R.id.tv_p_name);
            tv_po_name = view.findViewById(R.id.tv_po_name);
            tv_td_name = view.findViewById(R.id.tv_td_name);
            tv_tp_name = view.findViewById(R.id.tv_tp_name);
            tv_bq_name = view.findViewById(R.id.tv_bq_name);
          }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.blnc_qty_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        ClosingStock order = mArrayList.get(pos);
        holder.tv_p_name.setText(order.getItemName());
        holder.tv_tp_name.setText(order.getQty());
        holder.tv_bq_name.setText(""+order.getBalancedQTY());
        holder.tv_td_name.setText(""+order.getDelieveredQTY());
        holder.tv_po_name.setText(""+order.getPendingDelieveredQTY());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);


    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


}
