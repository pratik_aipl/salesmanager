package com.vk.salesmanager.Activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.vk.salesmanager.App;
import com.vk.salesmanager.BuildConfig;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.MainUser;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity implements AsynchTaskListner {

    public LoginActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_forgot;
    public Button btn_login, btn_next;
    public LinearLayout lin_password, lin_number;
    public EditText et_mobile, et_password;
    public String device_id = "";
    public MainUser mainUser;
    public JsonParserUniversal jParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        instance = LoginActivity.this;
        device_id = Utils.OneSignalPlearID();
        jParser = new JsonParserUniversal();

        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_forgot = findViewById(R.id.tv_forgot);
        btn_login = findViewById(R.id.btn_login);
        btn_next = findViewById(R.id.btn_next);
        lin_password = findViewById(R.id.lin_password);
        lin_number = findViewById(R.id.lin_number);
        et_mobile = findViewById(R.id.et_mobile);
        et_password = findViewById(R.id.et_password);

        if (BuildConfig.DEBUG)
            et_password.setText("123456");

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_mobile.getText().toString().isEmpty()) {
                    et_mobile.setError("Please enter your registed mobile number");
                    et_mobile.setFocusable(true);
                } else {
                    new CallRequest(instance).checkUser(et_mobile.getText().toString());
                }
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Login");

        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_password.getText().toString().equals("")) {
                    et_password.setError("Please enter valid password");
                    et_password.setFocusable(true);
                } else {
                    new CallRequest(instance).getLogin(et_mobile.getText().toString(), et_password.getText().toString(), device_id);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (lin_password.getVisibility() == View.VISIBLE) {
            lin_password.setVisibility(View.GONE);
            Animation slideUp = AnimationUtils.loadAnimation(instance, R.anim.slide_right);
            lin_number.setVisibility(View.VISIBLE);
            lin_number.startAnimation(slideUp);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case checkUser:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);


                        if (respCode.equalsIgnoreCase("403")) {
                            if (jObj.getBoolean("status")) {
                                Utils.showToast(jObj.getString("message"), this);
                                JSONObject jData = jObj.getJSONObject("data");
                                App.mobile_no = jData.getString("mobile");
                                App.otp = jData.getString("OTP");
                                Intent intent = new Intent(instance, ConfirmOTPActivity.class);
                                startActivity(intent);
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        } else if (respCode.equalsIgnoreCase("200")) {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                Utils.showToast(jObj.getString("message"), this);
                                JSONObject jData = jObj.getJSONObject("data");
                                Animation slideUp = AnimationUtils.loadAnimation(instance, R.anim.slide_left);

                                if (lin_password.getVisibility() == View.GONE) {
                                    lin_number.setVisibility(View.GONE);
                                    lin_password.setVisibility(View.VISIBLE);
                                    lin_password.startAnimation(slideUp);

                                }
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), this);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;

                    case getLogin:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONObject jData = jObj.getJSONObject("data");
                            JSONObject jUser = jData.getJSONObject("user");

                            mainUser = (MainUser) jParser.parseJson(jUser, new MainUser());
                            Gson gson = new Gson();
                            String json = gson.toJson(mainUser);

                            App.mySharedPref.setUserModel(json);
                            App.mySharedPref.setUserId(jData.getString("user_id"));
                            App.mySharedPref.setLoginToken(jData.getString("login_token"));
                            App.mySharedPref.setIsLoggedIn(true);


                            Utils.showToast(jObj.getString("message"), this);
                            Intent intent = new Intent(instance, DashBoardActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }
}
