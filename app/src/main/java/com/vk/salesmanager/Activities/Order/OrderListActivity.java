package com.vk.salesmanager.Activities.Order;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.Order;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderListActivity extends AppCompatActivity implements AsynchTaskListner {
    public OrderListActivity instance;
    public FloatingActionButton float_add;
    public Toolbar toolbar, toolbar_search;

    public LinearLayout lin_empty;
    public RecyclerView rv_product_list;
    public ArrayList<Order> orderArrayList = new ArrayList<>();
    public Order order;
    public ImageView img_back, img_search;
    private SearchView mSearchView;
    public JsonParserUniversal jParser;
    public OrderListAdapter adapter;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_and_add_member);
        instance = OrderListActivity.this;
        jParser = new JsonParserUniversal();

        rv_product_list = findViewById(R.id.rcyclerView);
        img_search = findViewById(R.id.img_search);
        img_back = findViewById(R.id.img_back);
        mSearchView = findViewById(R.id.searchView1);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        float_add = findViewById(R.id.float_add);
        float_add.setVisibility(View.GONE);
        toolbar_search = findViewById(R.id.toolbar_search);
        lin_empty = findViewById(R.id.lin_empty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setSupportActionBar(toolbar_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_search.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbar_search.setVisibility(View.GONE);
                toolbar.setVisibility(View.VISIBLE);
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
            }
        });
        tv_title.setText("Sales Order list");
        mSearchView.setFocusable(false);
        search(mSearchView);


        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, PlaceOrderActivity.class));
            }
        });
        // new CallRequest(instance).getDistributorOrder();
    }

    public void search(SearchView searchView) {
        searchView.setQueryHint("Search...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.mainUser.getRole_id().equalsIgnoreCase("2")) {
//distributor
            new CallRequest(instance).getRsmOrder();
            float_add.setVisibility(View.GONE);
        }if (App.mainUser.getRole_id().equalsIgnoreCase("5")) {
//distributor
            new CallRequest(instance).getDistributorOrder();
            float_add.setVisibility(View.GONE);
        }
        if (App.mainUser.getRole_id().equalsIgnoreCase("3")) {
//asm
            new CallRequest(instance).getAsmOrder();
            float_add.setVisibility(View.GONE);
        }
        if (App.mainUser.getRole_id().equalsIgnoreCase("4")) {
//sm
            new CallRequest(instance).getSmOrder();
            float_add.setVisibility(View.VISIBLE);
        } }

    public void SetupRecylerView() {

        Log.i("Size", "==>" + orderArrayList.size() + "");
         adapter = new OrderListAdapter(orderArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_product_list.setLayoutAnimation(controller);
        rv_product_list.scheduleLayoutAnimation();
        rv_product_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_product_list.setItemAnimator(new DefaultItemAnimator());
        rv_product_list.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getDistributorOrder:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") ) {
                             orderArrayList.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            JSONArray jOrders = jData.getJSONArray("Orders");
                                if (jOrders != null && jOrders.length() > 0) {
                            lin_empty.setVisibility(View.GONE);
                            rv_product_list.setVisibility(View.VISIBLE);
                            for (int i = 0; i < jOrders.length(); i++) {
                                JSONObject jD = jOrders.getJSONObject(i);
                                JSONArray jinerData = jD.getJSONArray("data");
                                for (int j = 0; j < jinerData.length(); j++) {

                                    order = (Order) jParser.parseJson(jinerData.getJSONObject(j), new Order());
                                    if (j == 0) {
                                        order.setDate(jD.getString("date"));
                                    }
                                    orderArrayList.add(order);
                                }
                            }
                                    if(orderArrayList.size()<0){
                                        img_search.setClickable(false);
                                    }else{
                                        img_search.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                toolbar_search.setVisibility(View.VISIBLE);
                                                toolbar.setVisibility(View.GONE);
                                                mSearchView.setFocusable(true);
                                                mSearchView.onActionViewExpanded();
                                            }
                                        });
                                    }
                            SetupRecylerView();
                             } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}


