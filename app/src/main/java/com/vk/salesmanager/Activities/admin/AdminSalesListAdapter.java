package com.vk.salesmanager.Activities.admin;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.salesmanager.BuildConfig;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ayaz-Baharuni on 7/11/2020.
 */

public class AdminSalesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private ArrayList<ASM> mArrayList;
    public Context context;
    private ArrayList<ASM> mFilteredList;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;

    public AdminSalesListAdapter(ArrayList<ASM> moviesList, Context context) {
        mArrayList = moviesList;
        mFilteredList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_state, tv_name, tv_mobile,tv_date,tv_sales_price;
        public LinearLayout lin_info,lin_admin_sales;
        public CircleImageView img_profile;

        public MyViewHolder(View view) {
            super(view);
            tv_state = view.findViewById(R.id.tv_state);
            tv_name = view.findViewById(R.id.tv_name);
            tv_mobile = view.findViewById(R.id.tv_mobile);
            img_profile = view.findViewById(R.id.img_profile);

            tv_date = view.findViewById(R.id.tv_date);
            tv_sales_price = view.findViewById(R.id.tv_sales_price);
            lin_admin_sales = view.findViewById(R.id.lin_admin_sales);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        public TextView tv_state, tv_name, tv_mobile,tv_date,tv_sales_price;
        public LinearLayout lin_info,lin_admin_sales;
        public CircleImageView img_profile;

        public ViewHolderFooter(View view) {
            super(view);
            tv_state = view.findViewById(R.id.tv_state);
            tv_name = view.findViewById(R.id.tv_name);
            tv_mobile = view.findViewById(R.id.tv_mobile);
            img_profile = view.findViewById(R.id.img_profile);

            tv_date = view.findViewById(R.id.tv_date);
            tv_sales_price = view.findViewById(R.id.tv_sales_price);
            lin_admin_sales = view.findViewById(R.id.lin_admin_sales);
        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {
        holder.lin_admin_sales.setVisibility(View.VISIBLE);
        ASM asmObj = mFilteredList.get(pos);
        holder.tv_name.setText(asmObj.getFirstName() + " " + asmObj.getLastName());
        holder.tv_mobile.setText(asmObj.getMobile());
        holder.tv_state.setText(asmObj.getArea());

        holder.tv_date.setText(asmObj.getOrderDate());
        holder.tv_sales_price.setText("RS "+asmObj.getTotalSales());

        Picasso.with(context)
                .load(BuildConfig.API_URL + asmObj.getPhotoURL())
                .error(R.drawable.profile)
                .into(holder.img_profile);


    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        holder.lin_admin_sales.setVisibility(View.VISIBLE);

        ASM asmObj = mFilteredList.get(pos);
        holder.tv_name.setText(asmObj.getFirstName() + " " + asmObj.getLastName());
        holder.tv_mobile.setText(asmObj.getMobile());
        holder.tv_state.setText(asmObj.getArea());

        holder.tv_date.setText(asmObj.getOrderDate());
        holder.tv_sales_price.setText("RS "+asmObj.getTotalSales());

        Picasso.with(context)
                .load(BuildConfig.API_URL + asmObj.getPhotoURL())
                .error(R.drawable.profile)
                .into(holder.img_profile);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == (mFilteredList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<ASM> filteredList = new ArrayList<>();

                    for (ASM androidVersion : mArrayList) {

                        if (androidVersion.getLastName().toLowerCase().contains(charString)
                                || androidVersion.getFirstName().toLowerCase().contains(charString)
                                || androidVersion.getLastName().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ASM>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}

