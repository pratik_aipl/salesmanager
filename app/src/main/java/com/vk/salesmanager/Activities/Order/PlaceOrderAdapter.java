package com.vk.salesmanager.Activities.Order;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vk.salesmanager.Models.Items;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 8/27/2018.
 */

public class PlaceOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private ArrayList<Items> mFilteredList;

    public PlaceOrderAdapter(ArrayList<Items> moviesList, Context context) {

        mFilteredList = moviesList;
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_place_order, parent, false));

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_no, tv_product;
        public EditText et_quantity;

        public MyViewHolder(View view) {
            super(view);
            tv_no = view.findViewById(R.id.tv_no);
            tv_product = view.findViewById(R.id.tv_product);
            et_quantity = view.findViewById(R.id.et_quantity);

        }
    }


    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final Items asmObj = mFilteredList.get(pos);
        holder.tv_no.setText(String.valueOf(pos + 1));
        holder.tv_product.setText(asmObj.getItemName());
        holder.et_quantity.setHint("Max " + String.valueOf(asmObj.getQTY()));

        holder.et_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int q = 0, totalQ = 0;
                if (!asmObj.getQTY().equals(""))
                    totalQ = Integer.parseInt(asmObj.getQTY());
                if (!s.toString().equals(""))
                    q = Integer.parseInt(s.toString());
                if (q > totalQ) {
                    holder.et_quantity.setText(s.toString().substring(0, s.length() - 1));
                    holder.et_quantity.setSelection(holder.et_quantity.length());

                    Utils.showToast("Should not be more than " + totalQ, context);
                } else {
                    mFilteredList.get(pos).setEnterQty(holder.et_quantity.getText().toString());
                }
            }
        });
    }

    public ArrayList<PlaceOrder> getSelQuestionTypesArray() {
        PlaceOrder obj = new PlaceOrder();
        ArrayList<PlaceOrder> array = new ArrayList<>();
        Items types = null;
        for (int i = 0; i < mFilteredList.size(); i++) {
            types = mFilteredList.get(i);

            if (!types.getEnterQty().equals("")) {
                obj = new PlaceOrder();
                obj.setOrderDetailID(mFilteredList.get(i).getOrderDetailID());
                obj.setDelieveredQTY(mFilteredList.get(i).getEnterQty());
                array.add(obj);
            }
        }
        return array;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder addrHolder = (MyViewHolder) holder;
        bindMyViewHolder(addrHolder, position);
    }


    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    public void setToAskAlert(View view, int i) {
        CardView card = (CardView) view;
        RelativeLayout rel = (RelativeLayout) card.getChildAt(0);
        LinearLayout lin_one = (LinearLayout) rel.getChildAt(2);
        LinearLayout lin_two = (LinearLayout) lin_one.getChildAt(0);
        LinearLayout lin_three = (LinearLayout) lin_two.getChildAt(1);

        EditText et = (EditText) lin_three.getChildAt(0);

        //EditText et = (EditText) lin_two.getChildAt(0);
        et.setError("Please Enter Questions To ASK ");
        et.requestFocus();


    }
}
