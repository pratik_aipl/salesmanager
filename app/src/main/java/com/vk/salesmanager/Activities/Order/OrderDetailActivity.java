package com.vk.salesmanager.Activities.Order;

import android.app.Dialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.Items;
import com.vk.salesmanager.Models.Order;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderDetailActivity extends AppCompatActivity implements AsynchTaskListner {
    public OrderDetailActivity instance;
    public String OrderHeaderID;
    public TextView tv_order_id, tv_retailr_name, tv_sales_maneger, tv_order_status;
    public Order order;
    public JsonParserUniversal jParser;
    public Toolbar toolbar;
    public TextView tv_title;
    public Items items;
    public RecyclerView recyclerView;
    public ConfirmOrderAdapter confirmAdapter;
    public PlaceOrderAdapter placeAdapter;
    public Button btn_cancel, btn_confirm, btn_otpconfirm;
    public Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        instance = OrderDetailActivity.this;
        jParser = new JsonParserUniversal();
        OrderHeaderID = getIntent().getStringExtra("OrderHeaderID");
        tv_order_id = findViewById(R.id.tv_order_id);
        tv_retailr_name = findViewById(R.id.tv_retailr_name);
        tv_sales_maneger = findViewById(R.id.tv_sales_maneger);
        tv_order_status = findViewById(R.id.tv_order_status);
        recyclerView = findViewById(R.id.rcyclerView);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_otpconfirm = findViewById(R.id.btn_otpconfirm);

        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Order Detail");
        new CallRequest(instance).getOrderDetail(OrderHeaderID);


    }

    public void SetupRecylerView() {

        confirmAdapter = new ConfirmOrderAdapter(order.itemsArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(confirmAdapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getOrderDetail:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            JSONObject jData = jObj.getJSONObject("data");
                            JSONObject jOrders = jData.getJSONObject("Order");

                            order = (Order) jParser.parseJson(jOrders, new Order());
                            //  order.itemsArrayList.clear();
                            JSONArray jItems = jOrders.getJSONArray("Items");
                            for (int i = 0; i < jItems.length(); i++) {
                                items = (Items) jParser.parseJson(jItems.getJSONObject(i), new Items());
                                order.itemsArrayList.add(items);
                            }
                            setData(order);
                            SetupRecylerView();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                    case orderConfirm:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), this);
                            new CallRequest(instance).getOrderDetail(OrderHeaderID);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                    case orderCancel:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), this);
                            onBackPressed();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Something went wrong", this);
                e.printStackTrace();
            }
        } else {
            Utils.showToast("Something went wrong", this);

        }
    }

    private void setData(final Order order) {
        tv_order_id.setText("Order # : " + order.getOrderNo());
        tv_retailr_name.setText(order.getRetailerName());
        tv_sales_maneger.setText("Sales Maneger : " + order.getSaleManagerFirstName() + " " + order.getSaleManagerLastName());
        tv_order_status.setText("Order Status : " + order.getOrderStatus());
        if (App.mainUser.getRole_id().equalsIgnoreCase("5")) {
            if (order.getOrderStatus().equalsIgnoreCase("Confirm")) {
                btn_confirm.setVisibility(View.VISIBLE);
                btn_cancel.setVisibility(View.VISIBLE);
                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new CallRequest(instance).orderCancel(order.getOrderHeaderID());
                    }
                });
                btn_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertConfirm();
                    }
                });
            }


        }
        if (App.mainUser.getRole_id().equalsIgnoreCase("4")) {
         /*   if (order.getOrderStatus().equalsIgnoreCase("Pending")) {
                btn_otpconfirm.setVisibility(View.VISIBLE);

                btn_otpconfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(instance, VerifyOrderActivity.class)
                                .putExtra("OrderHeaderID", OrderHeaderID)
                                .putExtra("OTP", ""));
                       *//* startActivity(new Intent(instance, VerifyOrderActivity.class)
                                .putExtra("OrderHeaderID", OrderHeaderID));*//*

                    }
                });
            }*/
        }
    }

    public void alertConfirm() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_place_order);
        RecyclerView recyclerView = dialog.findViewById(R.id.rcyclerView);
        Button btn_place_order = dialog.findViewById(R.id.btn_place_order);
        btn_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (placeAdapter.getSelQuestionTypesArray().size() > 0) {
                    gson = new Gson();
                    JsonArray json = gson.toJsonTree(placeAdapter.getSelQuestionTypesArray()).getAsJsonArray();
                    Log.i("TAG", "=>" + json.toString());

                    new CallRequest(instance).orderConfirm(json.toString());
                    dialog.dismiss();
                } else {
                    Utils.showToast("Please select one", instance);
                }

            }
        });
        dialog.show();
        setUpAlertRecycleView(recyclerView);

    }

    private void setUpAlertRecycleView(RecyclerView recyclerView) {
        placeAdapter = new PlaceOrderAdapter(order.itemsArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(placeAdapter);
    }
}
