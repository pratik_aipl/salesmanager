package com.vk.salesmanager.Activities.Distributor;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.Models.AddMember;
import com.vk.salesmanager.Models.CompanyLocation;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MultiSelectionSpinner;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.vk.salesmanager.Utils.Constant.FILTER_TYPE.SHIPTYPE;

public class DistributorAddOneActivity extends AppCompatActivity implements AsynchTaskListner, MultiSelectionSpinner.OnMultipleItemsSelectedListener {
    public DistributorAddOneActivity instance;
    public FloatingActionButton float_right_button;
    public EditText et_first_name, et_last_name, et_email, et_mobile, et_distributor;
    public TextView tv_title, tv_detail_name;
    final Calendar myCalendar = Calendar.getInstance();
    public Toolbar toolbar;
    Uri selectedUri;
    public String logoPath = "";
    public CircleImageView img_profile;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;
    public MultiSelectionSpinner sp_company_location;
    public MultiSelectionSpinner sp_ss_list;
    public CompanyLocation companyLocation;
    public ArrayList<CompanyLocation> companyLocationArray = new ArrayList<>();
    public ArrayList<String> strCompanyLocationArray = new ArrayList<>();
    public String CompanyLocationID = "";
    public JsonParserUniversal jParser;
    public ASM asm, SS;
    public LinearLayout lin_distributor, lin_asm_tv;
    public TextView txtarea, tv_asm;


    public Spinner sp_asm, sp_ss;
    LinearLayout lin_asm_sp, lin_ss_sp;
    public String ASM_id = "", SS_id = "";

    public ArrayList<ASM> asmArrayList = new ArrayList<>();
    public ArrayList<String> strSmArrayList = new ArrayList<>();

    public ArrayList<ASM> SSArrayList = new ArrayList<>();
    public ArrayList<String> strssArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member1);
        instance = DistributorAddOneActivity.this;
        App.addMemberObj = new AddMember();
        FindElements();
        jParser = new JsonParserUniversal();
        companyLocation = new CompanyLocation();
    }

    public void FindElements() {

        lin_asm_sp = findViewById(R.id.lin_asm_sp);
        lin_ss_sp = findViewById(R.id.lin_ss_sp);

        lin_asm_sp.setVisibility(View.GONE);
        lin_ss_sp.setVisibility(View.VISIBLE);

        sp_asm = findViewById(R.id.sp_asm);
        sp_ss = findViewById(R.id.sp_ss);

        lin_asm_tv = findViewById(R.id.lin_asm_tv);
        tv_asm = findViewById(R.id.tv_asm);

        tv_asm.setText(App.mainUser.getFirstName() + " " + App.mainUser.getLastName());

        lin_asm_tv.setVisibility(View.VISIBLE);
        txtarea = findViewById(R.id.txtarea);
        lin_distributor = findViewById(R.id.lin_distributor);
        // li_ss_dropdown = findViewById(R.id.li_ss_dropdown);
        et_distributor = findViewById(R.id.et_distributor);
        sp_company_location = findViewById(R.id.sp_company_location);
//        sp_ss_list = findViewById(R.id.sp_ss_list);
        float_right_button = findViewById(R.id.float_right_button);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        img_profile = findViewById(R.id.img_profile);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        tv_title.setText("Add Distributor");
        tv_detail_name = findViewById(R.id.tv_detail_name);
        tv_detail_name.setText(" ADD DISTRIBUTOR DETAILS");
        lin_distributor.setVisibility(View.VISIBLE);

//        li_ss_dropdown.setVisibility(View.VISIBLE);

        ClickEvents();

        new CallRequest(instance).getSS();
        new CallRequest(instance).getCompanyLocation();

        SpinerClickEvent();

        if (!TextUtils.isEmpty(CompanyLocationID)) {
            int[] array = new int[App.indexListArea.size()];
            int counter = 0;
            for (Integer myInt : App.indexListArea) {
                array[counter++] = myInt;
            }
            System.out.println("multiSelectedJobState==" + CompanyLocationID);
            System.out.println("job state size==" + array.length);
            sp_company_location.setSelection(array);
            txtarea.setVisibility(View.GONE);
        }
    }

    public void SpinerClickEvent() {
     /*   sp_asm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        ASM_id = String.valueOf(asmArrayList.get(position).getId());

                    } else {
                        ASM_id = "0";
                    }
                } catch (Exception e) {
                    ASM_id = "0";
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/
        sp_ss.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        SS_id = String.valueOf(SSArrayList.get(position).getId());


                    } else {
                        SS_id = "0";
                    }
                } catch (Exception e) {
                    SS_id = "0";
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void ClickEvents() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();

            }
        });
    }

    public void Validation() {

        if (et_first_name.getText().toString().equals("")) {
            et_first_name.setError("Please enter first name");
            et_first_name.setFocusable(true);
        } else if (et_last_name.getText().toString().equals("")) {
            et_last_name.setError("Please enter last name");
            et_last_name.setFocusable(true);
        } else if (et_distributor.getText().toString().equals("")) {
            et_distributor.setError("Please enter Distributor name");
            et_distributor.setFocusable(true);
        } else if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
            et_email.setError(" Please enter valid email id");
            et_email.setFocusable(true);
        } else if (et_mobile.getText().toString().equals("")) {
            et_mobile.setError("Please enter mobile no");
            et_mobile.setFocusable(true);
        } else if (SS_id.equalsIgnoreCase("0") || SS_id.equalsIgnoreCase("")) {
            Utils.showToast("Please select SuperStockist", instance);
        } else if (CompanyLocationID.equals("0") || CompanyLocationID.equals("")) {
            Utils.showToast("Please select area", instance);
        } else {
            App.addMemberObj.setDistributorName(et_distributor.getText().toString());
            App.addMemberObj.setFirstName(et_first_name.getText().toString());
            App.addMemberObj.setLastName(et_last_name.getText().toString());
            App.addMemberObj.setEmail(et_email.getText().toString());
            App.addMemberObj.setMobile(et_mobile.getText().toString());
            App.addMemberObj.setCompanyLocationID(CompanyLocationID);
            App.addMemberObj.setPhoto(logoPath);
            App.addMemberObj.setSSUserID(SS_id);

            startActivity(new Intent(instance, DistributorAddTwoActivity.class));
        }


    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath().toString();

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    img_profile.setImageURI(result.getUri());
                    selectedUri = result.getUri();
                    logoPath = selectedUri.getPath().toString();
                    Log.i("TAG", "logoPath :-> " + logoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getCompanyLocation:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            strCompanyLocationArray.clear();
                            companyLocationArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            JSONArray jAreaArray = jData.getJSONArray("CompanyLocation");
                            if (jAreaArray != null && jAreaArray.length() > 0) {
                                for (int i = 0; i < jAreaArray.length(); i++) {
                                    companyLocation = (CompanyLocation) jParser.parseJson(jAreaArray.getJSONObject(i), new CompanyLocation());
                                    companyLocationArray.add(companyLocation);
                                    strCompanyLocationArray.add(companyLocation.getValue());
                                }
                                sp_company_location.setItems(strCompanyLocationArray, SHIPTYPE);
                                sp_company_location.setSelection(new int[]{0});
                                sp_company_location.setListener(this);
                                sp_company_location.clearSelection();

                                if (!TextUtils.isEmpty(CompanyLocationID)) {
                                    int[] array = new int[App.indexListArea.size()];
                                    int counter = 0;
                                    for (Integer myInt : App.indexListArea) {
                                        array[counter++] = myInt;
                                    }
                                    sp_company_location.setSelection(array);
                                }
                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

               /* case getASMById:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            asm = new ASM();
                            asm.setFirstName(" Select ASM");
                            asmArrayList.add(asm);
                            strSmArrayList.add(" Select ASM");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("ASM");
                            if (jarray.length() > 0) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    asm = (ASM) jParser.parseJson(jData, new ASM());
                                    asmArrayList.add(asm);
                                    strSmArrayList.add(asm.getFirstName() + " " + asm.getLastName());
                                }
                                sp_asm.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strSmArrayList));

                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;*/
                case getSS:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            SS = new ASM();
                            SS.setFirstName(" Select Super Stockist");
                            SSArrayList.add(SS);
                            strssArrayList.add(" Select Super Stockist");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("SS");
                            if (jarray.length() > 0) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    SS = (ASM) jParser.parseJson(jData, new ASM());
                                    SSArrayList.add(SS);
                                    strssArrayList.add(SS.getName());
                                }
                                sp_ss.setAdapter(new ArrayAdapter<>(instance, android.R.layout.simple_spinner_dropdown_item, strssArrayList));
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {
        if (filter_type == SHIPTYPE) {
            App.indexListArea = indices;
            CompanyLocationID = "";
            ArrayList<String> shipIDArray = new ArrayList<>();
            for (Integer i : indices) {
                shipIDArray.add(companyLocationArray.get(i).getID());

            }
            txtarea.setVisibility(View.GONE);
            CompanyLocationID = android.text.TextUtils.join(",", shipIDArray);
            if (CompanyLocationID.equals("")) {
                txtarea.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {

    }
}
