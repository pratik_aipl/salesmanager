package com.vk.salesmanager.Activities.ASM;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.Activities.Order.AssignRoute;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AssignRouteListActivity extends AppCompatActivity implements AsynchTaskListner {
    public AssignRouteListActivity instance;
    private static final String TAG = "AssignRouteListActivity";
    int RESULT_CODE = 1;

    public FloatingActionButton float_add;
    public Toolbar toolbar, toolbar_search;
    public LinearLayout lin_empty;
    public RecyclerView rv_product_list;
    public ArrayList<AssignRoute> routeArrayList = new ArrayList<>();
    public AssignRoute routeObj;
    public ImageView img_back, img_search;
    private SearchView mSearchView;
    public JsonParserUniversal jParser;
    public AssignRouteListAdapter adapter;
    public TextView tv_title;

    public static DrawerLayout drawer;
    public View navHeader;
    private NavigationView navigationView;

    public ImageView img_refresh, img_filter, img_filter_right, img_filter_refresh;
    public Spinner sp_all;
    public TextView tv_to_date, tv_from_date;
    public ArrayList<ASM> smArrayList = new ArrayList<>();
    public ArrayList<String> strSmArray = new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    public ASM smObj;
    public String sm_id = "";
    public boolean isFilter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_list);

        instance = this;
        jParser = new JsonParserUniversal();
        App.indexListArea.clear();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nvView);
        navHeader = navigationView.getHeaderView(0);

        sp_all = findViewById(R.id.sp_all);
        img_filter = findViewById(R.id.img_filter);
        img_refresh = findViewById(R.id.img_refresh);
        tv_from_date = findViewById(R.id.tv_from_date);
        tv_to_date = findViewById(R.id.tv_to_date);

        img_filter_refresh = findViewById(R.id.img_filter_refresh);
        img_filter_right = findViewById(R.id.img_filter_right);

        rv_product_list = findViewById(R.id.rcyclerView);
        img_search = findViewById(R.id.img_search);
        img_back = findViewById(R.id.img_back);
        mSearchView = findViewById(R.id.searchView1);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        float_add = findViewById(R.id.float_add);
        toolbar_search = findViewById(R.id.toolbar_search);
        lin_empty = findViewById(R.id.lin_empty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setSupportActionBar(toolbar_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_search.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbar_search.setVisibility(View.GONE);
                toolbar.setVisibility(View.VISIBLE);
                mSearchView.setQuery("", false);
                mSearchView.clearFocus();
            }
        });
        tv_title.setText("Assign Route list");
        mSearchView.setFocusable(false);
        search(mSearchView);
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbar_search.setVisibility(View.VISIBLE);
                toolbar.setVisibility(View.GONE);
                mSearchView.setFocusable(true);
                mSearchView.onActionViewExpanded();
            }
        });
        if (App.mainUser.getRole_id().equalsIgnoreCase("4")) {
            //sm
            float_add.setVisibility(View.GONE);
        }
        if (App.mainUser.getRole_id().equalsIgnoreCase("5")) {
            //distributor
            float_add.setVisibility(View.GONE);
        }
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(instance, AssignRouteActivity.class),RESULT_CODE);
            }
        });
        img_filter_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFilter = true;
                new CallRequest(instance).getAssignRoute(sm_id, tv_from_date.getText().toString(), tv_to_date.getText().toString());

                drawer.closeDrawers();
            }
        });

        img_filter_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFilter = false;
                tv_from_date.setText("");
                tv_to_date.setText("");
                sm_id = "";
                new CallRequest(instance).getAssignRoute(sm_id, tv_from_date.getText().toString(), tv_to_date.getText().toString());

                drawer.closeDrawers();
            }
        });


        setDrawer();
        DateSelection();
        new CallRequest(instance).getSM();
       callApi();

    }

    private void callApi() {
        new CallRequest(instance).getAssignRoute(sm_id, tv_from_date.getText().toString(), tv_to_date.getText().toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: ");
        if (requestCode == RESULT_CODE) {
            if(resultCode == Activity.RESULT_OK){
                callApi();
            }
        }
    }
    public void DateSelection() {

        tv_from_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_from_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(AssignRouteListActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        tv_to_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_to_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(AssignRouteListActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        sp_all.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        sm_id = String.valueOf(smArrayList.get(position).getId());

                    } else {
                        sm_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void setDrawer() {
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.END);
            }
        });
        drawer.closeDrawers();
    }

    public void search(SearchView searchView) {
        searchView.setQueryHint("Search...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    public void SetupRecylerView() {

        adapter = new AssignRouteListAdapter(routeArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_product_list.setLayoutAnimation(controller);
        rv_product_list.scheduleLayoutAnimation();
        rv_product_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_product_list.setItemAnimator(new DefaultItemAnimator());
        rv_product_list.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getAssignRoute:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            routeArrayList.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            JSONArray jOrders = jData.getJSONArray("AssignRoute");
                            if (jOrders != null && jOrders.length() > 0) {
                                lin_empty.setVisibility(View.GONE);
                                rv_product_list.setVisibility(View.VISIBLE);
                                for (int i = 0; i < jOrders.length(); i++) {
                                    JSONObject jD = jOrders.getJSONObject(i);
                                    JSONArray jinerData = jD.getJSONArray("data");
                                    for (int j = 0; j < jinerData.length(); j++) {

                                        routeObj = (AssignRoute) jParser.parseJson(jinerData.getJSONObject(j), new AssignRoute());
                                        if (j == 0) {
                                            routeObj.setRouteDate(jD.getString("RouteDate"));
                                        } else {
                                            routeObj.setRouteDate("");
                                        }
                                        routeArrayList.add(routeObj);
                                    }
                                }
                                routeArrayList.size();
                                img_search.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        toolbar_search.setVisibility(View.VISIBLE);
                                        toolbar.setVisibility(View.GONE);
                                        mSearchView.setFocusable(true);
                                        mSearchView.onActionViewExpanded();
                                    }
                                });
                                SetupRecylerView();

                            } else {
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_product_list.setVisibility(View.GONE);
                                Utils.showToast(jObj.getString("message"), instance);
                            }

                        } else {
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_product_list.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getSM:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            smObj = new ASM();
                            smObj.setFirstName(" Select SM");
                            smArrayList.add(smObj);
                            strSmArray.add(" Select SM");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("SM");
                            if (jarray.length() > 0) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    smObj = (ASM) jParser.parseJson(jData, new ASM());
                                    smArrayList.add(smObj);
                                    strSmArray.add(smObj.getFirstName() + " " + smObj.getLastName());
                                }
                                sp_all.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strSmArray));

                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

}
