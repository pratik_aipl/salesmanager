package com.vk.salesmanager.Activities.dsr;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.salesmanager.Activities.Distributor.Distributor;
import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.Activities.Retailer.Retailor;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Fonts.RobotoEditTextView;
import com.vk.salesmanager.Fonts.RobotoTextView;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.EasyWayLocation;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Listener;
import com.vk.salesmanager.Utils.MyCustomTypeface;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AddDailySalesReportActivity extends AppCompatActivity implements AsynchTaskListner, Listener {
    private static final String TAG = "AddDailySalesReportActi";
    public Button btn_submit;
    public TextView tv_date;
    public Spinner sp_retalier;
    final Calendar myCalendar = Calendar.getInstance();

    public AddDailySalesReportActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_image_name;


    public ArrayList<Retailor> retailerArrayList = new ArrayList<>();
    public ArrayList<Distributor> DistArrayList = new ArrayList<>();
    public ArrayList<String> strrtlArrayList = new ArrayList<>();
    public Retailor smObj;
    public Distributor dObj;

    public JsonParserUniversal jParser;
    public String sm_id = "";
    Switch switch_isorder;
    RelativeLayout rel_orderno;
    RobotoEditTextView et_orderno, et_nohr, et_remark;
    String From;
    RobotoTextView sp_title;

    LinearLayout lin_switch;
    RadioGroup radioGroup;
    String[] usertype = {"0"};
    EasyWayLocation easyWayLocation;
    double lati,longi;


    String[] PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    int PERMISSION_ALL = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daily_sales_report);
        instance = AddDailySalesReportActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);




        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_image_name = findViewById(R.id.tv_image_name);
        tv_date = findViewById(R.id.tv_date);
        sp_retalier = findViewById(R.id.sp_retalier);
        switch_isorder = findViewById(R.id.switch_isorder);
        btn_submit = findViewById(R.id.btn_submit);
        rel_orderno = findViewById(R.id.rel_orderno);
        sp_title = findViewById(R.id.sp_title);
        radioGroup = findViewById(R.id.radioGroup);
        lin_switch = findViewById(R.id.lin_switch);

        et_orderno = findViewById(R.id.et_orderno);
        et_nohr = findViewById(R.id.et_nohr);
        et_remark = findViewById(R.id.et_remark);

        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }else{
            easyWayLocation = new EasyWayLocation(this);
            easyWayLocation.setListener(this);
        }

        if (getIntent() != null)
            From = getIntent().getStringExtra("From");

        if (From.equalsIgnoreCase("SM")) {
            sp_title.setText("Retailer");
            new CallRequest(instance).getRetailor();

        } else {
            sp_title.setText("Distributor");
            new CallRequest(instance).getDistributor();
        }
        tv_date.setText(new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime()));


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Daily Sales Report");


        switch_isorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rel_orderno.setVisibility(View.VISIBLE);
                } else {
                    rel_orderno.setVisibility(View.GONE);
                }
            }
        });


        tv_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMinDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });

        sp_retalier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColor));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Roboto-Regular.ttf")));
                        if (From.equalsIgnoreCase("SM"))
                            sm_id = String.valueOf(retailerArrayList.get(position).getId());
                        else
                            sm_id = String.valueOf(DistArrayList.get(position).getId());

                        // new CallRequest(instance).getSmAssignRouteList(sm_id);
                    } else {
                        sm_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*    if (From.equalsIgnoreCase("SM")) {
                    if (sm_id.equalsIgnoreCase("0"))
                        Utils.showToast("Please Select Retailer", instance);
                } else {
                    if (sm_id.equalsIgnoreCase("0"))
                        Utils.showToast("Please Enter Distributor", instance);
                }*/

                if (TextUtils.isEmpty(tv_date.getText().toString().trim())) {
                    Utils.showToast("Please Enter Date", instance);
                } else if (switch_isorder.isChecked() && TextUtils.isEmpty(et_orderno.getText().toString().trim())) {
                    et_orderno.setError("Please Enter Order No");
                    et_orderno.setFocusable(true);
                }  else if (sm_id.equalsIgnoreCase("0") || sm_id.equalsIgnoreCase("")) {
                    if (From.equalsIgnoreCase("SM"))
                        Utils.showToast("Please Enter Retailer", instance);
                    else
                        Utils.showToast("Please Enter Distributor", instance);
                } else {
                    int placeorder = 0;
                    if (switch_isorder.isChecked()) {
                        placeorder = 1;
                    }
                    if(lati==0.0&&longi==0.0){
                        Toast.makeText(instance, "Please Check Your Location", Toast.LENGTH_SHORT).show();
                    }else{
                        new CallRequest(instance).Add_DSR(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()), sm_id,
                                new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss").format(Calendar.getInstance().getTime()),
                                et_remark.getText().toString().trim(),
                                String.valueOf(placeorder),
                                et_orderno.getText().toString().trim(),usertype[0],lati,
                                longi);
                    }

                    // Toast.makeText(instance, "Done...!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        final RadioButton[] radioButton = {null};

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                      radioButton[0] = findViewById(checkedId);
                                                      usertype[0] = radioButton[0].getText().toString();
                                                  if(usertype[0].equalsIgnoreCase("CHECK OUT")){
                                                      lin_switch.setVisibility(View.VISIBLE);
                                                      usertype = new String[]{"1"};
                                                  }else{
                                                      lin_switch.setVisibility(View.GONE);
                                                      usertype = new String[]{"0"};
                                                  }

                                                  }
                                              }
        );
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case getDSR:
                    Utils.hideProgressDialog();
                    if (respCode.equalsIgnoreCase("401")) {
                        App.mySharedPref.clearApp();
                        Intent i = new Intent(instance, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    } else if (respCode.equalsIgnoreCase("200")) {
                        JSONObject jObj = null;
                        try {
                            jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                Utils.showToast(jObj.getString("message"), this);
                                onBackPressed();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Utils.showToast("Server side error...", this);
                    }
                    break;
                case getRetailor:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            smObj = new Retailor();
                            smObj.setFirstName(" Select Retailer");
                            retailerArrayList.add(smObj);
                            strrtlArrayList.add(" Select Retailer");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("Retailer");
                            if (jarray.length() > 0) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    smObj = (Retailor) jParser.parseJson(jData, new Retailor());
                                    retailerArrayList.add(smObj);
                                    strrtlArrayList.add(smObj.getFirstName() + " " + smObj.getLastName());
                                }
                                sp_retalier.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strrtlArrayList));

                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case getDistributor:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            dObj = new Distributor();
                            dObj.setFirstName(" Select Distributor");
                            DistArrayList.add(dObj);
                            strrtlArrayList.add(" Select Distributor");

                            // Utils.showToast(jObj.getString("message"), this);
                            JSONObject jdata = jObj.getJSONObject("data");
                            JSONArray jarray = jdata.getJSONArray("Distributor");
                            if (jarray.length() > 0) {
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    dObj = (Distributor) jParser.parseJson(jData, new Distributor());
                                    DistArrayList.add(dObj);
                                    strrtlArrayList.add(dObj.getDistributorName());
                                }
                                sp_retalier.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strrtlArrayList));

                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
         lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        Log.d(TAG, "onPositionChanged: "+lati);
        Log.d(TAG, "onPositionChanged: "+longi);
        String[] loc =EasyWayLocation.getAddress(this, lati, longi, false, true);
   /*     if (Constant.isChangeLocation) {
            if (!TextUtils.isEmpty(loc[0])) {
                Constant.defaultCity = loc[0];
                currentCity = loc[0];
            }
            if (!TextUtils.isEmpty(loc[1])) {
                Constant.defaultCity = "";
                currentCity = "";
                Constant.defaultCity = loc[0] + "," + loc[1];
                currentCity = loc[0] + "," + loc[1];
            }
            Log.d(TAG, "CITYYY>>>: "+currentCity);
            tieCity.setText(currentCity);
        }*/
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}
