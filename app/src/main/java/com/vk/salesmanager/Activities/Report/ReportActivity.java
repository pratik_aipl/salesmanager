package com.vk.salesmanager.Activities.Report;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReportActivity extends AppCompatActivity implements AsynchTaskListner {
    public ReportActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_name, tv_date, tv_price, tv_qty;
    public RecyclerView recyclerList;
    public JsonParserUniversal jParser;
    public ReportDashBoard reportObj;
    public ArrayList<ReportDashBoard> reportList = new ArrayList<>();
    public ReportManuAdapter adapter;
    public CardView card_header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        reportObj = new ReportDashBoard();
        instance = ReportActivity.this;
        jParser = new JsonParserUniversal();

        toolbar = findViewById(R.id.toolbar);
        tv_qty = findViewById(R.id.tv_qty);
        tv_price = findViewById(R.id.tv_price);
        tv_date = findViewById(R.id.tv_date);
        tv_name = findViewById(R.id.tv_name);
        card_header = findViewById(R.id.card_header);
       /* if (App.mainUser.getRole_id().equals("5")) {
            card_header.setVisibility(View.GONE);
        }*/
        tv_title = toolbar.findViewById(R.id.tv_title);
        recyclerList = findViewById(R.id.recyclerList);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Reports");
        new CallRequest(instance).report();
    }

    public void SetupRecylerView() {
        LinearLayoutManager lln = new LinearLayoutManager(instance);
        recyclerList.setLayoutManager(lln);
        StaggeredGridLayoutManager sgaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerList.setLayoutManager(sgaggeredGridLayoutManager);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_from_right);
        recyclerList.setLayoutAnimation(controller);
        recyclerList.setHasFixedSize(true);

        adapter = new ReportManuAdapter(reportList, instance);
        recyclerList.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {
                            App.mySharedPref.clearApp();
                            Intent i = new Intent(instance, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else if (respCode.equalsIgnoreCase("200")) {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getBoolean("status")) {
                                JSONObject jdata = jObj.getJSONObject("data");
                                JSONArray jDailyReportarray = jdata.getJSONArray("DailyReport");
                                try {
                                    tv_name.setText(jDailyReportarray.getJSONObject(0).getString("Label"));
                                    tv_date.setText(jDailyReportarray.getJSONObject(0).getString("Date"));
                                    tv_price.setText(jDailyReportarray.getJSONObject(0).getString("Amount"));
                                   // tv_qty.setText(jDailyReportarray.getJSONObject(0).getString("Item"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                JSONArray jarray = jdata.getJSONArray("DashBoard");
                                if (jarray.length() > 0) {
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        reportObj = (ReportDashBoard) jParser.parseJson(jData, new ReportDashBoard());
                                        reportList.add(reportObj);
                                    }
                                    SetupRecylerView();
                                }

                            }
                        }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
