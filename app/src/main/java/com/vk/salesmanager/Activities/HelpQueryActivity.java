package com.vk.salesmanager.Activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vk.salesmanager.Adapter.HelpAdapter;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.Help;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HelpQueryActivity extends AppCompatActivity implements AsynchTaskListner {
    public HelpQueryActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, lbl_rsm, lbl_asm, lbl_sm;
    public Button btn_send, btn_call;
    public EditText et_message;
    public RecyclerView rcyclerViewSM, rcyclerViewRSM, rcyclerViewASM;
    public JsonParserUniversal jParser;
    public HelpAdapter adapter;
    public ArrayList<Help> helpArrayListSM = new ArrayList<>();
    public ArrayList<Help> helpArrayListASM = new ArrayList<>();
    public ArrayList<Help> helpArrayListRSM = new ArrayList<>();
    public Help help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_query);

        instance = HelpQueryActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);

        lbl_rsm = findViewById(R.id.lbl_rsm);
        lbl_asm = findViewById(R.id.lbl_asm);
        lbl_sm = findViewById(R.id.lbl_sm);

        jParser = new JsonParserUniversal();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Help Query");
        btn_call = (Button) findViewById(R.id.btn_call);
        btn_send = (Button) findViewById(R.id.btn_send);
        et_message = (EditText) findViewById(R.id.et_message);
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = "1800 333 5554";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });
        rcyclerViewSM = findViewById(R.id.rcyclerView);
        rcyclerViewRSM = findViewById(R.id.rcyclerView3);
        rcyclerViewASM = findViewById(R.id.rcyclerView2);
        new CallRequest(instance).help();


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case help:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            helpArrayListSM.clear();
                            //  JSONObject jData = jObj.getJSONObject("data");
                            JSONArray jOrders = jObj.getJSONArray("data");
                            if (jOrders != null && jOrders.length() > 0) {
                                for (int i = 0; i < jOrders.length(); i++) {
                                    JSONObject jD = jOrders.getJSONObject(i);

                                        help = (Help) jParser.parseJson(jD, new Help());
                                        if (help.getRoleID().equalsIgnoreCase("4")) {
                                            helpArrayListSM.add(help);

                                        }
                                        if (help.getRoleID().equalsIgnoreCase("3")) {
                                            helpArrayListASM.add(help);

                                        }
                                        if (help.getRoleID().equalsIgnoreCase("2")) {
                                            helpArrayListRSM.add(help);

                                        }
                                    }
                                    if (App.mainUser.getRoleID().equalsIgnoreCase("3")) {

                                        lbl_rsm.setVisibility(View.VISIBLE);
                                        lbl_asm.setVisibility(View.GONE);
                                        lbl_sm.setVisibility(View.GONE);
                                        rcyclerViewRSM.setVisibility(View.VISIBLE);
                                        rcyclerViewASM.setVisibility(View.GONE);
                                        rcyclerViewSM.setVisibility(View.GONE);
                                        SetupRecylerViewRSM();
                                    }
                                    if (App.mainUser.getRoleID().equalsIgnoreCase("4")) {

                                        lbl_rsm.setVisibility(View.VISIBLE);
                                        lbl_asm.setVisibility(View.VISIBLE);
                                        lbl_sm.setVisibility(View.GONE);
                                        rcyclerViewRSM.setVisibility(View.VISIBLE);
                                        rcyclerViewASM.setVisibility(View.VISIBLE);
                                        rcyclerViewSM.setVisibility(View.GONE);
                                        SetupRecylerViewRSM();
                                        SetupRecylerViewASM();
                                    }
                                    if (App.mainUser.getRoleID().equalsIgnoreCase("5")) {

                                        lbl_rsm.setVisibility(View.VISIBLE);
                                        lbl_asm.setVisibility(View.VISIBLE);
                                        lbl_sm.setVisibility(View.VISIBLE);
                                        rcyclerViewSM.setVisibility(View.VISIBLE);
                                        rcyclerViewASM.setVisibility(View.VISIBLE);
                                        rcyclerViewRSM.setVisibility(View.VISIBLE);
                                        SetupRecylerViewSM();
                                        SetupRecylerViewASM();
                                        SetupRecylerViewRSM();
                                    }



                            } else {
                                lbl_rsm.setVisibility(View.GONE);
                                lbl_asm.setVisibility(View.GONE);
                                lbl_sm.setVisibility(View.GONE);
                                rcyclerViewSM.setVisibility(View.GONE);
                                rcyclerViewASM.setVisibility(View.GONE);
                                rcyclerViewRSM.setVisibility(View.GONE);
                                Utils.showToast(jObj.getString("message"), instance);
                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    public void SetupRecylerViewSM() {

        adapter = new HelpAdapter(helpArrayListSM, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rcyclerViewSM.setLayoutAnimation(controller);
        rcyclerViewSM.scheduleLayoutAnimation();
        rcyclerViewSM.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rcyclerViewSM.setItemAnimator(new DefaultItemAnimator());
        rcyclerViewSM.setAdapter(adapter);
    }

    public void SetupRecylerViewASM() {

        adapter = new HelpAdapter(helpArrayListASM, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rcyclerViewASM.setLayoutAnimation(controller);
        rcyclerViewASM.scheduleLayoutAnimation();
        rcyclerViewASM.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rcyclerViewASM.setItemAnimator(new DefaultItemAnimator());
        rcyclerViewASM.setAdapter(adapter);
    }

    public void SetupRecylerViewRSM() {

        adapter = new HelpAdapter(helpArrayListRSM, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rcyclerViewRSM.setLayoutAnimation(controller);
        rcyclerViewRSM.scheduleLayoutAnimation();
        rcyclerViewRSM.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rcyclerViewRSM.setItemAnimator(new DefaultItemAnimator());
        rcyclerViewRSM.setAdapter(adapter);
    }

}
