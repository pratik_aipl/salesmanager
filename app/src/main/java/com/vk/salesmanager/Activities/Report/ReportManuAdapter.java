package com.vk.salesmanager.Activities.Report;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vk.salesmanager.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 8/30/2018.
 */

public class ReportManuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public Context context;
    private ArrayList<ReportDashBoard> reportList;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;
    public String type = "";


    public ReportManuAdapter(ArrayList<ReportDashBoard> reportList, Context context) {
        this.reportList = reportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_icon;
        public TextView tv_name;

        public MyViewHolder(View view) {
            super(view);
            img_icon = view.findViewById(R.id.img_icon);
            tv_name = view.findViewById(R.id.tv_name);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_report_raw, parent, false));

    }


    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {

        holder.tv_name.setText(reportList.get(pos).getName());
        if (reportList.get(pos).getName().equalsIgnoreCase("LOCATION WISE SALES")) {
            type = "LOCATION WISE SALES";
            holder.img_icon.setImageResource(R.drawable.area_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("LOCATION WISE ITEM SALES")) {
            type = "LOCATION WISE ITEM SALES";
            holder.img_icon.setImageResource(R.drawable.area_wise_item_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("ZSM WISE SALES")) {
            type = "ZSM WISE SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("RSM WISE SALES")) {
            type = "RSM WISE SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("CNF WISE SALES")) {
            type = "CNF WISE SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("CNF WISE ITEM SALES")) {
            type = "CNF WISE ITEM SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("SS WISE SALES")) {
            type = "SS WISE SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("SS WISE ITEM SALES")) {
            type = "SS WISE ITEM SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("ASM WISE SALES")) {
            type = "ASM WISE SALES";
            holder.img_icon.setImageResource(R.drawable.asm_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("SM WISE SALES")) {
            type = "SM WISE SALES";
            holder.img_icon.setImageResource(R.drawable.sm_wise_item_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("DISTRIBUTOR WISE SALES")) {
            type = "DISTRIBUTOR WISE SALES";
            holder.img_icon.setImageResource(R.drawable.dis_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("DISTRIBUTOR WISE ITEM SALES")) {
            type = "DISTRIBUTOR WISE ITEM SALES";
            holder.img_icon.setImageResource(R.drawable.dis_wise_item_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("RETAILER WISE SALES")) {
            type = "RETAILER WISE SALES";
            holder.img_icon.setImageResource(R.drawable.retailor_wise_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("RETAILER WISE ITEM SALES")) {
            type = "RETAILER WISE ITEM SALES";
            holder.img_icon.setImageResource(R.drawable.retailor_wise_item_sales);
        } else if (reportList.get(pos).getName().equalsIgnoreCase("ITEM SALES")) {
            type = "ITEM SALES";
            holder.img_icon.setImageResource(R.drawable.itemwisesales);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ReportDetailActivity.class)
                        .putExtra("type", reportList.get(pos).getName())
                        .putExtra("asm", "ASM"));
            }
        });

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;

        }
    }


    @Override
    public int getItemViewType(int position) {

        return VIEW_TYPE_ITEM;

    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }
}
