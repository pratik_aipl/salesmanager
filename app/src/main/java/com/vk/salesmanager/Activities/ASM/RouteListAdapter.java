package com.vk.salesmanager.Activities.ASM;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.salesmanager.App;
import com.vk.salesmanager.BuildConfig;
import com.vk.salesmanager.Models.Route;
import com.vk.salesmanager.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Karan - Empiere on 11/2/2018.
 */

public class RouteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private ArrayList<Route> mArrayList;
    public Context context;
    private ArrayList<Route> mFilteredList;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;

    public RouteListAdapter(ArrayList<Route> moviesList, Context context) {
        mArrayList = moviesList;
        mFilteredList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_state, tv_name, tv_mobile;
        public RelativeLayout rel_content;
        public CircleImageView img_profile;

        public MyViewHolder(View view) {
            super(view);
            tv_state = view.findViewById(R.id.tv_state);
            tv_name = view.findViewById(R.id.tv_name);
            img_profile = view.findViewById(R.id.img_profile);
            tv_mobile = view.findViewById(R.id.tv_mobile);
            rel_content = view.findViewById(R.id.rel_content);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        public TextView tv_state, tv_name, tv_mobile;
        public RelativeLayout rel_content;
        public CircleImageView img_profile;

        public ViewHolderFooter(View view) {
            super(view);
            tv_state = view.findViewById(R.id.tv_state);
            tv_name = view.findViewById(R.id.tv_name);
            img_profile = view.findViewById(R.id.img_profile);
            tv_mobile = view.findViewById(R.id.tv_mobile);
            rel_content = view.findViewById(R.id.rel_content);
        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {
        Route asmObj = mFilteredList.get(pos);
        holder.tv_name.setText(asmObj.getLocationName());
        holder.tv_mobile.setText(asmObj.getRounteNo());
        holder.tv_state.setText(asmObj.getFrom() + " - " + asmObj.getTo());
        Picasso.with(context)
                .load(BuildConfig.API_URL)
                .error(R.drawable.profile)
                //this is optional the image to display while the url image is downloading
                .into(holder.img_profile);

        if (App.mainUser.getRole_id().equalsIgnoreCase("5")) {
            //distributor
        } else {
            holder.rel_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, DrawRouteActivity.class)
                            .putExtra("obj", mFilteredList.get(pos))
                            .putExtra("edit", "edit"));
                }
            });
        }
    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        Route asmObj = mFilteredList.get(pos);
        holder.tv_name.setText(asmObj.getLocationName());
        holder.tv_mobile.setText(asmObj.getRounteNo());
        holder.tv_state.setText(asmObj.getFrom() + " - " + asmObj.getTo());

        Picasso.with(context)
                .load(BuildConfig.API_URL)
                .error(R.drawable.profile)
                //this is optional the image to display while the url image is downloading
                .into(holder.img_profile);

        if (App.mainUser.getRole_id().equalsIgnoreCase("5")) {
            //distributor
        } else {
            holder.rel_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, DrawRouteActivity.class)
                            .putExtra("obj", mFilteredList.get(pos))
                            .putExtra("edit", "edit"));
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == (mFilteredList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<Route> filteredList = new ArrayList<>();

                    for (Route androidVersion : mArrayList) {

                        if (androidVersion.getRounteNo().toLowerCase().contains(charString)
                                || androidVersion.getLocationName().toLowerCase().contains(charString)
                        ) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Route>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
