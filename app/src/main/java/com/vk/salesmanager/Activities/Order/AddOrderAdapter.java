package com.vk.salesmanager.Activities.Order;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vk.salesmanager.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class AddOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    private ArrayList<TempOrder> mFilteredList;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;

    public AddOrderAdapter(ArrayList<TempOrder> moviesList, Context context) {

        mFilteredList = moviesList;
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_add_order_raw, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_no, tv_product, tv_order_qty;
        public LinearLayout lin_info;
        public CircleImageView img_profile;

        public MyViewHolder(View view) {
            super(view);
            tv_no = view.findViewById(R.id.tv_no);
            tv_product = view.findViewById(R.id.tv_product);
            tv_order_qty = view.findViewById(R.id.tv_order_qty);

        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {

        public ViewHolderFooter(View view) {
            super(view);

        }
    }


    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {
        TempOrder asmObj = mFilteredList.get(pos);

    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        TempOrder asmObj = mFilteredList.get(pos);
        holder.tv_no.setText(String.valueOf(pos + 1));
        holder.tv_product.setText(asmObj.getItemName());
        holder.tv_order_qty.setText(String.valueOf(asmObj.getQTY()));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {

        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }


}
