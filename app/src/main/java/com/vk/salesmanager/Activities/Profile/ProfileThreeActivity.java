package com.vk.salesmanager.Activities.Profile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsyncTaskListner;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.MainUser;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileThreeActivity extends AppCompatActivity implements AsynchTaskListner, AsyncTaskListner {

    public ProfileThreeActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_image_name;
    public EditText et_unique_no;
    public RelativeLayout rel_browse;
    Uri selectedUri;
    public String logoPath = "";
    public Button btn_update, btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_three);

        instance = ProfileThreeActivity.this;
        et_unique_no = findViewById(R.id.et_unique_no);
        tv_image_name = findViewById(R.id.et_photo_proof);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_update = findViewById(R.id.btn_update);
        rel_browse = findViewById(R.id.rel_browse);
        btn_register = findViewById(R.id.btn_register);
        btn_register.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Add Member");
        setData(App.mainUser);
        rel_browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        rel_browse = findViewById(R.id.rel_browse);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validaton();
            }
        });
    }

    public void Validaton() {
        if (et_unique_no.getText().toString().equals("")) {
            et_unique_no.setError("Please enter Unique No");
            et_unique_no.setFocusable(true);
        } else {
            new CallRequest(instance).editProfile(App.mainUser.getFirstName(), App.mainUser.getLastName(), App.mainUser.getDOB(),
                    et_unique_no.getText().toString(), App.mainUser.getAddress(), App.mainUser.getArea(), App.mainUser.getCity(),
                    App.mainUser.getState(),
                    App.mainUser.getZipcode(), App.mainUser.getCompanyLocationID(),
                    App.mainUser.getEmail(),
                    App.mainUser.getMobile(), logoPath, "");
        }

    }

    private void setData(MainUser mainUser) {
        et_unique_no.setText(mainUser.getUniqueIdentityNO().toString());
        tv_image_name.setText(mainUser.getUniqueIdentityURL().toString());
    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath().toString();

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    selectedUri = result.getUri();
                    logoPath = selectedUri.getPath().toString();
                    tv_image_name.setText(logoPath.substring(logoPath.lastIndexOf("/") + 1, logoPath.length()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(2, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case editProfile:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            Utils.showToast(jObj.getString("message"), this);

                            onBackPressed();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case editProfile:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            Utils.showToast(jObj.getString("message"), this);

                            onBackPressed();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }
}
