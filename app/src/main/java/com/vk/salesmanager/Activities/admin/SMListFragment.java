package com.vk.salesmanager.Activities.admin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.Activities.Sales.SalesListAdapter;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.Models.CompanyLocation;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SMListFragment extends Fragment implements AsynchTaskListner {
    public SMListFragment instance;

    public JsonParserUniversal jParser;
    public TextView tv_title;
    public LinearLayout lin_empty;
    public RecyclerView rv_product_list;
    public ASM asmObj;
    public CompanyLocation companyLocationObj;
    @BindView(R.id.header)
    LinearLayout header;
    public FloatingActionButton float_add;

    private Unbinder unbinder;
    private SalesListAdapter salesListAdapter;
    public ArrayList<ASM> asmArrayList = new ArrayList<>();


    private View view;


    public SMListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_list_and_add_member, container, false);
        unbinder = ButterKnife.bind(this, view);
        instance = this;

        jParser = new JsonParserUniversal();
        float_add = view.findViewById(R.id.float_add);

        rv_product_list = view.findViewById(R.id.rcyclerView);
        lin_empty = view.findViewById(R.id.lin_empty);
        header.setVisibility(View.GONE);
        float_add.setVisibility(View.GONE);

        new CallRequest(instance).getFragSM();

        return view;
    }

    public void SetupRecylerView() {

        salesListAdapter = new SalesListAdapter(asmArrayList, getActivity());
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        rv_product_list.setLayoutAnimation(controller);
        rv_product_list.scheduleLayoutAnimation();
        rv_product_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_product_list.setItemAnimator(new DefaultItemAnimator());
        rv_product_list.setAdapter(salesListAdapter);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getSM:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {
                            App.mySharedPref.clearApp();
                            Intent i = new Intent(getActivity(), LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            getActivity().finish();
                        } else if (respCode.equalsIgnoreCase("200")) {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getBoolean("status") == true) {
                                // Utils.showToast(jObj.getString("message"), this);
                                JSONObject jdata = jObj.getJSONObject("data");
                                JSONArray jarray = jdata.getJSONArray("SM");
                                if (jarray.length() > 0 && jarray != null) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_product_list.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        asmObj = (ASM) jParser.parseJson(jData, new ASM());

                                        JSONArray jlocation = jData.getJSONArray("company_location");
                                        for (int j = 0; j < jlocation.length(); j++) {
                                            JSONObject jDataLocation = jlocation.getJSONObject(j);
                                            companyLocationObj = (CompanyLocation) jParser.parseJson(jDataLocation, new CompanyLocation());
                                            asmObj.company_location.add(companyLocationObj);
                                        }
                                        asmArrayList.add(asmObj);

                                    }
                                    SetupRecylerView();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("message"), getActivity());
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_product_list.setVisibility(View.GONE);
                            }
                        } else {
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_product_list.setVisibility(View.GONE);
                            Utils.showToast("Server side error...", getActivity());
                        }
                        break;
                }
            } catch (JSONException e) {
                lin_empty.setVisibility(View.VISIBLE);
                rv_product_list.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", getActivity());
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
