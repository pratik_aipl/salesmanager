package com.vk.salesmanager.Activities.Order;

import android.content.Intent;
import android.graphics.PorterDuff;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vk.salesmanager.Activities.DashBoardActivity;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.OtpView;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class VerifyOrderActivity extends AppCompatActivity implements AsynchTaskListner {
    public VerifyOrderActivity instance;
    public Toolbar toolbar;
    public TextView tv_title;
    public Button btn_confirm_reg;
    public OtpView otp_view;
    public String OrderHeaderID = "", OTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_otp);
        instance = this;
        OrderHeaderID = getIntent().getExtras().getString("OrderHeaderID");
        OTP = getIntent().getExtras().getString("OTP");
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_confirm_reg = findViewById(R.id.btn_confirm_reg);
        otp_view = findViewById(R.id.otp_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btn_confirm_reg.setText("CONFIRM OTP");
        tv_title.setText("Confirm OTP");


        btn_confirm_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (OTP.equalsIgnoreCase(otp_view.getOTP())) {
                    new CallRequest(instance).verifyOrder(OrderHeaderID);
                } else {
                    new CallRequest(instance).verifyOrder(OrderHeaderID);
                   // Utils.showToast("INVALID OTP", instance);
                }

            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case verifyOrder:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            Utils.showToast(jObj.getString("message"), instance);
                            startActivity(new Intent(instance, DashBoardActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

}
