package com.vk.salesmanager.Activities.Report;

import android.app.DatePickerDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.Activities.Distributor.Distributor;
import com.vk.salesmanager.Activities.Order.Product;
import com.vk.salesmanager.Activities.Order.RetailerOrder;
import com.vk.salesmanager.Activities.Report.Model.Area;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.ASM;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ReportDetailActivity extends AppCompatActivity implements AsynchTaskListner {
    public ReportDetailActivity instace;
    public JsonParserUniversal jParser;
    public WebView web_report;
    public TextView lbl_spiner_title, tv_title, tv_report_title, tv_date, tv_price, tv_total_sales_title, lbl_product, lbl_all, tv_to_date, tv_from_date;
    public ImageView img_refresh, img_filter, img_filter_right, img_filter_refresh;
    public String HTML = "", Type = "", DisplayTitle = "", DisplayDate = "", TotalSales = "", SelectedId = "", SelectedProductId = "";
    public DrawerLayout drawer;
    public View navHeader;
    public Toolbar toolbar;

    public LinearLayout lin_product, lin_all_spiner;

    public Spinner sp_all, sp_product;
    final Calendar myCalendar = Calendar.getInstance();

    public RetailerOrder retailerOrder;
    public ArrayList<RetailerOrder> retailerArrayList = new ArrayList<>();

    public Area araObj;
    public ArrayList<Area> areaArrayList = new ArrayList<>();
    public ArrayList<String> strAreaArray = new ArrayList<>();

    public Product productObj;
    public ArrayList<Product> productArrayList = new ArrayList<>();

    public ASM asm;
    public ArrayList<ASM> zsmArrayList = new ArrayList<>();
    public ArrayList<ASM> rsmArrayList = new ArrayList<>();
    public ArrayList<ASM> ssArrayList = new ArrayList<>();
    public ArrayList<ASM> cnfArrayList = new ArrayList<>();
    public ArrayList<ASM> asmArrayList = new ArrayList<>();

    public ArrayList<ASM> smArrayList = new ArrayList<>();

    private static final String TAG = "ReportDetailActivity";
    public Distributor distributor;
    public ArrayList<Distributor> distributorArrayList = new ArrayList<>();

    public ArrayAdapter<String> Spineradapter;

    public boolean isMultipelSpiner = false, isFilter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_detail);
        instace = this;
        jParser = new JsonParserUniversal();

        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Reports");
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nvView);

        Type = getIntent().getExtras().getString("type");
        Log.d(TAG, "Typppe: " + Type);
        navHeader = navigationView.getHeaderView(0);

        sp_all = findViewById(R.id.sp_all);
        sp_product = findViewById(R.id.sp_product);
        lbl_all = findViewById(R.id.lbl_all);
        lbl_spiner_title = findViewById(R.id.lbl_spiner_title);
        lin_all_spiner = findViewById(R.id.lin_all_spiner);
        img_filter_refresh = findViewById(R.id.img_filter_refresh);
        img_filter_right = findViewById(R.id.img_filter_right);

        lin_product = findViewById(R.id.lin_product);
        lbl_product = findViewById(R.id.lbl_product);
        tv_from_date = findViewById(R.id.tv_from_date);
        tv_to_date = findViewById(R.id.tv_to_date);
        tv_total_sales_title = findViewById(R.id.tv_total_sales_title);
        tv_price = findViewById(R.id.tv_price);
        tv_date = findViewById(R.id.tv_date);
        tv_report_title = findViewById(R.id.tv_report_title);
        web_report = findViewById(R.id.web_report);
        img_filter = findViewById(R.id.img_filter);
        img_refresh = findViewById(R.id.img_refresh);
        setDrawer();
        img_filter_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFilter = true;
                isFilter = false;
                tv_from_date.setText("");
                tv_to_date.setText("");
                SelectedId = "";
                SelectedProductId = "";
                getData("", "", "", "", "");
                getData(SelectedId, SelectedProductId, tv_from_date.getText().toString(), tv_to_date.getText().toString(), SelectedProductId);

            }
        });
        getData("", "", "", "", "");
        tv_report_title.setText(Type);
        DateSelection();

        img_filter_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFilter = true;
                getData(SelectedId, SelectedProductId, tv_from_date.getText().toString(), tv_to_date.getText().toString(), SelectedProductId);
                drawer.closeDrawers();
            }
        });

        img_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFilter = false;
                tv_from_date.setText("");
                tv_to_date.setText("");
                SelectedId = "";
                SelectedProductId = "";
                getData("", "", "", "", "");
                drawer.closeDrawers();
            }
        });

        SpinerClickEvent();
    }

    public void DateSelection() {

        tv_from_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_from_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instace, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        tv_to_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd-MMM-yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_to_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instace, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
    }

    public void getData(String ID, String ProductID, String FromDate, String ToDate, String productID) {
        if (Type.equalsIgnoreCase("LOCATION WISE SALES")) {
            new CallRequest(instace).area_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("LOCATION WISE ITEM SALES")) {
            new CallRequest(instace).area_item_wise_report(ID, ProductID, FromDate, ToDate);
        }

        //New Added

        if (Type.equalsIgnoreCase("RSM WISE SALES")) {
            new CallRequest(instace).rsm_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("ZSM WISE SALES")) {
            new CallRequest(instace).zsm_wise_report(ID, FromDate, ToDate);
        }

        if (Type.equalsIgnoreCase("CNF WISE SALES")) {
            new CallRequest(instace).cnf_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("CNF WISE ITEM SALES")) {
            new CallRequest(instace).cnf_item_wise_report(ID, productID, FromDate, ToDate);
        }


        if (Type.equalsIgnoreCase("SS WISE SALES")) {
            new CallRequest(instace).ss_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("SS WISE ITEM SALES")) {
            new CallRequest(instace).ss_item_wise_report(ID, productID, FromDate, ToDate);
        }

        //  ---------------------------------------------------------------------------------

        if (Type.equalsIgnoreCase("ASM WISE SALES")) {
            new CallRequest(instace).asm_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("SM WISE SALES")) {
            new CallRequest(instace).sm_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("DISTRIBUTOR WISE SALES")) {
            new CallRequest(instace).distributor_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("DISTRIBUTOR WISE ITEM SALES")) {

            new CallRequest(instace).distributor_item_wise_report(ID, ProductID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("RETAILER WISE SALES")) {

            new CallRequest(instace).retailer_wise_report(ID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("RETAILER WISE ITEM SALES")) {

            new CallRequest(instace).retailer_item_wise_report(ID, ProductID, FromDate, ToDate);
        }
        if (Type.equalsIgnoreCase("ITEM SALES")) {
            new CallRequest(instace).item_wise_report(ID, FromDate, ToDate);
        }

    }

    public void setDrawer() {
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.END);
            }
        });
        drawer.closeDrawers();
    }

    public void getSpinerData(int position) {
        if (Type.equalsIgnoreCase("LOCATION WISE SALES")) {
            SelectedId = String.valueOf(areaArrayList.get(position).getLocationID());
        }
        if (Type.equalsIgnoreCase("LOCATION WISE ITEM SALES")) {
            SelectedId = String.valueOf(areaArrayList.get(position).getLocationID());
        }

        //----------------------------

        if (Type.equalsIgnoreCase("RSM WISE SALES")) {
            SelectedId = String.valueOf(rsmArrayList.get(position).getId());
        }

        if (Type.equalsIgnoreCase("ZSM WISE SALES")) {
            SelectedId = String.valueOf(zsmArrayList.get(position).getId());
        }

        if (Type.equalsIgnoreCase("CNF WISE SALES")) {
            SelectedId = String.valueOf(cnfArrayList.get(position).getId());
        }
        if (Type.equalsIgnoreCase("CNF WISE ITEM SALES")) {
            SelectedId = String.valueOf(cnfArrayList.get(position).getId());
        }

        if (Type.equalsIgnoreCase("SS WISE SALES")) {
            SelectedId = String.valueOf(ssArrayList.get(position).getId());
        }
        if (Type.equalsIgnoreCase("SS WISE ITEM SALES")) {
            SelectedId = String.valueOf(ssArrayList.get(position).getId());
        }

        //-----------------------------

        if (Type.equalsIgnoreCase("ASM WISE SALES")) {
            SelectedId = String.valueOf(asmArrayList.get(position).getId());
        }
        if (Type.equalsIgnoreCase("SM WISE SALES")) {
            SelectedId = String.valueOf(smArrayList.get(position).getId());
        }
        if (Type.equalsIgnoreCase("DISTRIBUTOR WISE SALES")) {
            SelectedId = String.valueOf(distributorArrayList.get(position).getId());
        }
        if (Type.equalsIgnoreCase("DISTRIBUTOR WISE ITEM SALES")) {
            SelectedId = String.valueOf(distributorArrayList.get(position).getId());
        }
        if (Type.equalsIgnoreCase("RETAILER WISE SALES")) {
            SelectedId = String.valueOf(retailerArrayList.get(position).getRetailerID());
        }
        if (Type.equalsIgnoreCase("RETAILER WISE ITEM SALES")) {
            SelectedId = String.valueOf(retailerArrayList.get(position).getRetailerID());

        }
    }

    public void SpinerClickEvent() {

        sp_all.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    getSpinerData(position);
                    Log.i("TAG", "POS :-> " + position);
                    lbl_all.setVisibility(View.GONE);

                } catch (Exception e) {
                    SelectedId = "";

                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        sp_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    SelectedProductId = String.valueOf(productArrayList.get(position).getItemID());
                    Log.i("TAG", "POS :-> " + position);
                    lbl_product.setVisibility(View.GONE);
                    SpinerSetup();

                } catch (Exception e) {
                    SelectedProductId = "";

                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void SpinerSetup() {
        Spineradapter = new ArrayAdapter<String>(instace, android.R.layout.simple_spinner_dropdown_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };
        Spineradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();
                JSONObject jObj = new JSONObject(result);
                Log.d(TAG, "Result>>>: " + result);
                System.out.println(result);
                switch (request) {

                    case zsm_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);

                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jSm = jdata.getJSONArray("ZSM");
                                if (jSm.length() > 0) {
                                    lbl_all.setVisibility(View.GONE);
                                    SpinerSetup();
                                    for (int i = 0; i < jSm.length(); i++) {
                                        JSONObject jData = jSm.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        zsmArrayList.add(asm);
                                        Spineradapter.add(asm.getFirstName() + " " + asm.getLastName());
                                    }
                                    lbl_spiner_title.setText("ZSM");
                                    Spineradapter.add("Select ZSM");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }


                            }
                        }
                        break;

                    case rsm_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);

                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jSm = jdata.getJSONArray("RSM");
                                if (jSm.length() > 0) {
                                    lbl_all.setVisibility(View.GONE);
                                    SpinerSetup();
                                    for (int i = 0; i < jSm.length(); i++) {
                                        JSONObject jData = jSm.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        rsmArrayList.add(asm);
                                        Spineradapter.add(asm.getFirstName() + " " + asm.getLastName());
                                    }
                                    lbl_spiner_title.setText("RSM");
                                    Spineradapter.add("Select RSM");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }


                            }
                        }
                        break;

                    case sm_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);

                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jSm = jdata.getJSONArray("SM");
                                if (jSm.length() > 0) {
                                    lbl_all.setVisibility(View.GONE);
                                    SpinerSetup();
                                    for (int i = 0; i < jSm.length(); i++) {
                                        JSONObject jData = jSm.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        smArrayList.add(asm);
                                        Spineradapter.add(asm.getFirstName() + " " + asm.getLastName());
                                    }
                                    lbl_spiner_title.setText("Sales Maneger");
                                    Spineradapter.add("Select SM");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }


                            }
                        }
                        break;

                    case distributor_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");

                                JSONArray jDistributorarray = jdata.getJSONArray("Distributor");
                                if (jDistributorarray.length() > 0 && jDistributorarray != null) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jDistributorarray.length(); i++) {
                                        JSONObject jData = jDistributorarray.getJSONObject(i);
                                        distributor = (Distributor) jParser.parseJson(jData, new Distributor());
                                        distributorArrayList.add(distributor);
                                        Spineradapter.add(distributor.getDistributorName());
                                    }
                                    lbl_spiner_title.setText("Distributor");
                                    Spineradapter.add("Select Distributor");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }

                            }
                        }
                        break;

                    case distributor_item_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jDistributorarray = jdata.getJSONArray("Distributor");
                                if (jDistributorarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jDistributorarray.length(); i++) {
                                        JSONObject jData = jDistributorarray.getJSONObject(i);
                                        distributor = (Distributor) jParser.parseJson(jData, new Distributor());
                                        distributorArrayList.add(distributor);
                                        Spineradapter.add(distributor.getDistributorName());
                                    }
                                    lbl_spiner_title.setText("Distributor");

                                    Spineradapter.add("Select Distributor");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }


                                JSONArray jProductsarray = jdata.getJSONArray("Products");
                                if (jProductsarray.length() > 0) {
                                    lin_product.setVisibility(View.VISIBLE);
                                    isMultipelSpiner = true;
                                    SpinerSetup();
                                    for (int i = 0; i < jProductsarray.length(); i++) {
                                        JSONObject jData = jProductsarray.getJSONObject(i);
                                        productObj = (Product) jParser.parseJson(jData, new Product());
                                        productArrayList.add(productObj);
                                        Spineradapter.add(productObj.getItemName() + " (" + productObj.getSKU() + ")");
                                    }
                                    Spineradapter.add("Select Products");
                                    if (!isFilter) {
                                        sp_product.setAdapter(Spineradapter);
                                        sp_product.setSelection(Spineradapter.getCount());
                                    }
                                } else {
                                    isMultipelSpiner = false;
                                    lin_product.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;

                    case retailer_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jRetailer = jdata.getJSONArray("Retailer");
                                if (jRetailer.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jRetailer.length(); i++) {
                                        JSONObject jData = jRetailer.getJSONObject(i);
                                        retailerOrder = (RetailerOrder) jParser.parseJson(jData, new RetailerOrder());
                                        retailerArrayList.add(retailerOrder);
                                        Spineradapter.add(retailerOrder.getRetailerName());
                                    }
                                    lbl_spiner_title.setText("Retailer");
                                    Spineradapter.add("Select Retailer");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }

                            }
                        }
                        break;
                    case retailer_item_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");

                                JSONArray jRetailer = jdata.getJSONArray("Retailer");
                                if (jRetailer.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jRetailer.length(); i++) {
                                        JSONObject jData = jRetailer.getJSONObject(i);
                                        retailerOrder = (RetailerOrder) jParser.parseJson(jData, new RetailerOrder());
                                        retailerArrayList.add(retailerOrder);
                                        Spineradapter.add(retailerOrder.getRetailerName());
                                    }
                                    lbl_spiner_title.setText("Retailer");

                                    Spineradapter.add("Select Retailer");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }
                                JSONArray jProductsarray = jdata.getJSONArray("Products");
                                if (jProductsarray.length() > 0) {
                                    lin_product.setVisibility(View.VISIBLE);
                                    isMultipelSpiner = true;
                                    SpinerSetup();
                                    for (int i = 0; i < jProductsarray.length(); i++) {
                                        JSONObject jData = jProductsarray.getJSONObject(i);
                                        productObj = (Product) jParser.parseJson(jData, new Product());
                                        productArrayList.add(productObj);
                                        Spineradapter.add(productObj.getItemName() + " (" + productObj.getSKU() + ")");
                                    }
                                    Spineradapter.add("Select Products");
                                    if (!isFilter) {
                                        sp_product.setAdapter(Spineradapter);
                                        sp_product.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    isMultipelSpiner = false;
                                    lin_product.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;

                    case area_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jarray = jdata.getJSONArray("Area");
                                if (jarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        araObj = (Area) jParser.parseJson(jData, new Area());
                                        areaArrayList.add(araObj);
                                        Spineradapter.add(araObj.getLocationName());
                                    }
                                    lbl_spiner_title.setText("Area");
                                    Spineradapter.add("Select Area");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;
                    case asm_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jarray = jdata.getJSONArray("ASM");
                                if (jarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        asmArrayList.add(asm);
                                        Spineradapter.add(asm.getFirstName() + " " + asm.getLastName());
                                    }
                                    lbl_spiner_title.setText("ASM");
                                    Spineradapter.add("Select ASM");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }

                            }
                        }
                        break;

                    case ss_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jarray = jdata.getJSONArray("SS");
                                if (jarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        ssArrayList.add(asm);
                                        Spineradapter.add(asm.getFirstName() + " " + asm.getLastName());
                                    }
                                    lbl_spiner_title.setText("SS");
                                    Spineradapter.add("Select SS");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }

                            }
                        }
                        break;
                    case ss_item_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jDistributorarray = jdata.getJSONArray("SS");
                                if (jDistributorarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jDistributorarray.length(); i++) {
                                        JSONObject jData = jDistributorarray.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        ssArrayList.add(asm);
                                        Spineradapter.add(asm.getName());
                                    }
                                    lbl_spiner_title.setText("Super Stockiest");

                                    Spineradapter.add("Select SS");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }


                                JSONArray jProductsarray = jdata.getJSONArray("Products");
                                if (jProductsarray.length() > 0) {
                                    lin_product.setVisibility(View.VISIBLE);
                                    isMultipelSpiner = true;
                                    SpinerSetup();
                                    for (int i = 0; i < jProductsarray.length(); i++) {
                                        JSONObject jData = jProductsarray.getJSONObject(i);
                                        productObj = (Product) jParser.parseJson(jData, new Product());
                                        productArrayList.add(productObj);
                                        Spineradapter.add(productObj.getItemName() + " (" + productObj.getSKU() + ")");
                                    }
                                    Spineradapter.add("Select Products");
                                    if (!isFilter) {
                                        sp_product.setAdapter(Spineradapter);
                                        sp_product.setSelection(Spineradapter.getCount());
                                    }
                                } else {
                                    isMultipelSpiner = false;
                                    lin_product.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;

                    case cnf_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jarray = jdata.getJSONArray("CNF");
                                if (jarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        cnfArrayList.add(asm);
                                        Spineradapter.add(asm.getFirstName() + " " + asm.getLastName());
                                    }
                                    lbl_spiner_title.setText("CNF");
                                    Spineradapter.add("Select CNF");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }

                            }
                        }
                        break;

                    case cnf_item_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


                                JSONArray jDistributorarray = jdata.getJSONArray("CNF");
                                if (jDistributorarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jDistributorarray.length(); i++) {
                                        JSONObject jData = jDistributorarray.getJSONObject(i);
                                        asm = (ASM) jParser.parseJson(jData, new ASM());
                                        cnfArrayList.add(asm);
                                        Spineradapter.add(asm.getName());
                                    }
                                    lbl_spiner_title.setText("CNF");

                                    Spineradapter.add("Select CNF");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }


                                JSONArray jProductsarray = jdata.getJSONArray("Products");
                                if (jProductsarray.length() > 0) {
                                    lin_product.setVisibility(View.VISIBLE);
                                    isMultipelSpiner = true;
                                    SpinerSetup();
                                    for (int i = 0; i < jProductsarray.length(); i++) {
                                        JSONObject jData = jProductsarray.getJSONObject(i);
                                        productObj = (Product) jParser.parseJson(jData, new Product());
                                        productArrayList.add(productObj);
                                        Spineradapter.add(productObj.getItemName() + " (" + productObj.getSKU() + ")");
                                    }
                                    Spineradapter.add("Select Products");
                                    if (!isFilter) {
                                        sp_product.setAdapter(Spineradapter);
                                        sp_product.setSelection(Spineradapter.getCount());
                                    }
                                } else {
                                    isMultipelSpiner = false;
                                    lin_product.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;
                    case area_item_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");

                                JSONArray jarray = jdata.getJSONArray("Area");
                                if (jarray.length() > 0) {
                                    SpinerSetup();
                                    lbl_all.setVisibility(View.GONE);
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jData = jarray.getJSONObject(i);
                                        araObj = (Area) jParser.parseJson(jData, new Area());
                                        areaArrayList.add(araObj);
                                        Spineradapter.add(araObj.getLocationName());
                                    }
                                    lbl_spiner_title.setText("Area");
                                    Spineradapter.add("Select Area");
                                    if (!isFilter) {
                                        sp_all.setAdapter(Spineradapter);
                                        sp_all.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    lin_all_spiner.setVisibility(View.GONE);
                                }

                                JSONArray jProductsarray = jdata.getJSONArray("Products");
                                if (jProductsarray.length() > 0) {
                                    lin_product.setVisibility(View.VISIBLE);
                                    isMultipelSpiner = true;
                                    SpinerSetup();
                                    for (int i = 0; i < jProductsarray.length(); i++) {
                                        JSONObject jData = jProductsarray.getJSONObject(i);
                                        productObj = (Product) jParser.parseJson(jData, new Product());
                                        productArrayList.add(productObj);
                                        Spineradapter.add(productObj.getItemName() + " (" + productObj.getSKU() + ")");
                                    }
                                    Spineradapter.add("Select Products");
                                    if (!isFilter) {
                                        sp_product.setAdapter(Spineradapter);
                                        sp_product.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    isMultipelSpiner = false;
                                    lin_product.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;
                    case item_wise_report:
                        Utils.hideProgressDialog();
                        if (respCode.equalsIgnoreCase("401")) {

                        } else {
                            if (jObj.getBoolean("status")) {

                                JSONObject jdata = jObj.getJSONObject("data");
                                DisplayTitle = jdata.getString("DisplayTitle");
                                DisplayDate = jdata.getString("DisplayDate");
                                TotalSales = jdata.getString("TotalSales");
                                tv_total_sales_title.setText(DisplayTitle);
                                tv_price.setText(TotalSales);
                                tv_date.setText(DisplayDate);
                                HTML = jdata.getString("html");
                                web_report.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
                                lin_all_spiner.setVisibility(View.GONE);
                                JSONArray jProductsarray = jdata.getJSONArray("Products");
                                if (jProductsarray.length() > 0 && jProductsarray != null) {
                                    lin_product.setVisibility(View.VISIBLE);
                                    isMultipelSpiner = true;
                                    SpinerSetup();
                                    for (int i = 0; i < jProductsarray.length(); i++) {
                                        JSONObject jData = jProductsarray.getJSONObject(i);
                                        productObj = (Product) jParser.parseJson(jData, new Product());
                                        productArrayList.add(productObj);
                                        Spineradapter.add(productObj.getItemName() + " (" + productObj.getSKU() + ")");
                                    }
                                    Spineradapter.add("Select Products");
                                    if (!isFilter) {
                                        sp_product.setAdapter(Spineradapter);
                                        sp_product.setSelection(Spineradapter.getCount());
                                    }

                                } else {
                                    isMultipelSpiner = false;
                                    lin_product.setVisibility(View.GONE);
                                }
                            }
                        }
                        break;

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
