package com.vk.salesmanager.Activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.vk.salesmanager.App;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.EasyWayLocation;
import com.vk.salesmanager.Utils.Listener;
import com.vk.salesmanager.Utils.Utils;


public class SplashActivity extends AppCompatActivity implements Listener {

    private static int SPLASH_TIME_OUT = 3000;
    public SplashActivity instance;
    RxPermissions rxPermissions;
    String[] PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    int PERMISSION_ALL = 1;
    EasyWayLocation easyWayLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        instance = this;
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }*/
  /*      rxPermissions = new RxPermissions(this);
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean granted) throws Exception {
                        if (granted) {

                        }
                    }
                });*/

        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        }else {
            easyWayLocation = new EasyWayLocation(this);
            easyWayLocation.setListener(this);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (App.mySharedPref.getIsLoggedIn()) {
                    startActivity(new Intent(instance, DashBoardActivity.class));
                    finish();
                } else {


                    startActivity(new Intent(instance, IntroActivity.class));
                    finish();
                }
            }


        }, SPLASH_TIME_OUT);
    }

    @Override
    public void locationOn() {

    }

    @Override
    public void onPositionChanged() {

    }

    @Override
    public void locationCancelled() {

    }
}
