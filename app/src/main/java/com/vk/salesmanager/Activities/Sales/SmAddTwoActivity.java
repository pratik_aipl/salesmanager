package com.vk.salesmanager.Activities.Sales;

import android.content.Intent;
import android.graphics.PorterDuff;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.Area;
import com.vk.salesmanager.Models.City;
import com.vk.salesmanager.Models.States;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SmAddTwoActivity extends AppCompatActivity implements AsynchTaskListner {
    public SmAddTwoActivity instance;
    public EditText et_address, et_area, et_country, et_state, et_city, et_pincode;
    public FloatingActionButton float_previous, float_next;
    public Toolbar toolbar;
    public TextView tv_title,lbl_city, lbl_area;
    public Spinner  sp_state, sp_city, sp_area;
    public JsonParserUniversal jParser;
    public States states;
    public ArrayList<States> statesArray = new ArrayList<>();
    public ArrayList<String> strStatesArray = new ArrayList<>();
    public String states_id = "";
    public City city;
    public ArrayList<City> cityArray = new ArrayList<>();
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String city_id = "";
    public Area area;
    public ArrayList<Area> areaArray = new ArrayList<>();
    public ArrayList<String> strAreaArray = new ArrayList<>();
    public String area_id = "";
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member2);
        instance = SmAddTwoActivity.this;
        jParser = new JsonParserUniversal();
        FindElements();

        new CallRequest(instance).getState();

        SpinerClickEvent();


    }

    public void FindElements() {
        float_previous = findViewById(R.id.float_previous);
        float_next = findViewById(R.id.float_next);
        et_address = findViewById(R.id.et_address);
        et_pincode = findViewById(R.id.et_pincode);
        sp_state = findViewById(R.id.sp_state);
        lbl_area = findViewById(R.id.lbl_area);
        lbl_city = findViewById(R.id.lbl_city);
        sp_city = findViewById(R.id.sp_city);
        sp_area = findViewById(R.id.sp_area);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Add SM");

        float_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        float_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validaton();

            }
        });

        states = new States();
        city = new City();
        area = new Area();
    }


    public void SpinerClickEvent() {

        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    states_id = String.valueOf(statesArray.get(position).getID());
                    Log.i("TAG", "POS :-> " + position);
                    lbl_city.setVisibility(View.GONE);
                    lbl_area.setVisibility(View.VISIBLE);
                    SpinerSetup();
                    adapter.add("Select Area");
                    sp_area.setAdapter(adapter);
                    sp_area.setSelection(adapter.getCount());
                    new CallRequest(instance).getCity(states_id);

                } catch (Exception e) {
                    states_id = "0";
                    city_id = "0";
                    area_id = "-1";
                    lbl_city.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    city_id = String.valueOf(cityArray.get(position).getID());
                    lbl_area.setVisibility(View.GONE);
                    new CallRequest(instance).getArea(city_id);
                } catch (Exception e) {
                    city_id = "0";
                    area_id = "-1";
                    lbl_area.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sp_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    area_id = String.valueOf(areaArray.get(position).getID());
                } catch (Exception e) {
                    area_id = "0";
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    public void SpinerSetup() {
        adapter = new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case getState:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status") == true) {
                            strStatesArray.clear();
                            statesArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");

                            JSONArray jStateArray = jData.getJSONArray("State");
                            if (jStateArray != null && jStateArray.length() > 0) {

                                SpinerSetup();
                                for (int i = 0; i < jStateArray.length(); i++) {
                                    states = (States) jParser.parseJson(jStateArray.getJSONObject(i), new States());
                                    statesArray.add(states);
                                    strStatesArray.add(states.getValue());
                                    adapter.add(states.getValue());
                                }
                                adapter.add("Select State");
                                sp_state.setAdapter(adapter);
                                sp_state.setSelection(adapter.getCount());
                                //sp_state.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strStatesArray));

                            } else {
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getCity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status") == true) {
                            strCityArray.clear();
                            cityArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");
                            JSONArray jCityArray = jData.getJSONArray("CIty");
                            if (jCityArray != null && jCityArray.length() > 0) {
                                SpinerSetup();
                                lbl_city.setVisibility(View.GONE);
                                for (int i = 0; i < jCityArray.length(); i++) {
                                    city = (City) jParser.parseJson(jCityArray.getJSONObject(i), new City());
                                    cityArray.add(city);
                                    strCityArray.add(city.getValue());
                                    adapter.add(city.getValue());
                                }
                                adapter.add("Select City");
                                sp_city.setAdapter(adapter);
                                sp_city.setSelection(adapter.getCount());

                                // sp_city.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));

                            } else {
                                SpinerSetup();
                                adapter.add("Select City");
                                sp_city.setAdapter(adapter);
                                sp_city.setSelection(adapter.getCount());
                                city_id = "-1";
                                lbl_city.setVisibility(View.VISIBLE);
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getArea:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("status") == true) {
                            strAreaArray.clear();
                            areaArray.clear();
                            JSONObject jData = jObj.getJSONObject("data");

                            JSONArray jAreaArray = jData.getJSONArray("Area");
                            if (jAreaArray != null && jAreaArray.length() > 0) {
                                SpinerSetup();
                                lbl_area.setVisibility(View.GONE);
                                for (int i = 0; i < jAreaArray.length(); i++) {
                                    area = (Area) jParser.parseJson(jAreaArray.getJSONObject(i), new Area());
                                    areaArray.add(area);
                                    strAreaArray.add(area.getValue());
                                    adapter.add(area.getValue());
                                }
                                adapter.add("Select area");
                                sp_area.setAdapter(adapter);
                                sp_area.setSelection(adapter.getCount());
                                //  sp_area.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strAreaArray));

                            } else {
                                SpinerSetup();
                                adapter.add("Select area");
                                sp_area.setAdapter(adapter);
                                area_id = "-1";
                                sp_area.setSelection(adapter.getCount());

                                lbl_area.setVisibility(View.VISIBLE);
                                Utils.showToast(jObj.getString("message"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public void Validaton() {
                 App.addMemberObj.setAddress(et_address.getText().toString());
            App.addMemberObj.setState(states_id);
            App.addMemberObj.setCity(city_id);
            App.addMemberObj.setArea(area_id);
            App.addMemberObj.setZipcode(et_pincode.getText().toString());

            startActivity(new Intent(instance, SmAddThreeActivity.class));


    }


}
