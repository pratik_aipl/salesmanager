package com.vk.salesmanager.Activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.OtpView;
import com.vk.salesmanager.Utils.Utils;

public class ConfirmOTPActivity extends AppCompatActivity implements AsynchTaskListner {

    public ConfirmOTPActivity instance;
    public Toolbar toolbar;
    public TextView tv_title;
    public Button btn_confirm_reg;
    public OtpView otp_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_otp);
        instance = ConfirmOTPActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_confirm_reg = findViewById(R.id.btn_confirm_reg);
        otp_view = findViewById(R.id.otp_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        otp_view.setOTP(App.otp);
        tv_title.setText("Confirm OTP");


        btn_confirm_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (App.otp.equals(otp_view.getOTP())) {
                    Intent intent = new Intent(instance, SetPasswordActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Utils.showToast("INVALID OTP", instance);
                }

            }
        });

    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {

    }
}
