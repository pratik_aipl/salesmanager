package com.vk.salesmanager.Activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.Models.MainUser;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class SetPasswordActivity extends AppCompatActivity implements AsynchTaskListner {
    public SetPasswordActivity instance;
    public Toolbar toolbar;
    public TextView tv_title;
    public Button btn_confirm;
    public EditText et_confirm_password, et_password;
    public String device_id = "";
    public MainUser mainUser;
    public JsonParserUniversal jParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        instance = SetPasswordActivity.this;
        jParser = new JsonParserUniversal();
        device_id = Utils.OneSignalPlearID();

        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        btn_confirm = findViewById(R.id.btn_confirm);
        et_confirm_password = findViewById(R.id.et_confirm_password);
        et_password = findViewById(R.id.et_password);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Set New Password");

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_password.getText().toString().equals("")) {
                    et_password.setError("Please enter valid password");
                    et_password.setFocusable(true);
                } else if (et_confirm_password.getText().toString().equals("")) {
                    et_confirm_password.setError("Please enter confirm password");
                    et_confirm_password.setFocusable(true);
                } else if (!et_confirm_password.getText().toString().equals(et_password.getText().toString())) {
                    et_confirm_password.setError("Password and Confirm password not match");
                    et_confirm_password.setFocusable(true);
                } else {
                    new CallRequest(instance).changePasswordLogin(App.mobile_no, et_password.getText().toString(), et_confirm_password.getText().toString());

                }

            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {


                    case changePasswordLogin:

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            Utils.showToast(jObj.getString("message"), this);
                            new CallRequest(instance).getLogin(App.mobile_no, et_password.getText().toString(), device_id);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                    case getLogin:
                        Utils.hideProgressDialog();
                         jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            JSONObject jData = jObj.getJSONObject("data");
                            JSONObject jUser = jData.getJSONObject("user");

                            mainUser = (MainUser) jParser.parseJson(jUser, new MainUser());
                            Gson gson = new Gson();
                            String json = gson.toJson(mainUser);

                            App.mySharedPref.setUserModel(json);
                            App.mySharedPref.setUserId(jData.getString("user_id"));
                            App.mySharedPref.setLoginToken(jData.getString("login_token"));
                            App.mySharedPref.setIsLoggedIn(true);

                            Utils.showToast(jObj.getString("message"), this);
                            Intent intent = new Intent(instance, DashBoardActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }

}
