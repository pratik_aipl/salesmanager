package com.vk.salesmanager.Activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LeaderBoardActivity extends AppCompatActivity implements  AsynchTaskListner {

    private static final String TAG = "LeaderBoardActivity";
    ImageView img_back,img_user_day,img_user_week,img_user_month;
    TextView tv_curnt_area_name,tv_curnt_area_sales,tv_curnt_zone_name,tv_curnt_zone_sales,user_name_day,
    day_area,day_total_sell, user_name_week, week_area,week_total_sell,user_name_month,
    month_area,month_total_sell;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);

        img_back=findViewById(R.id.back);

        img_user_day=findViewById(R.id.img_user_day);
        img_user_week=findViewById(R.id.img_user_week);
        img_user_month=findViewById(R.id.img_user_month);

        tv_curnt_area_name=findViewById(R.id.tv_curnt_area_name);
        tv_curnt_area_sales=findViewById(R.id.tv_curnt_area_sales);
        tv_curnt_zone_name=findViewById(R.id.tv_curnt_zone_name);
        tv_curnt_zone_sales=findViewById(R.id.tv_curnt_zone_sales);
        user_name_day=findViewById(R.id.user_name_day);
        day_area=findViewById(R.id.day_area);
        day_total_sell=findViewById(R.id.day_total_sell);
        user_name_week=findViewById(R.id.user_name_week);
        week_area=findViewById(R.id.week_area);
        week_total_sell=findViewById(R.id.week_total_sell);
        user_name_month=findViewById(R.id.user_name_month);
        month_area=findViewById(R.id.month_area);
        month_total_sell=findViewById(R.id.month_total_sell);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (Utils.isNetworkAvailable(this)){
            new CallRequest(LeaderBoardActivity.this).getLeaderBoardInfo();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) throws JSONException {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();
                switch (request) {
                    case getLeaderBoardInfo:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "leading result>>: "+result);

                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            JSONArray lastday=jData.getJSONArray("LastDay");
                            JSONArray lastweek=jData.getJSONArray("LastWeek");
                            JSONArray lastmonth =jData.getJSONArray("LastMonth");
                            JSONArray CurrentMonthArea =jData.getJSONArray("CurrentMonthArea");
                            JSONArray CurrentMonthZone =jData.getJSONArray("CurrentMonthZone");

                            user_name_day.setText(lastday.getJSONObject(0).getString("Name"));
                            day_area.setText(lastday.getJSONObject(0).getString("Area"));
                            day_total_sell.setText("Total Sales : ₹ "+lastday.getJSONObject(0).getString("Total"));
                            Picasso.with(this)
                                    .load( lastday.getJSONObject(0).getString("Photo"))
                                    .error(R.drawable.profile)
                                    .into(img_user_day);

                            user_name_week.setText(lastweek.getJSONObject(0).getString("Name"));
                            week_area.setText(lastweek.getJSONObject(0).getString("Area"));
                            week_total_sell.setText("Total Sales : ₹ "+lastweek.getJSONObject(0).getString("Total"));
                            Picasso.with(this)
                                    .load(lastweek.getJSONObject(0).getString("Photo"))
                                    .error(R.drawable.profile)
                                    .into(img_user_week);

                            user_name_month.setText(lastmonth.getJSONObject(0).getString("Name"));
                            month_area.setText(lastmonth.getJSONObject(0).getString("Area"));
                            month_total_sell.setText("Total Sales : ₹ "+lastmonth.getJSONObject(0).getString("Total"));
                            Picasso.with(this)
                                    .load(lastmonth.getJSONObject(0).getString("Photo"))
                                    .error(R.drawable.profile)
                                    .into(img_user_month);

                            tv_curnt_area_name.setText(CurrentMonthArea.getJSONObject(0).getString("Area"));
                            tv_curnt_area_sales.setText("Total Sales : ₹ "+CurrentMonthArea.getJSONObject(0).getString("Total"));

                            tv_curnt_zone_name.setText(CurrentMonthZone.getJSONObject(0).getString("Area"));
                            tv_curnt_zone_sales.setText("Total Sales : ₹ "+CurrentMonthZone.getJSONObject(0).getString("Total"));

                        } else {
                            Utils.hideProgressDialog();
                            onBackPressed();
                            Utils.showToast(jObj.getString("message"), this);

                        }
                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        }
    }
}
