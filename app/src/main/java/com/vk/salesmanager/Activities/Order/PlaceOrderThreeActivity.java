package com.vk.salesmanager.Activities.Order;

import android.content.Intent;
import android.graphics.PorterDuff;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;
import com.vk.salesmanager.R;
import com.vk.salesmanager.Utils.CallRequest;
import com.vk.salesmanager.Utils.Constant;
import com.vk.salesmanager.Utils.JsonParserUniversal;
import com.vk.salesmanager.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class PlaceOrderThreeActivity extends AppCompatActivity implements AsynchTaskListner {
    public PlaceOrderThreeActivity instance;
    public JsonParserUniversal jParser;
    public Toolbar toolbar;
    public TextView tv_retailer_name, tv_dis_name, tv_date;
    public RecyclerView rcyclerView;
    public AddOrderAdapter adapter;
    public Button btn_order;
    public Gson gson;
    public String OrderHeaderID = "", OTP = "";
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_oreder_three);

        instance = this;
        jParser = new JsonParserUniversal();
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        btn_order = findViewById(R.id.btn_order);
        rcyclerView = findViewById(R.id.rcyclerView);
        tv_date = findViewById(R.id.tv_date);
        tv_dis_name = findViewById(R.id.tv_dis_name);
        tv_retailer_name = findViewById(R.id.tv_retailer_name);
        tv_date.setText(App.addOrderObj.getDate());
        tv_dis_name.setText(App.addOrderObj.getDistributorName());
        tv_retailer_name.setText(App.addOrderObj.getRetailerName());
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("Place Order");
        SetupRecylerView();
        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gson = new Gson();
                JsonArray json = gson.toJsonTree(App.addOrderObj.tempOrderArrayList).getAsJsonArray();
                Log.i("TAG", "=>" + json.toString());
                new CallRequest(instance).addOrder(App.addOrderObj.getRetailerID(), App.addOrderObj.getDistributorID(), json.toString(),App.addOrderObj.getDate());
            }
        });
    }

    public void SetupRecylerView() {

        adapter = new AddOrderAdapter(App.addOrderObj.tempOrderArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rcyclerView.setLayoutAnimation(controller);
        rcyclerView.scheduleLayoutAnimation();
        rcyclerView.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rcyclerView.setItemAnimator(new DefaultItemAnimator());
        rcyclerView.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request, String respCode) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case addOrder:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("status")) {
                            JSONObject jData = jObj.getJSONObject("data");
                            JSONObject jOrder = jData.getJSONObject("Order");
                            OrderHeaderID = jOrder.getString("OrderHeaderID");
                            Utils.showToast(jObj.getString("message"), instance);

                            startActivity(new Intent(instance, OrderListActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                          /*  OTP = jData.getString("OTP");
                            startActivity(new Intent(instance, VerifyOrderActivity.class)
                                    .putExtra("OrderHeaderID", OrderHeaderID)
                                    .putExtra("OTP", OTP));*/
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}
