package com.vk.salesmanager.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vk.salesmanager.Models.Help;
import com.vk.salesmanager.R;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 8/29/2018.
 */

public class HelpAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Help> mArrayList;
    public Context context;

    public HelpAdapter(ArrayList<Help> moviesList, Context context) {
        mArrayList = moviesList;
         this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_number, tv_name;

        public MyViewHolder(View view) {
            super(view);
            tv_number = view.findViewById(R.id.tv_number);
            tv_name = view.findViewById(R.id.tv_name);
          }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_help_raw, parent, false));

    }

    public void bindHolder(final MyViewHolder holder, final int pos) {
        Help order = mArrayList.get(pos);
        holder.tv_name.setText(order.getName().toString());
        holder.tv_number.setText(order.getMobile().toString());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder footerHolder = (MyViewHolder) holder;
        bindHolder(footerHolder, position);


    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


}
