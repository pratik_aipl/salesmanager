package com.vk.salesmanager.Utils;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {
    // public static String BASE_URL = "http://sales.clientworkspace.biz/";
    //public static String BASE_URL ="https://test.tscore.parshvaa.com/sales/";
    public enum POST_TYPE {
        POST_WITH_HEADER, POST, GET, POST_WITH_IMAGE, POST_WITH_JSON;


    }

    public enum FILTER_TYPE {
        APPLIEDRANK, SHIPTYPE;
    }

    public enum REQUESTS {
        changePasswordLogin, getLogin, forgotPassword, getASM, getRSM, getProfile, getSalesDetails, getCountry,
        getState, getCity, getArea, editProfile, AddMember, AddSS, EditSS, AddRSM, getUserLogin,
        getSM, AddRetailer, AddDistributor, AddSM, getDistributor, getCNF, getSS, getRetailor,
        getCompanyLocation, getSSCompanyLocation, EditAsm, EditRsm, EditSM, EditRetailer,
        EditDistributor, getDistributorOrder,getDistributorStock,getDSRDetails, getRetailorOrder, getProduct, addOrder, verifyOrder,
        getSmOrder, getOrderDetail, orderConfirm, orderCancel, help, report, sm_wise_report,
        distributor_wise_report, retailer_wise_report, distributor_item_wise_report, ss_item_wise_report,cnf_item_wise_report,retailer_item_wise_report,
        asm_wise_report, zsm_wise_report,rsm_wise_report, ss_wise_report,cnf_wise_report, area_wise_report, area_item_wise_report,
        item_wise_report, define_route, getRoute, getAssignRoute, getSmAssignRouteList,
        getDSR,getAdminSales, assign_route, getAssignRouteSM, checkUser,getLeaderBoardInfo
    }


}
