package com.vk.salesmanager.Utils;

import android.content.Context;
import android.os.AsyncTask;
import androidx.fragment.app.Fragment;
import android.util.Log;

import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsynchTaskListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static android.content.ContentValues.TAG;


/**
 * Created by admin on 10/11/2016.
 */
public class AsynchHttpRequest {
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public boolean header = false;
    public String token, user_id;
    public String tempData = "";

    public AsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }
            if (map.containsKey("header")) {
                header = true;
                token = map.get("token");
                user_id = map.get("user_id");
            }

            new MyRequest().execute(this.map);
        } else {
            try {
                aListner.onTaskCompleted(null, request, "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!map.containsKey("show")) {
                Utils.showToast("No Internet, Please try again later", ft.getActivity());
            }
        }
    }

    public Context ct;

    public AsynchHttpRequest(Context ct, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ct;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ct;
        this.post_type = post_type;
        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ct);
            } else {
                this.map.remove("show");
            }
            if (map.containsKey("header")) {
                header = true;
                token = map.get("token");
                user_id = map.get("user_id");
            }
            new MyRequest().execute(this.map);
        } else {
            try {
                aListner.onTaskCompleted(null, request, "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!map.containsKey("show")) {
                Utils.showToast("No Internet, Please try again later", ct);
            }
        }
    }

    class MyRequest extends AsyncTask<Map<String, String>, Void, String[]> {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));
            String[] responseBody = new String[]{"", ""};
            final String basicAuth = "Basic VC1TQUxFUy1BRE1JTjpBUElAU0FMRVMhIyRXRUIk";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient = getNewHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        map[0].remove("url");

                        List<String> values = new ArrayList<String>(map[0].values());
                        List<String> keys = new ArrayList<String>(map[0].keySet());

                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();

                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i);
                            tempData += "\n" + keys.get(i) + "====>" + values.get(i);
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }


                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);
                        httpGet.setHeader("Authorization", basicAuth);
                        httpGet.addHeader("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        httpGet.addHeader("X-SALES-LOGIN-TOKEN", App.mySharedPref.getLoginToken());
                        httpGet.addHeader("USER-ID", App.mySharedPref.getUserId());
                        Log.d(TAG, "USERID>>>: "+App.mySharedPref.getUserId());
                        Log.d(TAG, "Login Token >>>: "+App.mySharedPref.getLoginToken());
                        HttpResponse httpResponse = httpClient.execute(httpGet);

                        int respCode = httpResponse.getStatusLine().getStatusCode();
                        HttpEntity httpEntity = httpResponse.getEntity();

                        if (respCode == 401) {

                            return new String[]{"401", ""};
                        } else {

                            responseBody[0] = EntityUtils.toString(httpEntity);
                            responseBody[1] = String.valueOf(respCode);

                            System.out.println("Responce" + "====>" + responseBody[0]);
                            tempData += "\n" + "Responce" + "====>" + responseBody[0];
                            //writeToSDFile(tempData);
                            return responseBody;
                        }

                    case POST:
                        tempData = "";
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        postRequest.setHeader("Authorization", basicAuth);

                        postRequest.addHeader("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        httpClient = getNewHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            tempData += "\n" + key + "====>" + map[0].get(key);
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpClient.execute(postRequest);

                        //  writeToSDFile(tempData);
                        respCode = response.getStatusLine().getStatusCode();


                        if (respCode == 401) {

                            return new String[]{"401", ""};
                        } else {

                            httpEntity = response.getEntity();

                            responseBody[0] = EntityUtils.toString(httpEntity);
                            responseBody[1] = String.valueOf(respCode);
                            tempData += "\n" + "Responce" + "====>" + responseBody[0];
                            System.out.println("Responce" + "====>" + responseBody[0]);
                            System.out.println("respCode" + "====>" + respCode);

                            return responseBody;

                        }
                        //
                        // return responseBody;
                    case POST_WITH_JSON:
                        String result = "";
                        httpClient = getNewHttpClient();
                        postRequest = new HttpPost(map[0].get("url"));
                        postRequest.setHeader("Authorization", basicAuth);
                        postRequest.addHeader("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        postRequest.addHeader("X-SALES-LOGIN-TOKEN", App.mySharedPref.getLoginToken());
                        postRequest.addHeader("USER-ID", App.mySharedPref.getUserId());

                        nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }

                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                        httpResponse = httpClient.execute(postRequest);
                        httpEntity = httpResponse.getEntity();


                        result = EntityUtils.toString(httpEntity, "UTF-8");

                        responseBody[0] = result;
                        responseBody[1] = "";
                        System.out.println("Responce" + "====>" + responseBody[0]);

                        return responseBody;
                    case POST_WITH_HEADER:
                        tempData = "";
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        postRequest = new HttpPost(map[0].get("url"));
                        postRequest.setHeader("Authorization", basicAuth);
                        postRequest.addHeader("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        postRequest.addHeader("X-SALES-LOGIN-TOKEN", App.mySharedPref.getLoginToken());
                        postRequest.addHeader("USER-ID", App.mySharedPref.getUserId());
                        httpClient = getNewHttpClient();
                        nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            tempData += "\n" + key + "====>" + map[0].get(key);
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        //  System.out.println("USER-ID::" + App.mySharedPref.getUserId());
                        //  System.out.println("LOGIN-TOKEN::" + App.mySharedPref.getLoginToken());

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        response = httpClient.execute(postRequest);

                        //  writeToSDFile(tempData);

                        respCode = response.getStatusLine().getStatusCode();


                        if (respCode == 401) {

                            return new String[]{"401", ""};
                        } else {

                            httpEntity = response.getEntity();
                            responseBody[0] = EntityUtils.toString(httpEntity);
                            responseBody[1] = String.valueOf(respCode);
                            tempData += "\n" + "Responce" + "====>" + responseBody[0];
                            System.out.println("Responce" + "====>" + responseBody[0]);

                            return responseBody;

                        }

                    case POST_WITH_IMAGE:

                        String charset = "UTF-8";
                        try {
                            httpClient = getNewHttpClient();
                            postRequest = new HttpPost(map[0].get("url"));
                            postRequest.setHeader("Authorization", basicAuth);
                            postRequest.addHeader("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                            postRequest.addHeader("X-SALES-LOGIN-TOKEN", App.mySharedPref.getLoginToken());
                            postRequest.addHeader("USER-ID", App.mySharedPref.getUserId());
                            if (header) {
                                postRequest.addHeader("X-SALES-LOGIN-TOKEN", token);
                                postRequest.addHeader("USER-ID", user_id);
                            }
                            reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                                @Override
                                public void transferred(long num) {
                                }
                            });
                            BasicHttpContext localContext = new BasicHttpContext();
                            for (String key : map[0].keySet()) {
                                System.out.println();
                                System.out.println(key + "====>" + map[0].get(key));
                                if (matchKeysForImages(key)) {
                                    if (map[0].get(key) != null) {
                                        if (map[0].get(key).length() > 1) {
                                            System.out.println("in inner if");
                                            String type = "image/png";
                                            File f = new File(map[0].get(key));
                                            FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                            reqEntity.addPart(key, fbody);
                                        }
                                    }
                                } else {
                                    if (map[0].get(key) == null) {
                                        reqEntity.addPart(key, new StringBody(""));
                                    } else {
                                        reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                    }
                                }
                            }
                            postRequest.setEntity(reqEntity);
                            HttpResponse responses = httpClient.execute(postRequest, localContext);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                    "UTF-8"));
                            String sResponse;
                            while ((sResponse = reader.readLine()) != null) {
                                responseBody[0] = responseBody[0] + sResponse;
                            }
                            responseBody[1] = String.valueOf(responses.getStatusLine().getStatusCode());
                            System.out.println("Responce" + "====>" + responseBody[0]);
                            return responseBody;

                        } catch (IOException e) {
                            Utils.hideProgressDialog();
                            Log.d("ExPostActivity", e.getMessage());
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
                Utils.hideProgressDialog();
                try {
                    aListner.onTaskCompleted(null, request, "");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            return null;

        }


        @Override
        protected void onPostExecute(String[] result) {

            try {
                aListner.onTaskCompleted(result[0], request, result[1]);

            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
        }
    }

    public boolean matchKeysForImages(String key) {

        if (Pattern.compile(Pattern.quote("image"), Pattern.CASE_INSENSITIVE).matcher(key).find()) {
            return true;
        }

        if (key.contains("image") ||
                key.equalsIgnoreCase("UniqueIdentityImage") ||
                key.equalsIgnoreCase("Photo") ||
                key.equalsIgnoreCase("Image") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}
