package com.vk.salesmanager.Utils;

import android.content.Context;
import androidx.fragment.app.Fragment;

import com.vk.salesmanager.App;
import com.vk.salesmanager.BuildConfig;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest {

    public static String URL = BuildConfig.API_URL + "api/v1/";

    public App app;
    public Context ct;
    public Fragment ft;

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public void checkUser(String mobile) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "auth/check-user");
        map.put("mobile", mobile);
        new AsynchHttpRequest(ct, Constant.REQUESTS.checkUser, Constant.POST_TYPE.POST, map);

    }

    public void changePasswordLogin(String mobile, String new_password, String confirm_new_password) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "auth/change-password-login");
        map.put("mobile", mobile);
        map.put("new_password", new_password);
        map.put("confirm_new_password", confirm_new_password);
        new AsynchHttpRequest(ct, Constant.REQUESTS.changePasswordLogin, Constant.POST_TYPE.POST, map);

    }

    public void getLogin(String mobile, String password, String device_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "auth/login");
        map.put("mobile", mobile);
        map.put("password", password);
        map.put("device_id", device_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.POST, map);
    }

    public void forgotPassword(String mobile) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "auth/forgot-password");
        map.put("mobile", mobile);
        new AsynchHttpRequest(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }


    public void GetClosingStock(String ID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor/product_manage/" + ID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributorStock, Constant.POST_TYPE.GET, map);
    }

    public void GetDSRDetails(String ID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/daily_sales_report/" + ID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDSRDetails, Constant.POST_TYPE.GET, map);
    }

    public void getProfile() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "auth/profile");
        map.put("show", "");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getProfile, Constant.POST_TYPE.GET, map);
    }

    public void getAdminSalesData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "Order/dashboard");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getSalesDetails, Constant.POST_TYPE.GET, map);
    }

    public void getLeaderBoardInfo() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/leaderboard");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getLeaderBoardInfo, Constant.POST_TYPE.GET, map);
    }

    public void getCountry() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "location/country");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCountry, Constant.POST_TYPE.GET, map);
    }

    public void getCompanyLocation() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "location/company_location/" + App.mainUser.getCompanyLocationID());
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCompanyLocation, Constant.POST_TYPE.GET, map);
    }

    public void getSSCompanyLocation() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "location/ss_company_location");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getSSCompanyLocation, Constant.POST_TYPE.GET, map);
    }

    public void getState() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "location/state");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getState, Constant.POST_TYPE.GET, map);
    }

    public void getCity(String state_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "location/city/" + state_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCity, Constant.POST_TYPE.GET, map);
    }

    public void getArea(String area_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "location/area/" + area_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getArea, Constant.POST_TYPE.GET, map);
    }

    public void getASM() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "asm");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getASM, Constant.POST_TYPE.GET, map);
    }
    public void getFragASM() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "asm");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getASM, Constant.POST_TYPE.GET, map);
    }

  /*  public void getASMById(String SS_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "asm?SSID="+SS_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getASMById, Constant.POST_TYPE.GET, map);
    }*/

    public void getRSM() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "zsm/rsm_list");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getRSM, Constant.POST_TYPE.GET, map);
    }

    public void getSM() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "sm");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getSM, Constant.POST_TYPE.GET, map);
    }
    public void getFragSM() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "sm");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSM, Constant.POST_TYPE.GET, map);
    }

    public void getRetailor() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "retailer");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getRetailor, Constant.POST_TYPE.GET, map);
    }

    public void getDistributor() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributor, Constant.POST_TYPE.GET, map);
    }

    public void getCNF() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "cnf");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCNF, Constant.POST_TYPE.GET, map);
    }

    public void getSS() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "ss");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getSS, Constant.POST_TYPE.GET, map);
    }

    public void AddMember(String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                          String Address, String Area, String City, String State,
                          String Zipcode, String CompanyLocationID, String Email,
                          String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "asm");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddMember, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void AddSS(String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                      String Address, String Area, String City, String State,
                      String Zipcode, String CompanyLocationID, String Email,
                      String Mobile, String UniqueIdentityImage, String Photo, String RSMId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "zsm/ss_add");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        map.put("RSMUserID", RSMId);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddSS, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditSS(String UserID, String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                       String Address, String Area, String City, String State,
                       String Zipcode, String CompanyLocationID, String Email,
                       String Mobile, String UniqueIdentityImage, String Photo, String RSMId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "zsm/ss_add");
        map.put("UserID", UserID);
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        map.put("RSMUserID", RSMId);
        new AsynchHttpRequest(ct, Constant.REQUESTS.EditSS, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void AddRSM(String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                       String Address, String Area, String City, String State,
                       String Zipcode, String CompanyLocationID, String Email,
                       String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "zsm/rsm_add");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddRSM, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditRsm(String UserID, String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                        String Address, String Area, String City, String State,
                        String Zipcode, String CompanyLocationID, String Email,
                        String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "zsm/rsm_add");
        map.put("UserID", UserID);
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.EditRsm, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditAsm(String UserID, String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                        String Address, String Area, String City, String State,
                        String Zipcode, String CompanyLocationID, String Email,
                        String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "asm");
        map.put("UserID", UserID);
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.EditAsm, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void editProfile(String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                            String Address, String Area, String City, String State,
                            String Zipcode, String CompanyLocationID, String Email,
                            String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "auth/profile");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.editProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void AddSM(String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                      String Address, String Area, String City, String State,
                      String Zipcode, String CompanyLocationID, String Email,
                      String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "sm");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddSM, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditSM(String UserID, String FirstName, String LastName, String DOB, String UniqueIdentityNO,
                       String Address, String Area, String City, String State,
                       String Zipcode, String CompanyLocationID, String Email,
                       String Mobile, String UniqueIdentityImage, String Photo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "sm");
        map.put("UserID", UserID);
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.EditSM, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void AddRetailer(String FirstName, String LastName, String RetailerName, String DOB, String UniqueIdentityNO,
                            String Address, String Area, String City, String State,
                            String Zipcode, String CompanyLocationID, String Email,
                            String Mobile, String UniqueIdentityImage, String Photo, String SMUserID, String DistID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "retailer");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("RetailerName", RetailerName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        map.put("ContactNo", "");
        map.put("CompanyURL", "");
        map.put("SMUserID", SMUserID);
        map.put("ReportingTO", DistID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddRetailer, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditRetailer(String UserID, String FirstName, String LastName, String RetailerName, String DOB, String UniqueIdentityNO,
                             String Address, String Area, String City, String State,
                             String Zipcode, String CompanyLocationID, String Email,
                             String Mobile, String UniqueIdentityImage, String Photo, String SMUserID, String DistID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "retailer");
        map.put("UserID", UserID);
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("RetailerName", RetailerName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        map.put("ContactNo", "");
        map.put("CompanyURL", "");
        map.put("SMUserID", SMUserID);
        map.put("ReportingTO", DistID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.EditRetailer, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void AddDistributor(String FirstName, String LastName, String DOB, String DistributorName, String UniqueIdentityNO,
                               String Address, String Area, String City, String State,
                               String Zipcode, String CompanyLocationID, String Email,
                               String Mobile, String UniqueIdentityImage, String Photo, String SSUserID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor");
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DOB", DOB);
        map.put("DistributorName", DistributorName);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        map.put("ContactNo", "");
        map.put("CompanyURL", "");
        map.put("ASMUserID", App.mainUser.getUserID());
        map.put("SSMUserID", SSUserID);

        new AsynchHttpRequest(ct, Constant.REQUESTS.AddDistributor, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditDistributor(String UserID, String FirstName, String LastName, String DistributorName, String DOB, String UniqueIdentityNO,
                                String Address, String Area, String City, String State,
                                String Zipcode, String CompanyLocationID, String Email,
                                String Mobile, String UniqueIdentityImage, String Photo, String SSUserID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor");
        map.put("UserID", UserID);
        map.put("FirstName", FirstName);
        map.put("LastName", LastName);
        map.put("DistributorName", DistributorName);
        map.put("DOB", DOB);
        map.put("UniqueIdentityNO", UniqueIdentityNO);
        map.put("Address", Address);
        map.put("Area", Area);
        map.put("City", City);
        map.put("State", State);
        map.put("Zipcode", Zipcode);
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("Email", Email);
        map.put("Mobile", Mobile);
        map.put("UniqueIdentityImage", UniqueIdentityImage);
        map.put("Photo", Photo);
        map.put("ContactNo", "");
        map.put("CompanyURL", "");
        map.put("ASMUserID", App.mainUser.getUserID());
        map.put("SSMUserID", SSUserID);

        new AsynchHttpRequest(ct, Constant.REQUESTS.EditDistributor, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getDistributorOrder() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor/order");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributorOrder, Constant.POST_TYPE.GET, map);
    }

    public void getRetailorOrder() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "retailer/list");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getRetailorOrder, Constant.POST_TYPE.GET, map);
    }

    public void getDistributorOrder(String RetailerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor/list/" + RetailerID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributorOrder, Constant.POST_TYPE.GET, map);
    }

    public void getProduct() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "product");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getProduct, Constant.POST_TYPE.GET, map);
    }

    public void addOrder(String RetailerID, String DistributorID, String Items, String OrderDate) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "Order");
        map.put("RetailerID", RetailerID);
        map.put("DistributorID", DistributorID);
        map.put("Items", Items);
        map.put("OrderDate", OrderDate);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addOrder, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void verifyOrder(String OrderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "order/verify");
        map.put("OrderID", OrderID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.verifyOrder, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void getSmOrder() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "sm/order");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributorOrder, Constant.POST_TYPE.GET, map);
    }

    public void getAsmOrder() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "asm/order");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributorOrder, Constant.POST_TYPE.GET, map);
    }

    public void getRsmOrder() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "rsm/order");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDistributorOrder, Constant.POST_TYPE.GET, map);
    }

    public void getOrderDetail(String OrderHeaderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor/order_detail/" + OrderHeaderID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getOrderDetail, Constant.POST_TYPE.GET, map);
    }

    public void orderConfirm(String Items) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor/order_confirm");
        map.put("Items", Items);
        new AsynchHttpRequest(ct, Constant.REQUESTS.orderConfirm, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void orderCancel(String OrderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "distributor/order_cancel");
        map.put("OrderID", OrderID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.orderCancel, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void help() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "help");
        new AsynchHttpRequest(ct, Constant.REQUESTS.help, Constant.POST_TYPE.GET, map);
    }

    public void report() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report");
        new AsynchHttpRequest(ct, Constant.REQUESTS.report, Constant.POST_TYPE.GET, map);
    }

    public void sm_wise_report(String SMID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/sm_wise_report?");
        map.put("SMID", SMID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.sm_wise_report, Constant.POST_TYPE.GET, map);
    }


    public void distributor_wise_report(String DistributorID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/distributor_wise_report?");
        map.put("DistributorID", DistributorID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.distributor_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void distributor_item_wise_report(String DistributorID, String ProductID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/distributor_item_wise_report?");

        map.put("DistributorID", DistributorID);
        map.put("ProductID", ProductID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.distributor_item_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void ss_item_wise_report(String SSID, String ProductID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/ss_item_wise_report?");

        map.put("SSID", SSID);
        map.put("ProductID", ProductID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.ss_item_wise_report, Constant.POST_TYPE.GET, map);
    }
    public void cnf_item_wise_report(String CNFID, String ProductID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<>();
        map.put("url", URL + "report/cnf_item_wise_report?");

        map.put("CNFID", CNFID);
        map.put("ProductID", ProductID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.cnf_item_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void retailer_wise_report(String RetailerID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/retailer_wise_report?");

        map.put("RetailerID", RetailerID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.retailer_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void retailer_item_wise_report(String RetailerID, String ProductID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/retailer_item_wise_report?");

        map.put("RetailerID", RetailerID);
        map.put("ProductID", ProductID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.retailer_item_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void asm_wise_report(String ASMID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/asm_wise_report?");

        map.put("ASMID", ASMID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.asm_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void zsm_wise_report(String ZSMID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/zsm_wise_report?");

        map.put("ZSMID", ZSMID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.zsm_wise_report, Constant.POST_TYPE.GET, map);
    }
    public void rsm_wise_report(String RSMID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/rsm_wise_report?");

        map.put("RSMID", RSMID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.rsm_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void ss_wise_report(String SSID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/ss_wise_report?");

        map.put("SSID", SSID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.ss_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void cnf_wise_report(String CNFID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<>();
        map.put("url", URL + "report/cnf_wise_report?");

        map.put("CNFID", CNFID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.cnf_wise_report, Constant.POST_TYPE.GET, map);
    }


    public void area_wise_report(String AreaID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/area_wise_report?");

        map.put("AreaID", AreaID);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.area_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void area_item_wise_report(String AreaID, String ProductID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/area_item_wise_report?");

        map.put("AreaID", AreaID);
        map.put("ProductID", ProductID);

        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.area_item_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void item_wise_report(String ProductID, String from_date, String to_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "report/item_wise_report?");


        map.put("ProductID", ProductID);

        map.put("from_date", from_date);
        map.put("to_date", to_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.item_wise_report, Constant.POST_TYPE.GET, map);
    }

    public void define_route(String CompanyLocationID, String From, String To, String RounteNo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "route?");
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("From", From);
        map.put("To", To);
        map.put("RounteNo", RounteNo);

        new AsynchHttpRequest(ct, Constant.REQUESTS.define_route, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void define_route_edit(String CompanyLocationID, String From, String To, String RounteNo, String RouteID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "route?");
        map.put("CompanyLocationID", CompanyLocationID);
        map.put("From", From);
        map.put("To", To);
        map.put("RounteNo", RounteNo);
        map.put("RouteID", RouteID);

        new AsynchHttpRequest(ct, Constant.REQUESTS.define_route, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void getRoute() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "route");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getRoute, Constant.POST_TYPE.GET, map);
    }

    public void getAssignRoute(String SMID, String From, String To) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "route/sm?");
        map.put("SMID", SMID);
        map.put("From", From);
        map.put("To", To);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getAssignRoute, Constant.POST_TYPE.GET, map);
    }

    public void getSmAssignRouteList(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "route/lists/" + id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getSmAssignRouteList, Constant.POST_TYPE.GET, map);
    }

    public void assign_route(String Date, String RouteID, String SMID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "route/sm?");
        map.put("Date", Date);
        map.put("RouteID", RouteID);
        map.put("SMID", SMID);

        new AsynchHttpRequest(ct, Constant.REQUESTS.assign_route, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }


    public void Add_DSR(String OrderDate, String Retailer, String TotalHours,
                        String Remarks, String OrderPlace, String OrderNumber, String usertype, double lati, double longi) {
        Map<String, String> map = new HashMap<>();
        map.put("url", URL + "sm/daily_sales_report");

        map.put("OrderDate", OrderDate);
        map.put("Retailer", Retailer);
        map.put("TotalHours", TotalHours);
        map.put("Remarks", Remarks);
        map.put("OrderPlace", OrderPlace);
        map.put("OrderNumber", OrderNumber);

        map.put("DSRType", usertype);
        map.put("CheckInOut", usertype);
        map.put("Latitude", String.valueOf(lati));
        map.put("Longitude",String.valueOf(longi));

        new AsynchHttpRequest(ct, Constant.REQUESTS.getDSR, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

    public void getAssignRouteSM(String From, String To) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "sm/route?");
        map.put("From", From);
        map.put("To", To);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getAssignRouteSM, Constant.POST_TYPE.GET, map);
    }

    public void Get_admin_sales_sm_list(String TYPE) {
        Map<String, String> map = new HashMap<>();
        map.put("url", URL + "Order/report");
        map.put("Type", TYPE);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getAdminSales, Constant.POST_TYPE.POST_WITH_HEADER, map);
    }

}
