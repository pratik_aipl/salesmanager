package com.vk.salesmanager.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.vk.salesmanager.Activities.LoginActivity;
import com.vk.salesmanager.App;
import com.vk.salesmanager.Interface.AsyncTaskListner;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

/**
 * Created by Krupa Kakkad on 25 June 2018
 */
public class AsyncHttpRequestNew {


    private static final String TAG = "Demo";
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsyncTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public Context ct;

    public AsyncHttpRequestNew(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.ct = ft.getActivity();
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {

            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity(), "Please Wait...");
            }


            //new MyRequest().execute(this.map);

            callMyRequest(this.map);

        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }

    }

    public AsyncHttpRequestNew(Context ct, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ct;
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ct;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(this.ct)) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ct, "Please Wait...");
            }
            //new MyRequest().execute(this.map);
            callMyRequest(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", this.ct);
        }
    }

    private void callMyRequest(Map<String, String> map) {
        final String[] responseBody = {""};
        final String basicAuth = "Basic " + Base64.encodeToString("T-SALES-ADMIN:API@SALES!#$WEB$".getBytes(), Base64.NO_WRAP);

        try {
            switch (post_type) {
                case GET:
                    String tempData = "";
                    String query = map.get("url");
                    tempData += "\n\n\n\n URL : " + map.get("url");

                    map.remove("url");

                    List<String> values = new ArrayList<String>(map.values());
                    List<String> keys = new ArrayList<String>(map.keySet());

                    for (int i = 0; i < values.size(); i++) {
                        System.out.println();

                        System.out.println(keys.get(i) + "====>" + values.get(i));
                        query = query + keys.get(i) + "=" + values.get(i);
                        tempData += "\n" + keys.get(i) + "====>" + values.get(i);
                        if (i < values.size() - 1) {
                            query += "&";
                        }
                    }
                    System.out.println("URL" + "====>" + query);

                    AndroidNetworking.get(query)
                            //.get("https://fierce-cove-29863.herokuapp.com/getAllUsers/{pageNumber}")
                            //.addPathParameter("pageNumber", "0")
                            //.addQueryParameter("limit", "3")
                            //.addHeaders("token", "1234")
                            //.setTag("test")
                            .setPriority(Priority.LOW)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // do anything with response

                                    System.out.println("Response -----> " + response);

                                    responseBody[0] = response.toString();
                                    aListner.onTaskCompleted(responseBody[0], request);
                                }

                                @Override
                                public void onError(ANError error) {
                                    // handle error
                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Run your task here
                                            Utils.showAlert("Something went wrong", ct);
                                        }
                                    }, 1000);

                                    aListner.onTaskCompleted(null, request);

                                    System.out.println("Error -----> " + error);
                                }
                            });
                    break;

                case POST:
                    tempData = "";
                    System.out.println("new Requested URL >> " + map.get("url"));
                    tempData += "\n\n\n\n URL : " + map.get("url");


                    AndroidNetworking.post(map.get("url"))
                            .addUrlEncodeFormBodyParameter(map) // posting map
                            .addHeaders("Authorization", basicAuth)
                            .addHeaders("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss")
                            .addHeaders("X-SALES-LOGIN-TOKEN", App.mySharedPref.getLoginToken())
                            .addHeaders("USER-ID", App.mySharedPref.getUserId())

                            .setTag("test")
                            .setPriority(Priority.MEDIUM)
                            .build()

                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // do anything with response

                                    System.out.println("Response -----> " + response);

                                    responseBody[0] = response.toString();
                                    aListner.onTaskCompleted(responseBody[0], request);
                                }

                                @Override
                                public void onError(ANError error) {
                                    // handle error

                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Run your task here
                                            Utils.showAlert("Something went wrong", ct);
                                        }
                                    }, 1000);

                                    aListner.onTaskCompleted(null, request);

                                    System.out.println("Error -----> " + error);
                                }
                            });

                    break;

                case POST_WITH_IMAGE:

                    Map<String, File> fileMap = new HashMap<>();

                    for (String key : map.keySet()) {
                        System.out.println();
                        System.out.println(key + "====>" + map.get(key));
                        if (matchKeysForImages(key)) {
                            if (map.get(key) != null) {
                                if (map.get(key).length() > 1) {
                                    String type = "image/png";
                                    File f = new File(map.get(key));

                                    fileMap.put(key, f);
                                }
                            }
                        }

                    }

                    AndroidNetworking.upload(map.get("url"))
                            .addHeaders("Authorization", basicAuth)
                            .addHeaders("X-SALES-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss")
                            .addHeaders("X-SALES-LOGIN-TOKEN", App.mySharedPref.getLoginToken())
                            .addHeaders("USER-ID", App.mySharedPref.getUserId())


                            .addMultipartFile(fileMap)
                            .addMultipartParameter(map)
                            .setTag("test")
                            .setPriority(Priority.HIGH)
                            .setExecutor(Executors.newSingleThreadExecutor()) // setting an executor to get response or completion on that executor thread
                            .build()
                            .setAnalyticsListener(new AnalyticsListener() {
                                @Override
                                public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
                                    Log.d(TAG, " timeTakenInMillis : " + timeTakenInMillis);
                                    Log.d(TAG, " bytesSent : " + bytesSent);
                                    Log.d(TAG, " bytesReceived : " + bytesReceived);
                                    Log.d(TAG, " isFromCache : " + isFromCache);
                                }
                            })

                            .setUploadProgressListener(new UploadProgressListener() {
                                @Override
                                public void onProgress(long bytesUploaded, long totalBytes) {
                                    // do anything with progress
                                }
                            })
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // do anything with response

                                    System.out.println("Response -----> " + response);

                                    responseBody[0] = response.toString();

                                    aListner.onTaskCompleted(responseBody[0], request);
                                }
                                @Override
                                public void onError(ANError error) {
                                    // handle error

                                    if (error.getErrorCode() != 0 && error.getErrorCode() == 401) {
                                        // received ANError from server
                                        // error.getErrorCode() - the ANError code from server
                                        // error.getErrorBody() - the ANError body from server
                                        // error.getErrorDetail() - just a ANError detail
                                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());

                                        App.mySharedPref.clearApp();

                                        Intent intent = new Intent(ct, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        ct.startActivity(intent);
                                    } else {
                                        // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                                    }
                                    Log.d(TAG, "onError errorDetail : " + error.getErrorBody());
                                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Run your task here
                                            Utils.showAlert(responseBody[0], ct);
                                        }
                                    }, 1000);

                                    aListner.onTaskCompleted(error.getErrorBody(), request);

                                    System.out.println("Error -----> " + error);
                                }
                            });

                    break;


            }

        } catch (Exception e) {
            e.printStackTrace();


            aListner.onTaskCompleted(null, request);
        }
    }

    public boolean matchKeysForImages(String key) {


        if (key.equalsIgnoreCase("UniqueIdentityImage") ||
                key.equalsIgnoreCase("Photo") ||
                key.equalsIgnoreCase("Image3") ||
                key.equalsIgnoreCase("Image4") ||
                key.equalsIgnoreCase("Image5") ||
                key.equalsIgnoreCase("Image6") ||
                key.equalsIgnoreCase("Image7") ||
                key.equalsIgnoreCase("Image8") ||
                key.equalsIgnoreCase("Image9") ||
                key.equalsIgnoreCase("Image10") ||
                key.equalsIgnoreCase("Image11") ||
                key.equalsIgnoreCase("Image12") ||
                key.equalsIgnoreCase("Image13") ||
                key.equalsIgnoreCase("Image14") ||
                key.equalsIgnoreCase("Image15") ||
                key.equalsIgnoreCase("Image16") ||
                key.equalsIgnoreCase("Image17") ||
                key.equalsIgnoreCase("Image18") ||
                key.equalsIgnoreCase("Image19") ||
                key.equalsIgnoreCase("Image20") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("photo_id") ||
                key.equalsIgnoreCase("Image") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }
}