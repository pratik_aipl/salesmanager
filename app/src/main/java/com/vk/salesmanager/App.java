package com.vk.salesmanager;

import android.os.StrictMode;
import androidx.multidex.MultiDexApplication;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.vk.salesmanager.Activities.Order.AddOrder;
import com.vk.salesmanager.Models.AddMember;
import com.vk.salesmanager.Models.MainUser;
import com.vk.salesmanager.Utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by admin on 10/12/2016.
 */
public class App extends MultiDexApplication {

    public static App instance;
    public static String user_id = "";
    public static String login_token = "";
    public static MainUser mainUser;
    public static AddMember addMemberObj;
    public static MySharedPref mySharedPref;
    public static String myPref = "sales_pref";
    public static App app;
    public static String mobile_no = "";
    public static String otp = "";
    public static AddOrder addOrderObj;
    public static String rankvalue = "";
    public static boolean isClickedToAppliedRank = false;
    public static List<Integer> indexListArea = new ArrayList<>();
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        try {
            mySharedPref = new MySharedPref(instance);
            app = this;
            Class.forName("android.os.AsyncTask");

        } catch (Exception e) {
            e.printStackTrace();
        }
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                      /*  try {
                            Intent intent = new Intent(instance, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("inNoti", "1");
                            instance.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                    }
                })

                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();
    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static App getInstance() {
        return instance;
    }

}
