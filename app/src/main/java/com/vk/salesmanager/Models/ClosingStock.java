package com.vk.salesmanager.Models;

import java.io.Serializable;

public class ClosingStock implements Serializable {

    public String ItemID = "";
    public String ItemName = "";
    public String Qty = "";
    public int OrderedQTY = 0;
    public int DelieveredQTY = 0;
    public int PendingDelieveredQTY = 0;
    public int BalancedQTY = 0;

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public int getOrderedQTY() {
        return OrderedQTY;
    }

    public void setOrderedQTY(int orderedQTY) {
        OrderedQTY = orderedQTY;
    }

    public int getDelieveredQTY() {
        return DelieveredQTY;
    }

    public void setDelieveredQTY(int delieveredQTY) {
        DelieveredQTY = delieveredQTY;
    }

    public int getPendingDelieveredQTY() {
        return PendingDelieveredQTY;
    }

    public void setPendingDelieveredQTY(int pendingDelieveredQTY) {
        PendingDelieveredQTY = pendingDelieveredQTY;
    }

    public int getBalancedQTY() {
        return BalancedQTY;
    }

    public void setBalancedQTY(int balancedQTY) {
        BalancedQTY = balancedQTY;
    }
}
