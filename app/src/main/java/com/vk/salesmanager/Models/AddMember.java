package com.vk.salesmanager.Models;

import java.io.Serializable;

public class AddMember implements Serializable {

    public String FirstName = "";
    public String LastName = "";
    public String DOB = "";
    public String UniqueIdentityNO = "";
    public String Address = "";
    public String Area = "";
    public String City = "";
    public String State = "";
    public String Zipcode = "";
    public String CompanyLocationID = "";
    public String Email = "";
    public String Mobile = "";
    public String UniqueIdentityImage = "";
    public String Photo = "";
    public String UserId="";
    public String DistributorName="";
    public String RSMUserID="";
    public String ASMUserID="";
    public String SMUserID="";
    public String SSUserID="";
    public String DistId="";

    public String getDistId() {
        return DistId;
    }

    public void setDistId(String distId) {
        DistId = distId;
    }

    public String getSSUserID() {
        return SSUserID;
    }

    public void setSSUserID(String SSUserID) {
        this.SSUserID = SSUserID;
    }

    public String getSMUserID() {
        return SMUserID;
    }

    public void setSMUserID(String SMUserID) {
        this.SMUserID = SMUserID;
    }

    public String getASMUserID() {
        return ASMUserID;
    }

    public void setASMUserID(String ASMUserID) {
        this.ASMUserID = ASMUserID;
    }

    public String getRSMUserID() {
        return RSMUserID;
    }

    public void setRSMUserID(String RSMUserID) {
        this.RSMUserID = RSMUserID;
    }

    public String getRetailerName() {
        return RetailerName;
    }

    public void setRetailerName(String retailerName) {
        RetailerName = retailerName;
    }

    public String RetailerName="";

    public String getDistributorName() {
        return DistributorName;
    }

    public void setDistributorName(String distributorName) {
        DistributorName = distributorName;
    }

    public String getRetailorName() {
        return RetailorName;
    }

    public void setRetailorName(String retailorName) {
        RetailorName = retailorName;
    }

    public String RetailorName="";

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getUniqueIdentityNO() {
        return UniqueIdentityNO;
    }

    public void setUniqueIdentityNO(String uniqueIdentityNO) {
        UniqueIdentityNO = uniqueIdentityNO;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }


    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getCompanyLocationID() {
        return CompanyLocationID;
    }

    public void setCompanyLocationID(String companyLocationID) {
        CompanyLocationID = companyLocationID;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getUniqueIdentityImage() {
        return UniqueIdentityImage;
    }

    public void setUniqueIdentityImage(String uniqueIdentityImage) {
        UniqueIdentityImage = uniqueIdentityImage;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }
}
