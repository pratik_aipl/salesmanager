package com.vk.salesmanager.Models;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 11/2/2018.
 */

public class Route implements Serializable {
    public String RouteID = "";
    public String CompanyLocationID = "";
    public String From = "";
    public String To = "";
    public String LocationName = "";

    public String getRouteID() {
        return RouteID;
    }

    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    public String getCompanyLocationID() {
        return CompanyLocationID;
    }

    public void setCompanyLocationID(String companyLocationID) {
        CompanyLocationID = companyLocationID;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getRounteNo() {
        return RounteNo;
    }

    public void setRounteNo(String rounteNo) {
        RounteNo = rounteNo;
    }

    public String RounteNo="";

}
