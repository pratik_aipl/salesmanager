package com.vk.salesmanager.Models;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 8/29/2018.
 */

public class Help implements Serializable {
    public String Name = "";
    public String Email = "";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getRoleID() {
        return RoleID;
    }

    public void setRoleID(String roleID) {
        RoleID = roleID;
    }

    public String Mobile = "";
    public String RoleID = "";
}
