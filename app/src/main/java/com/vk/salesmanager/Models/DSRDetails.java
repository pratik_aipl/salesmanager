package com.vk.salesmanager.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class DSRDetails implements Serializable {
    public String CheckIn = "";
    public ArrayList<DSRCheckOutDetails> dsrCheckOutDetails = new ArrayList<>();

    public String getCheckIn() {
        return CheckIn;
    }

    public void setCheckIn(String checkIn) {
        CheckIn = checkIn;
    }

    public ArrayList<DSRCheckOutDetails> getDsrCheckOutDetails() {
        return dsrCheckOutDetails;
    }

    public void setDsrCheckOutDetails(ArrayList<DSRCheckOutDetails> dsrCheckOutDetails) {
        this.dsrCheckOutDetails = dsrCheckOutDetails;
    }
}
