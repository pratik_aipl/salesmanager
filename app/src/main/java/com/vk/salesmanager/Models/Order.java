package com.vk.salesmanager.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable {
    public String Date = "";
    public String OrderHeaderID = "";

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String OrderNo = "";
    public String OrderDate = "";
    public String UserID = "";
    public ArrayList<Items> itemsArrayList=new ArrayList<>();

    public String getOrderHeaderID() {
        return OrderHeaderID;
    }

    public void setOrderHeaderID(String orderHeaderID) {
        OrderHeaderID = orderHeaderID;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getRetailerID() {
        return RetailerID;
    }

    public void setRetailerID(String retailerID) {
        RetailerID = retailerID;
    }

    public String getDistributorID() {
        return DistributorID;
    }

    public void setDistributorID(String distributorID) {
        DistributorID = distributorID;
    }

    public String getOrderAMT() {
        return OrderAMT;
    }

    public void setOrderAMT(String orderAMT) {
        OrderAMT = orderAMT;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getUserDetailID() {
        return UserDetailID;
    }

    public void setUserDetailID(String userDetailID) {
        UserDetailID = userDetailID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getUniqueIdentityNO() {
        return UniqueIdentityNO;
    }

    public void setUniqueIdentityNO(String uniqueIdentityNO) {
        UniqueIdentityNO = uniqueIdentityNO;
    }

    public String getUniqueIdentityURL() {
        return UniqueIdentityURL;
    }

    public void setUniqueIdentityURL(String uniqueIdentityURL) {
        UniqueIdentityURL = uniqueIdentityURL;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getCompanyLocationID() {
        return CompanyLocationID;
    }

    public void setCompanyLocationID(String companyLocationID) {
        CompanyLocationID = companyLocationID;
    }

    public String getParentUserID() {
        return ParentUserID;
    }

    public void setParentUserID(String parentUserID) {
        ParentUserID = parentUserID;
    }

    public String getRetailerName() {
        return RetailerName;
    }

    public void setRetailerName(String retailerName) {
        RetailerName = retailerName;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getCompanyURL() {
        return CompanyURL;
    }

    public void setCompanyURL(String companyURL) {
        CompanyURL = companyURL;
    }

    public String getAdminUserID() {
        return AdminUserID;
    }

    public void setAdminUserID(String adminUserID) {
        AdminUserID = adminUserID;
    }

    public String RetailerID = "";
    public String DistributorID = "";
    public String OrderAMT = "";
    public String OrderStatus = "";
    public String UserDetailID = "";
    public String FirstName = "";
    public String LastName = "";
    public String DOB = "";
    public String UniqueIdentityNO = "";
    public String UniqueIdentityURL = "";
    public String Address = "";
    public String Area = "";
    public String City = "";
    public String State = "";
    public String Country = "";
    public String Zipcode = "";
    public String PhotoURL = "";
    public String CompanyLocationID = "";
    public String ParentUserID = "";
    public String RetailerName = "";
    public String ContactNo = "";
    public String CompanyURL = "";
    public String AdminUserID = "";
    public String SaleManagerFirstName = "";
    public String SaleManagerLastName = "";

    public String getSaleManagerFirstName() {
        return SaleManagerFirstName;
    }

    public void setSaleManagerFirstName(String saleManagerFirstName) {
        SaleManagerFirstName = saleManagerFirstName;
    }

    public String getSaleManagerLastName() {
        return SaleManagerLastName;
    }

    public void setSaleManagerLastName(String saleManagerLastName) {
        SaleManagerLastName = saleManagerLastName;
    }
}
