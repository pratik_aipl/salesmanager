package com.vk.salesmanager.Models;

import java.io.Serializable;

public class Area implements Serializable {
    public String ID = "";

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String Value = "";
}