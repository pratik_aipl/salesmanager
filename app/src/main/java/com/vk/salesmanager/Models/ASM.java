package com.vk.salesmanager.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ASM implements Serializable {
    public String id = "";
    public String role_id = "";
    public String username = "";
    public String password = "";
    public String email = "";
    public String mobile = "";
    public String activated = "";
    public String banned = "";
    public String ban_reason = "";
    public String new_password_key = "";
    public String new_password_requested = "";
    public String new_email = "";
    public String new_email_key = "";
    public String last_ip = "";
    public String last_login = "";
    public String created = "";
    public String modified = "";
    public String UserDetailID = "";
    public String UserID = "";
    public String FirstName = "";
    public String LastName = "";
    public String DOB = "";
    public String UniqueIdentityNO = "";
    public String UniqueIdentityURL = "";
    public String Address = "";
    public String Area = "";
    public String City = "";
    public String State = "";
    public String Country = "";
    public String Zipcode = "";
    public String PhotoURL = "";
    public String CompanyLocationID = "";
    public String ParentUserID = "";
    public String AreaName = "";
    public String CityName = "";
    public String StateName = "";
    public String CountryName = "";
    public String  ItemName  = "";
    public String  itemID  = "";
    public String  Name  = "";

    public String  OrderDate  = "";
    public String  TotalSales  = "";

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getTotalSales() {
        return TotalSales;
    }

    public void setTotalSales(String totalSales) {
        TotalSales = totalSales;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        AreaName = areaName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getBanned() {
        return banned;
    }

    public void setBanned(String banned) {
        this.banned = banned;
    }

    public String getBan_reason() {
        return ban_reason;
    }

    public void setBan_reason(String ban_reason) {
        this.ban_reason = ban_reason;
    }

    public String getNew_password_key() {
        return new_password_key;
    }

    public void setNew_password_key(String new_password_key) {
        this.new_password_key = new_password_key;
    }

    public String getNew_password_requested() {
        return new_password_requested;
    }

    public void setNew_password_requested(String new_password_requested) {
        this.new_password_requested = new_password_requested;
    }

    public String getNew_email() {
        return new_email;
    }

    public void setNew_email(String new_email) {
        this.new_email = new_email;
    }

    public String getNew_email_key() {
        return new_email_key;
    }

    public void setNew_email_key(String new_email_key) {
        this.new_email_key = new_email_key;
    }

    public String getLast_ip() {
        return last_ip;
    }

    public void setLast_ip(String last_ip) {
        this.last_ip = last_ip;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getUserDetailID() {
        return UserDetailID;
    }

    public void setUserDetailID(String userDetailID) {
        UserDetailID = userDetailID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getUniqueIdentityNO() {
        return UniqueIdentityNO;
    }

    public void setUniqueIdentityNO(String uniqueIdentityNO) {
        UniqueIdentityNO = uniqueIdentityNO;
    }

    public String getUniqueIdentityURL() {
        return UniqueIdentityURL;
    }

    public void setUniqueIdentityURL(String uniqueIdentityURL) {
        UniqueIdentityURL = uniqueIdentityURL;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public void setZipcode(String zipcode) {
        Zipcode = zipcode;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getCompanyLocationID() {
        return CompanyLocationID;
    }

    public void setCompanyLocationID(String companyLocationID) {
        CompanyLocationID = companyLocationID;
    }

    public String getParentUserID() {
        return ParentUserID;
    }

    public void setParentUserID(String parentUserID) {
        ParentUserID = parentUserID;
    }

    public ArrayList<CompanyLocation> getCompany_location() {
        return company_location;
    }

    public void setCompany_location(ArrayList<CompanyLocation> company_location) {
        this.company_location = company_location;
    }

    public ArrayList<CompanyLocation> company_location = new ArrayList<>();
}
