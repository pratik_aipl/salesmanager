package com.vk.salesmanager.Models;

/**
 * Created by empiere-vaibhav on 8/4/2018.
 */

public class NavDrawerItem {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcons() {
        return icons;
    }

    public void setIcons(int icons) {
        this.icons = icons;
    }

    private int icons;

}
