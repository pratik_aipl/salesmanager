package com.vk.salesmanager.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class DSRCheckOutDetails implements Serializable {

    public String CheckOut = "";
    public String RetailerName = "";
    public String FirstName = "";
    public String LastName = "";
    public String OrderPlace = "";
    public String OrderNumber = "";
    public String Remarks = "";

    public String getCheckOut() {
        return CheckOut;
    }

    public void setCheckOut(String checkOut) {
        CheckOut = checkOut;
    }

    public String getRetailerName() {
        return RetailerName;
    }

    public void setRetailerName(String retailerName) {
        RetailerName = retailerName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getOrderPlace() {
        return OrderPlace;
    }

    public void setOrderPlace(String orderPlace) {
        OrderPlace = orderPlace;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }
}
