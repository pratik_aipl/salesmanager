package com.vk.salesmanager.Models;

import java.io.Serializable;

public class Items implements Serializable {
    public String OrderDetailID = "";
    public String OrderHeaderID = "";
    public String ItemID = "";
    public String QTY = "";
    public String Price = "";
    public String ItemName = "";
    public String SKU = "";
    public String CompanyID = "";
    public String Weight = "";
    public String UnitWeight = "";
    public String EnterQty="";
    public String Pendding="";

    public String getPendding() {
        return Pendding;
    }

    public void setPendding(String pendding) {
        Pendding = pendding;
    }

    public String getEnterQty() {
        return EnterQty;
    }

    public void setEnterQty(String enterQty) {
        EnterQty = enterQty;
    }

    public String getOrderDetailID() {
        return OrderDetailID;
    }

    public void setOrderDetailID(String orderDetailID) {
        OrderDetailID = orderDetailID;
    }

    public String getOrderHeaderID() {
        return OrderHeaderID;
    }

    public void setOrderHeaderID(String orderHeaderID) {
        OrderHeaderID = orderHeaderID;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getQTY() {
        return QTY;
    }

    public void setQTY(String QTY) {
        this.QTY = QTY;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getUnitWeight() {
        return UnitWeight;
    }

    public void setUnitWeight(String unitWeight) {
        UnitWeight = unitWeight;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getItemUrl() {
        return ItemUrl;
    }

    public void setItemUrl(String itemUrl) {
        ItemUrl = itemUrl;
    }

    public String getMRP() {
        return MRP;
    }

    public void setMRP(String MRP) {
        this.MRP = MRP;
    }

    public String getOrderFollowUpID() {
        return OrderFollowUpID;
    }

    public void setOrderFollowUpID(String orderFollowUpID) {
        OrderFollowUpID = orderFollowUpID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDelieveredQTY() {
        return DelieveredQTY;
    }

    public void setDelieveredQTY(String delieveredQTY) {
        DelieveredQTY = delieveredQTY;
    }

    public String Remark = "";
    public String ItemUrl = "";
    public String MRP = "";
    public String OrderFollowUpID = "";
    public String Date = "";
    public String DelieveredQTY = "";
}
