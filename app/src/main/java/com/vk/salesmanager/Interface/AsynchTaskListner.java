package com.vk.salesmanager.Interface;


import com.vk.salesmanager.Utils.Constant;

import org.json.JSONException;

/**
 * Created by admin on 10/12/2016.
 */
public interface AsynchTaskListner {

    public void onTaskCompleted(String result, Constant.REQUESTS request ,String respCode) throws JSONException;

  }
