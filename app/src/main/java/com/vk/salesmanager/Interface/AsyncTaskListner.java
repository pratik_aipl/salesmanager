package com.vk.salesmanager.Interface;

import com.vk.salesmanager.Utils.Constant;

/**
 * Created by Karan - Empiere on 8/13/2018.
 */

public interface AsyncTaskListner {

    public void onTaskCompleted(String result, Constant.REQUESTS request);

}
